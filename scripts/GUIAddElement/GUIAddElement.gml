/// @function GUIAddElement
/// @description GUIAddElement
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Color

with obj_GUI {
	gridIterator++
	ds_grid_resize(guiGrid, guiGrid_t.LENGTH, gridIterator+1)
	guiGrid[# guiGrid_t.TYPE,	gridIterator] = argument0
	guiGrid[# guiGrid_t.X,		gridIterator] = argument1
	guiGrid[# guiGrid_t.Y,		gridIterator] = argument2
	guiGrid[# guiGrid_t.COLOR,	gridIterator] = argument3
}