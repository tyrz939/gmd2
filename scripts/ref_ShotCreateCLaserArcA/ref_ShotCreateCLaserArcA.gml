/// @function ShotCreateCLaserArcA
/// @description Creates a arc of lasers
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Spread
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} StepSpeedChange
/// @param {real} FinalSpeed
/// @param {real} [opt]SpeedChangeDelay
/// @param {real} [opt]Sound

var __ac = argument_count
if !in_range(__ac, 12, 14) return lua_show_error("ShotCreateCLaserArcA: Expected 12-14 arguments, got " + string(__ac));

if __ac == 12 {
	return ShotCreateCLaserArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11]);
} else if __ac == 13 {
	return ShotCreateCLaserArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11], argument[12]);
} else if __ac == 14 {
	return ShotCreateCLaserArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11], argument[12], argument[13]);
}