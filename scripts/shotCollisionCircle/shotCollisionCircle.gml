/// @function CollisionCircle
/// @description Shot Creater
/// @param {real} x
/// @param {real} y
/// @param {real} Radius
gml_pragma("forceinline")

var _x = argument0
var _y = argument1
var _radius = argument2

var _xc, _yc, _c, _i = 0, _bdWidth, _bdHeight, _bdColor, _bdType

// Check Player
_xc = game.bulletRemover[# _i, bulletRemover_t.X]
_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
_c = point_distance(_x, _y, _xc, _yc) - _radius - game.bulletRemover[# _i, bulletRemover_t.RADIUS] - game.grazeRadius
if _c <= 0 {
	playerGraze(grazeAdd * clamp(spd, 1, 5))
	_c = point_distance(_x, _y, _xc, _yc) - _radius - game.bulletRemover[# _i, bulletRemover_t.RADIUS]
	if _c <= 0 {
		deleteType = DELTYPE_ANIMATED
		return _i
	}
}

// Check Deleters
for(_i = 1; _i < ds_grid_width(game.bulletRemover); _i++) {
	// Circle Deleters
	if game.bulletRemover[# _i, bulletRemover_t.HITBOX] == 0 {
		_xc = game.bulletRemover[# _i, bulletRemover_t.X]
		_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
		_c = point_distance(_x, _y, _xc, _yc) - _radius - game.bulletRemover[# _i, bulletRemover_t.RADIUS]
		
		if _c <= 0 {
			if (game.bulletRemover[# _i, bulletRemover_t.COLOR] == shotColor || game.bulletRemover[# _i, bulletRemover_t.COLOR] == -1)  &&
				(game.bulletRemover[# _i, bulletRemover_t.TYPE] == shotType || game.bulletRemover[# _i, bulletRemover_t.TYPE] == -1) {
				deleteType = game.bulletRemover[# _i, bulletRemover_t.DELETE_TYPE]
				return _i
			}
		}
	} else if game.bulletRemover[# _i, bulletRemover_t.HITBOX] == 1 {
	// Rectangle Deleters
		_xc = game.bulletRemover[# _i, bulletRemover_t.X]
		_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
		_bdWidth = game.bulletRemover[# _i, bulletRemover_t.WIDTH] + game.bulletRemover[# _i, bulletRemover_t.RADIUS]
		_bdHeight = game.bulletRemover[# _i, bulletRemover_t.HEIGHT] + game.bulletRemover[# _i, bulletRemover_t.RADIUS]
		_bdColor = game.bulletRemover[# _i, bulletRemover_t.COLOR]
		_bdType = game.bulletRemover[# _i, bulletRemover_t.TYPE]
		
		var __angleDiff = angle_difference(game.bulletRemover[# _i, bulletRemover_t.ANGLE], point_direction(_xc, _yc, _x, _y))
		var __distance = point_distance(_xc, _yc, _x, _y)
		var __pdx = _xc + lengthdir_x(__distance, __angleDiff)
		var __pdy = _yc + lengthdir_y(__distance, __angleDiff)
		
		_c = false
		if __pdx > _xc - _bdWidth && __pdx < _xc + _bdWidth &&
			__pdy > _yc - _bdHeight && __pdy < _yc + _bdHeight{
			_c = true
		}
		if _c != false {
			if (_bdColor == shotColor || _bdColor == -1)  &&
				(_bdType == shotType || _bdType == -1) {
				deleteType = game.bulletRemover[# _i, bulletRemover_t.DELETE_TYPE]
				return _i
			}
		}
	}
}

return noone