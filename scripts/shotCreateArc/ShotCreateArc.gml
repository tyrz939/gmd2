/// @function shotCreateArc
/// @description Creates an arc of shots
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Spread
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 8 {
	if argument[8] != false {
		play_sfx(argument[8], false)
	}
}
// Delay Timer
if argument_count > 9 {
	global.__shotDelay = argument[9]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}

var __shotType = argument[0]
var __shotX = argument[1]
var __shotY = argument[2]
var __shotSpeed = argument[3]
var __shotDirection = argument[4]


global.__shotGroupAmount = argument[5]
global.__shotGroupSpread = argument[6]
global.__shotGroupDistFromCenter = argument[7]

global.__shotCreateType = bulletType_t.shotGroup
global.__shotGroupType = shotGroupType.arc
__shotDirection -= (global.__shotGroupSpread / 2) - (global.__shotGroupSpread / global.__shotGroupAmount) / 2

return ShotCreate(__shotType, __shotX, __shotY, __shotSpeed, __shotDirection, false, global.__shotDelay)