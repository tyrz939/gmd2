/// @function shotAddCLaser
/// @description shotAddCLaser
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color

if (argument_count != 3)  return lua_show_error("shotAddCLaser: Expected 3 arguments, got " + string(argument_count));
return shotAddCLaser(argument[0], argument[1], argument[2]);