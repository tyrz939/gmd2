/// @function shotCreateCircleA
/// @description Creates a circle of shots with Acceleration/Deceleration
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} DistanceFromCenter
/// @param {real} StepSpeedChange
/// @param {real} FinalSpeed
/// @param {real} [opt]SpeedChangeDelay
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 10 {
	if argument[10] != false {
		play_sfx(argument[10], false)
	}
}
// Delay Timer
if argument_count > 11 {
	global.__shotDelay = argument[11]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}
var __inst = ShotCreateCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], false, global.__shotDelay)
with __inst {
	for(var i = 0; i < __amount; i++) {
		grid[# __accelStepSpeed, i] = argument[7]
		grid[# __accelFinalSpeed, i] = argument[8]
		
		// Work out how long it will take
		grid[# __accelTime, i] = abs((argument[3] - grid[# __accelFinalSpeed, i]) / grid[# __accelStepSpeed, i])
		
		// Accelerating, 1. Decelerating 2
		if grid[# __accelFinalSpeed, i] > grid[# __spd, i] {
			grid[# __accelState, i] = 1
		} else {
			grid[# __accelState, i] = 2
		}
		// Delay timer before starting
		if argument_count > 9 {
			grid[# __accelDelay, i] = argument[9]
		} else {
			grid[# __accelDelay, i] = 0
		}
	}
}
return __inst