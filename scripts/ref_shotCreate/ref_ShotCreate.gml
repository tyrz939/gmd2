/// @description  ShotCreate
/// @function  ShotCreate
/// @param  Type:number
/// @param  x:number
/// @param  y:number
/// @param  Speed:number
/// @param  Direction:number
/// @param  Sound:number
/// @param  Delay:number
var __ac = argument_count
if !in_range(__ac, 5, 7) return lua_show_error("ShotCreate: Expected 5-7 arguments, got " + string(__ac));

if __ac == 5 {
	return ShotCreate(argument[0], argument[1], argument[2], argument[3], argument[4]);
} else if __ac == 6 {
	return ShotCreate(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5]);
} else if __ac == 7 {
	return ShotCreate(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6]);
}