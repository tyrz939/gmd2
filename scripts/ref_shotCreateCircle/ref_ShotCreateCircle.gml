/// @description  ShotCreateCircle
/// @function  ShotCreateCircle
/// @param  Type:number
/// @param  x:number
/// @param  y:number
/// @param  Speed:number
/// @param  Direction:number
/// @param  Amount:number
/// @param  DistFromCenter:number
/// @param  Sound:number
/// @param  Delay:number
var __ac = argument_count
if !in_range(__ac, 7, 9) return lua_show_error("ShotCreateCircle: Expected 7-9 arguments, got " + string(__ac));

if __ac == 7 {
	return ShotCreateCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6]);
} else if __ac == 8 {
	return ShotCreateCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7]);
} else if __ac == 9 {
	return ShotCreateCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8]);
}