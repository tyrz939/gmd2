/// @function deleteShotAll
/// @description deleteShotAll
/// @param {real} DeleteType

// TODO delete type

var deleteType = argument0

//with obj_bulletParent {instance_destroy()}
with obj_delayLaser {instance_destroy()}

deleteShotRectangle(0, 0, global.playAreaWidth, global.playAreaHeight, 30)