/// @function shotGroupDeleteBullet
/// @description Shot Creater
/// @param {real} GroupID
/// @param {real} BulletNumber

var __groupID = argument0, __bulletNumber = argument1

if instance_exists(__groupID) {
	with __groupID {
		if object_index == obj_bulletGroup {
			if __bulletNumber < __amount {
				if grid[# __alive, __bulletNumber] {
					grid[# __alive, __bulletNumber] = false
					__amountLeft--
					if deleteType >= 1 {
						part_particles_create_colour(global.partSys, grid[# __xx, __bulletNumber], grid[# __yy, __bulletNumber], global.partShotDie, shotColor, 1)
					}
				}
				// TODO graphics for destroying bullets
				return true
			} else {
				print_error("shotGroupDeleteBullet: Bullet number '" + string(argument1) + "' exceeded number of bullets in group")
			}
		} else {
			return print_error("shotGroupDeleteBullet: Object isn't a bullet group")
		}
	}
} else {
	return print_error("shotGroupDeleteBullet: Bullet group doesn't exist")
}