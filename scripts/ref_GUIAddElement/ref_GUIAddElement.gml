/// @function GUIAddElement
/// @description GUIAddElement
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Color

if (argument_count != 4)  return lua_show_error("GUIAddElement: Expected 4 arguments, got " + string(argument_count));
GUIAddElement(argument[0], argument[1], argument[2], argument[3]);
