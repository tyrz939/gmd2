/// @function shotAdd
/// @description shotAdd
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} hitboxType
/// @param {real} radius
/// @param {real} width
/// @param {real} height
/// @param {real} drawRotated
/// @param {real} drawRounded
/// @param {real} spawnSprite
/// @param {real} spawnSubimage

global.shotAddIterator++
var _i = global.shotAddIterator

ds_grid_resize(shotCreateGrid, _i+1, shotCreation.length)

shotCreateGrid[# _i, shotCreation.sprite] =			argument0
shotCreateGrid[# _i, shotCreation.subimage] =		argument1
shotCreateGrid[# _i, shotCreation.color] =			argument2
shotCreateGrid[# _i, shotCreation.hitboxType] =		argument3
shotCreateGrid[# _i, shotCreation.radius] =			argument4
shotCreateGrid[# _i, shotCreation.width] =			argument5
shotCreateGrid[# _i, shotCreation.height] =			argument6
shotCreateGrid[# _i, shotCreation.drawRotated] =	argument7
shotCreateGrid[# _i, shotCreation.drawRounded] =	argument8
shotCreateGrid[# _i, shotCreation.spawnSprite] =	argument9
shotCreateGrid[# _i, shotCreation.spawnSubimage] =	argument10

return _i