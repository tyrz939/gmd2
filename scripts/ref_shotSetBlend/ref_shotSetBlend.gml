/// @function shotSetBlend
/// @description shotSetBlend
/// @param {real} Instance
/// @param {real} BlendType

if (argument_count != 2)  return lua_show_error("ShotSetBlend: Expected 2 arguments, got " + string(argument_count));
shotSetBlend(argument[0], argument[1]);
