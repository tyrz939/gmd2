var state = argument0;
//lua_add_function(state, "d3d_draw_wall", ref_d3d_draw_wall);
//lua_add_function(state, "d3d_draw_floor", ref_d3d_draw_floor);
//lua_add_function(state, "sprite_get_texture", ref_sprite_get_texture);

// Adding 3D
lua_add_function(state, "d3dAddFloor", ref_d3d_addFloor);
lua_add_function(state, "d3dAddWall", ref_d3d_addWall);
lua_add_function(state, "d3dAddSetBlend", ref_d3d_addSetBlend);
lua_add_function(state, "d3dAddSetAlpha", ref_d3d_addSetAlpha);
lua_add_function(state, "d3dAddSetColor", ref_d3d_addSetColor);

// Setting 3d variables
lua_add_function(state, "d3dSetFog", ref_d3dSetFog);
lua_add_function(state, "d3dSetX", ref_d3dSetX);
lua_add_function(state, "d3dSetY", ref_d3dSetY);
lua_add_function(state, "d3dSetZ", ref_d3dSetZ);
lua_add_function(state, "d3dSetYaw", ref_d3dSetYaw);
lua_add_function(state, "d3dSetPitch", ref_d3dSetPitch);
lua_add_function(state, "d3dGetX", ref_d3dGetX);
lua_add_function(state, "d3dGetY", ref_d3dGetY);
lua_add_function(state, "d3dGetZ", ref_d3dGetZ);
lua_add_function(state, "d3dGetYaw", ref_d3dGetYaw);
lua_add_function(state, "d3dGetPitch", ref_d3dGetPitch);