/// @function shotCreateCircle
/// @description Creates a circle of shots
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 7 {
	if argument[7] != false {
		play_sfx(argument[7], false)
	}
}
// Delay Timer
if argument_count > 8 {
	global.__shotDelay = argument[8]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}

var __shotType = argument[0]
var __shotX = argument[1]
var __shotY = argument[2]
var __shotSpeed = argument[3]
var __shotDirection = argument[4]

global.__shotGroupAmount = argument[5]
global.__shotGroupDistFromCenter = argument[6]

global.__shotCreateType = bulletType_t.shotGroup
global.__shotGroupType = shotGroupType.circle
return ShotCreate(__shotType, __shotX, __shotY, __shotSpeed, __shotDirection, false, global.__shotDelay)