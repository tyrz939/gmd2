/// @function shotAddRectangle
/// @description shotAddRectangle
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} width
/// @param {real} height
/// @param {real} drawRotated
/// @param {real} drawRounded

if (argument_count != 7)  return lua_show_error("shotAddRectangle: Expected 7 arguments, got " + string(argument_count));
return shotAddRectangle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6]);