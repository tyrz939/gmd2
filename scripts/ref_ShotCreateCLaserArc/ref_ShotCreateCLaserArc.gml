/// @function ShotCreateCLaserArc
/// @description Creates a arc of lasers
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Spread
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound

var __ac = argument_count
if !in_range(__ac, 10, 11) return lua_show_error("ShotCreateCLaserArc: Expected 10-11 arguments, got " + string(__ac));

if __ac == 10 {
	return ShotCreateCLaserArc(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9]);
} else if __ac == 11 {
	return ShotCreateCLaserArc(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10]);
}