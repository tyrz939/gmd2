/// @function shotAddSLaser
/// @description shotAddshotAddSLaser
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} Width (hitbox)
/// @param {real} spawnSprite
/// @param {real} spawnSubimage

return shotAdd(argument0, argument1, argument2, STATICLASER, 0, argument3, 0, -1, 0, argument4, argument5)