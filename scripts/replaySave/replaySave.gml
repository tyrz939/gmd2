/// @function replaySave
/// @description Replay Saver
/// @param {real} replayNumber
var s
var _replayString = ""
// Date
s = ""
s += stringZerosBefore(current_day, 2) + "/"
s += stringZerosBefore(current_month, 2) + "/"
s += string(current_year)
_replayString += "$md" + s
//Time
s = ""
s += stringZerosBefore(current_hour, 2) + ":"
s += stringZerosBefore(current_minute, 2)
_replayString += "$mt" + s

// Script Related
_replayString += "$ss" + string(global.scriptDirectory)
_replayString += "$sf" + string(global.scriptFile)
_replayString += "$sn" + string(global.scriptName)
_replayString += "$sd" + string(global.scriptDesc)
_replayString += "$sa" + string(global.scriptAuth)
// Game/Player Values
_replayString += "$gq" + string(score)
_replayString += "$gc" + string(global.playerNumber)
_replayString += "$gd" + string(global.difficulty)
_replayString += "$gf" + string(game.frame)
_replayString += "$gs" + string(game.stageNumber)
_replayString += "$gp" + string(game.stagePosition)
// Input Stream
_replayString += "$iw" + string(ds_grid_width(player.__replay))
_replayString += "$ii" + ds_grid_write(player.__replay)

// END OF STRING, IMPORTANT!!!!
_replayString += "$es"

// Buffer Creation
var _replayBuffer = buffer_create(1, buffer_grow, 1)
buffer_write(_replayBuffer, buffer_string, _replayString)

// Buffer Compression
var _replayBufferCompressed = buffer_compress(_replayBuffer, 0, buffer_get_size(_replayBuffer))

// Saving
var _file = replayNameGen(argument0)
while !directory_exists_ns("replay/") {
	show_message("Please create a 'replay' folder in the games executable directory, then click OK")
}

buffer_save_ns(_replayBufferCompressed, _file)

// Delete Buffers
buffer_delete(_replayBuffer)
buffer_delete(_replayBufferCompressed)
