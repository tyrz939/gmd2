uniTime = shader_get_uniform(sh_haze, "Time")
uniSamp = shader_get_sampler_index(sh_haze, "Noise")
uniSampSize = shader_get_sampler_index(sh_haze, "NoiseSize")

uniSpeed = shader_get_uniform(sh_haze, "Speed")
uniSize = shader_get_uniform(sh_haze, "Size")
uniFreq = shader_get_uniform(sh_haze, "Freq")

surfaceDistortion = noone