var state = argument0;
lua_add_function(state, "draw_image", ref_draw_image);
lua_add_function(state, "draw_image_pos", ref_draw_image_pos);
lua_add_function(state, "draw_image_ext", ref_draw_image_ext);
lua_add_function(state, "draw_image_stretched", ref_draw_image_stretched);
lua_add_function(state, "draw_image_stretched_ext", ref_draw_image_stretched_ext);
lua_add_function(state, "draw_image_part", ref_draw_image_part);
lua_add_function(state, "draw_image_part_ext", ref_draw_image_part_ext);
lua_add_function(state, "draw_image_general", ref_draw_image_general);