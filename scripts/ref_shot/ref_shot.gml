var state = argument0;
// Create
lua_add_function(state, "ShotCreate", ref_ShotCreate);
lua_add_function(state, "ShotCreateA", ref_ShotCreateA);
lua_add_function(state, "ShotCreateCircle", ref_ShotCreateCircle);
lua_add_function(state, "ShotCreateCircleA", ref_ShotCreateCircleA);
lua_add_function(state, "ShotCreateArc", ref_ShotCreateArc);
lua_add_function(state, "ShotCreateArcA", ref_ShotCreateArcA);
lua_add_function(state, "ShotCreateCLaserCircle", ref_ShotCreateCLaserCircle);
lua_add_function(state, "ShotCreateCLaserCircleA", ref_ShotCreateCLaserCircleA);
lua_add_function(state, "ShotCreateCLaserArc", ref_ShotCreateCLaserArc);
lua_add_function(state, "ShotCreateCLaserArcA", ref_ShotCreateCLaserArcA);
lua_add_function(state, "ShotCreateSLaser", ref_ShotCreateSLaser);
// Delete
lua_add_function(state, "DeleteShotAll", ref_deleteShotAll);
lua_add_function(state, "DeleteShotCircle", ref_deleteShotCircle);
lua_add_function(state, "DeleteShotRectangle", ref_deleteShotRectangle);
lua_add_function(state, "DeleteShotSetXY", ref_deleteShotSetXY);
lua_add_function(state, "DeleteShotSetRadius", ref_deleteShotSetRadius);
lua_add_function(state, "DeleteShotSetAngle", ref_deleteShotSetAngle);
lua_add_function(state, "DeleteShotExists", ref_deleteShotExists);
lua_add_function(state, "DeleteShotDelete", ref_deleteShotDelete);
lua_add_function(state, "DeleteShotDeleteAllDeleters", ref_deleteShotDeleteAllDeleters);
// Group
lua_add_function(state, "ShotGroupDeleteBullet", ref_shotGroupDeleteBullet);
lua_add_function(state, "ShotGroupSetDirection", ref_shotGroupSetDirection);
lua_add_function(state, "ShotGroupSetDirectionPoint", ref_shotGroupSetDirectionPoint);
lua_add_function(state, "ShotGroupDeletePattern", ref_shotGroupDeletePattern);
lua_add_function(state, "ShotGroupReplaceAll", ref_shotGroupReplaceAll);
// Misc
lua_add_function(state, "ShotSetBlend", ref_shotSetBlend);
lua_add_function(state, "ShowHitboxes", ref_showHitboxes);
// Adding Shots
lua_add_function(state, "ShotAdd", ref_shotAdd);
lua_add_function(state, "ShotAddCircle", ref_shotAddCircle);
lua_add_function(state, "ShotAddOval", ref_shotAddOval);
lua_add_function(state, "ShotAddRectangle", ref_shotAddRectangle);
lua_add_function(state, "ShotAddCLaser", ref_shotAddCLaser);
lua_add_function(state, "ShotAddSLaser", ref_shotAddSLaser);
lua_add_function(state, "ShotAddDelayLaser", ref_shotAddDelayLaser);
// Delay Laser
lua_add_function(state, "ShotCreateDelayLaser", ref_shotCreateDelayLaser);
lua_add_function(state, "DelayLaserAdd", ref_delayLaserAdd);