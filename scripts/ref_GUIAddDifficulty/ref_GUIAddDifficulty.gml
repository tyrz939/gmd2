/// @function GUIAddDifficulty
/// @description GUIAddDifficulty
/// @param {real} x
/// @param {real} y

if (argument_count != 2)  return lua_show_error("GUIAddDifficulty: Expected 2 arguments, got " + string(argument_count));
GUIAddElement("difficulty", argument[0], argument[1], 0);
