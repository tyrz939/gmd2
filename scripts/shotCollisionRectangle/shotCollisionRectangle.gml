/// @function CollisionCircle
/// @description Shot Creater
/// @param {real} x
/// @param {real} y
/// @param {real} Width
/// @param {real} Height
/// @param {real} Direction
gml_pragma("forceinline")

var _x = argument0
var _y = argument1
var __width = argument2
var __height = argument3
var _direction = argument4


var _xc, _yc, _c, __width, __height, __angleDiff, __distance, __pdx, __pdy, _ret, _BDradius, _i = 0, _bdWidth, _bdHeight, _bdColor, _bdType

// Check Player
_xc = game.bulletRemover[# _i, bulletRemover_t.X]
_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
_BDradius = game.bulletRemover[# _i, bulletRemover_t.RADIUS]
if point_distance(_xc, _yc, _x, _y) < (max(__width, __height) * 1.5) + _BDradius + game.grazeRadius {
	__angleDiff = angle_difference(point_direction(_x, _y, _xc, _yc), _direction)
	__distance = point_distance(_x, _y, _xc, _yc)
	__pdx = _x + lengthdir_x(__distance, __angleDiff)
	__pdy = _y + lengthdir_y(__distance, __angleDiff)
	_c = rectangle_in_circle(_x - (__width + game.grazeRadius), _y - (__height + game.grazeRadius), _x + (__width + game.grazeRadius), _y + (__height + game.grazeRadius), __pdx, __pdy, game.bulletRemover[# _i, bulletRemover_t.RADIUS])
	if _c != false {
		playerGraze(grazeAdd * clamp(spd, 1, 5))
		_c = rectangle_in_circle(_x - __width, _y - __height, _x + __width, _y + __height, __pdx, __pdy, game.bulletRemover[# _i, bulletRemover_t.RADIUS])
		if _c != false {
			deleteType = DELTYPE_ANIMATED
			return _i
		}
	}
}

// Check Deleters
for(_i = 1; _i < ds_grid_width(game.bulletRemover); _i++) {
	// Circle Deleters
	if game.bulletRemover[# _i, bulletRemover_t.HITBOX] == 0 {
		_xc = game.bulletRemover[# _i, bulletRemover_t.X]
		_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
		_BDradius = game.bulletRemover[# _i, bulletRemover_t.RADIUS]
		if point_distance(_xc, _yc, _x, _y) < (max(__width, __height) * 1.5) + _BDradius {
			var __angleDiff = angle_difference(point_direction(_x, _y, _xc, _yc), _direction)
			var __distance = point_distance(_x, _y, _xc, _yc)
			var __pdx = _x + lengthdir_x(__distance, __angleDiff)
			var __pdy = _y + lengthdir_y(__distance, __angleDiff)
			_c = rectangle_in_circle(_x - __width, _y - __height, _x + __width, _y + __height, __pdx, __pdy, game.bulletRemover[# _i, bulletRemover_t.RADIUS])
			if _c != false {
				_ret = false
				if (game.bulletRemover[# _i, bulletRemover_t.COLOR] == shotColor || game.bulletRemover[# _i, bulletRemover_t.COLOR] == -1)  &&
					(game.bulletRemover[# _i, bulletRemover_t.TYPE] == shotType || game.bulletRemover[# _i, bulletRemover_t.TYPE] == -1) {
					_ret = true
				}
				if _ret {
					deleteType = game.bulletRemover[# _i, bulletRemover_t.DELETE_TYPE]
					return _i
				}
			}
		}
	} else if game.bulletRemover[# _i, bulletRemover_t.HITBOX] == 1 {
	// Rectangle Deleters
		_xc = game.bulletRemover[# _i, bulletRemover_t.X]
		_yc = game.bulletRemover[# _i, bulletRemover_t.Y]
		_bdWidth = game.bulletRemover[# _i, bulletRemover_t.WIDTH] + max(__width, __height) * 1.5
		_bdHeight = game.bulletRemover[# _i, bulletRemover_t.HEIGHT] + max(__width, __height) * 1.5
		_bdColor = game.bulletRemover[# _i, bulletRemover_t.COLOR]
		_bdType = game.bulletRemover[# _i, bulletRemover_t.TYPE]
		
		var __angleDiff = angle_difference(game.bulletRemover[# _i, bulletRemover_t.ANGLE], point_direction(_xc, _yc, _x, _y))
		var __distance = point_distance(_x, _y, _xc, _yc)
		var __pdx = _xc + lengthdir_x(__distance, __angleDiff)
		var __pdy = _yc + lengthdir_y(__distance, __angleDiff)
		
		_c = false
		if __pdx > _xc - _bdWidth && __pdx < _xc + _bdWidth &&
			__pdy > _yc - _bdHeight && __pdy < _yc + _bdHeight{
			_c = true
		}
		if _c != false {
			_ret = false
			if (_bdColor == shotColor || _bdColor == -1)  &&
				(_bdType == shotType || _bdType == -1) {
				_ret = true
			}
			if _ret {
				deleteType = game.bulletRemover[# _i, bulletRemover_t.DELETE_TYPE]
				return _i
			}
		}
	}
}

return noone