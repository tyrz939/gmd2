/// @function shotGroupDeleteBullet
/// @description Shot Creater
/// @param {real} GroupID
/// @param {real} BulletNumber

if (argument_count != 2)  return lua_show_error("shotGroupDeleteBullet: Expected 2 arguments, got " + string(argument_count));

return shotGroupDeleteBullet(argument[0], argument[1]);