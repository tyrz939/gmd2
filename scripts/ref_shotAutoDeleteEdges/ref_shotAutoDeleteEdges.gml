/// @function shotAutoDeleteEdges
/// @description Shot Creater
/// @param {real} left
/// @param {real} top
/// @param {real} right
/// @param {real} bottom

if (argument_count != 4)  return lua_show_error("shotAutoDeleteEdges: Expected 4 arguments, got " + string(argument_count));
shotAutoDeleteEdges(argument[0], argument[1], argument[2], argument[3]);
