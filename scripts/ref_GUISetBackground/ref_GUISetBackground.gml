if image_exists(argument[0]) {
	game.backgroundGUI = argument[0]
} else {
	return lua_show_error("GUISetBackground: Image doesn't exist");
}