/// @description  image_group_create(string:name)
/// @function  image_group_create
/// @param string:name
if (argument_count != 1) return lua_show_error("image_group_create: Expected 1 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_group_create: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
return image_group_create(argument0);