/// @description MOD_Pause(pause)
/// @function MOD_Pause
/// @param pause

var _pause = argument0;

with(obj_mod_player)
{
  playing = !_pause;
}