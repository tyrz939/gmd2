/// @description MOD_Stop()
/// @function MOD_Stop

with(obj_mod_player)
{
  playing = false;
  end_reached = false;
  ModPlug_Stop();
}