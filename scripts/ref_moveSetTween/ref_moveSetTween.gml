/// @function MoveSetTween
/// @description moveTween
/// @param {real} TweenType

if (argument_count != 1)  return lua_show_error("MoveSetTween: Expected 1 arguments, got " + string(argument_count));
var t = tweenCheck(argument0)
if t != false {global.tweenMove = t}
else {lua_show_error("MoveSetTween: No Tween of type '" + string(argument0) + "' exists");}