/// @function shotGroupSetDirection
/// @description shotGroupSetDirection
/// @param {real} instance
/// @param {real} bulletNumber
/// @param {real} direction

var __groupID = argument0, __bulletNumber = argument1, __bulletDirection = argument2

if instance_exists(__groupID) {
	with __groupID {
		if object_index == obj_bulletGroup || object_index == obj_cLaserGroup {
			if __bulletNumber < __amount {
				if __bulletNumber == -1 {
					for(var __i = 0; __i < __amount; __i++) {
						grid[# __dir, __i] = __bulletDirection
					}
				} else {
					grid[# __dir, __bulletNumber] = __bulletDirection
				}
				return true
			} else {
				print_error("shotGroupSetDirection: Bullet number '" + string(argument1) + "' exceeded number of bullets in group")
				return false
			}
		} else {
			print_error("shotGroupSetDirection: Object isn't a bullet group")
			return false
		}
	}
} else {
	print_error("shotGroupSetDirection: Bullet group doesn't exist")
	return false
}