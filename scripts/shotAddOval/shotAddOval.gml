/// @function shotAddOval
/// @description shotAddOval
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} width
/// @param {real} height
/// @param {real} drawRotated
/// @param {real} drawRounded

return shotAdd(argument0, argument1, argument2, OVAL, 0, argument3, argument4, argument5, argument6, 0, 0)