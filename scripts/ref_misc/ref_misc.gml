var state = argument0;
// Setup
lua_add_function(state, "PlayFieldSetup", ref_playFieldSetup);
lua_add_function(state, "ShotAutoDeleteEdges", ref_shotAutoDeleteEdges);
lua_add_function(state, "ShotSetDefaultDelay", ref_shotSetDefaultDelay);
lua_add_function(state, "SetPlayerBoundary", ref_setPlayerBoundary);
lua_add_function(state, "SetPlayerSpawn", ref_setPlayerSpawn);

// GUI
lua_add_function(state, "GUISetAnchor", ref_GUISetAnchor);
lua_add_function(state, "GUISetType", ref_GUISetType);
lua_add_function(state, "GUIAddDifficulty", ref_GUIAddDifficulty);
lua_add_function(state, "GUIAddElement", ref_GUIAddElement);
lua_add_function(state, "GUISetBackground", ref_GUISetBackground);

// Get Variables
lua_add_function(state, "GetX", ref_getX);
lua_add_function(state, "GetY", ref_getY);
lua_add_function(state, "GetDir", ref_getDir);
lua_add_function(state, "PlayerX", ref_playerX);
lua_add_function(state, "PlayerY", ref_playerY);
lua_add_function(state, "GetCenterX", ref_getCenterX);
lua_add_function(state, "GetCenterY", ref_getCenterY);
lua_add_function(state, "GetPlayAreaWidth", ref_getPlayAreaWidth);
lua_add_function(state, "GetPlayAreaHeight", ref_getPlayAreaHeight);

// Set Variables
lua_add_function(state, "SetLives", ref_setLives);
lua_add_function(state, "SetBombs", ref_setBombs);
lua_add_function(state, "SetPower", ref_setPower);
lua_add_function(state, "SetContinues", ref_setContinues);