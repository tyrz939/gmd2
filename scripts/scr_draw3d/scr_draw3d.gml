gml_pragma("forceinline")
if scene3d[# 0, 0] != -1 {
	for(var _i = 0; _i < ds_grid_width(scene3d); _i++) {
		switch scene3d[# _i, 0] {
			case d3dTypes.Wall:
				d3d_draw_wall(scene3d[# _i, 1], scene3d[# _i, 2],
							scene3d[# _i, 3], scene3d[# _i, 4],
							scene3d[# _i, 5], scene3d[# _i, 6],
							scene3d[# _i, 7], scene3d[# _i, 8],
							scene3d[# _i, 9]);
				break;
			case d3dTypes.Floor:
				d3d_draw_floor(scene3d[# _i, 1], scene3d[# _i, 2],
							scene3d[# _i, 3], scene3d[# _i, 4],
							scene3d[# _i, 5], scene3d[# _i, 6],
							scene3d[# _i, 7], scene3d[# _i, 8],
							scene3d[# _i, 9]);
				break;
			case d3dTypes.blend:
				gpu_set_blendmode(scene3d[# _i, 1]);
				break;
			case d3dTypes.alpha:
				draw_set_alpha(scene3d[# _i, 1]);
				break;
			case d3dTypes.color:
				draw_set_color(scene3d[# _i, 1]);
				break;
		}
	}
}