/// @function replayParser
/// @description Replay String Parser
/// @param {string} StringFromReplayFile

var __replay = argument0
var _e = false
var _array
var _pos = 0
var _a
for(var _i = 1; _i <= string_length(__replay); _i++) {
	if string_char_at(__replay, _i) == "$" {
		_i++
		_array[_pos, 0] = string_copy(__replay, _i, 2)
		if _array[_pos, 0] != "es" {
			_i += 2
			_a = 0
			_e = false
			while _e == false {
				_a++
				if string_char_at(__replay, _i + _a) == "$" {
					_e = true
				}
			}
			_array[_pos, 1] = string_copy(__replay, _i, _a)
			_i += _a-1
			_pos++
		} else {
			_array[_pos, 1] = "end"
			return _array
		}
	}
}
