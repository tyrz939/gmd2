/// @description  image_stream_finish(string:name)
/// @function  image_stream_finish
/// @param string:name
if (argument_count != 1) return lua_show_error("image_stream_finish: Expected 1 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_stream_finish: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
image_stream_finish(argument0);