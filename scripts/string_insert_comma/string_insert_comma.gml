/// @function string_insert_comma
/// @description string_insert_comma
/// @param {string} String

var str = argument0

var s = string_length(str)

for(var i = s-2; i > 1; i -= 3) {
	str = string_insert(",", str, i)
}

return str