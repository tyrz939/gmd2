/// @function EnemyCreate
/// @description EnemyCreate
/// @param x
/// @param y
/// @param sprite

if (argument_count != 3)  return lua_show_error("EnemyCreate: Expected 3 arguments, got " + string(argument_count));

var inst = instance_create_depth(argument[0], argument[1], ENEMY_DEPTH, obj_enemy)
inst.sprite = argument[2]
return inst