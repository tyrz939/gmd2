// Images
var state = argument0;
lua_add_function(state, "image_system_init", ref_image_system_init);
lua_add_function(state, "image_group_create", ref_image_group_create);
lua_add_function(state, "image_stream_start", ref_image_stream_start);
lua_add_function(state, "image_stream_finish", ref_image_stream_finish);
lua_add_function(state, "image_stream_add", ref_image_stream_add);
lua_add_function(state, "image_stream_add_ext", ref_image_stream_add_ext);
lua_add_function(state, "image_stream_add_3d", ref_image_stream_add_3d);
lua_add_function(state, "image_group_find_image", ref_image_group_find_image);
// Sound
lua_add_function(state, "wav_add", ref_wav_add);
lua_add_function(state, "wav_find", ref_wav_find);