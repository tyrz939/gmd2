/// @function replayInterpreter
/// @description Replay Array Interpreter
/// @param {array} parsedArray
/// @param {real} Type
/// @param {real} [MenuOnly]MenuPos

var _array = argument[0]
var _arrayLength = array_height_2d(_array)
println("Running Interpreter")
var a0, a1
for(var _i = 0; _i < _arrayLength; _i++) {
	a0 = _array[_i, 0]
	a1 = _array[_i, 1]
	if argument[1] == 0 {
		switch a0 {
			case "gc": global.playerNumber =	real(a1); break
			case "gd": global.difficulty =		real(a1); break
			case "gf": game.replayEndFrame =	real(a1); break
			case "gs": game.stageNumber =		real(a1); break
			case "gp": game.stagePosition =		real(a1); break
			case "iw": __replayWidth =			real(a1); break
			case "ii": __replay = ds_grid_create(__replayWidth, 2); ds_grid_read(__replay, a1); break
		}
	}
	if argument[1] == 1 {
		switch a0 {
			case "md": menuGrid[# replyMenu_t.DATE, argument[2]] = a1; break // Date
			case "mt": menuGrid[# replyMenu_t.TIME, argument[2]] = a1; break // Time
			case "gq": menuGrid[# replyMenu_t.SCORE,argument[2]] = real(a1); break // Difficulty
			case "gd": menuGrid[# replyMenu_t.DIFF, argument[2]] = real(a1); break // Difficulty
			case "gc": menuGrid[# replyMenu_t.CHAR, argument[2]] = real(a1); break // Character
			case "ss": menuGrid[# replyMenu_t.SDIR, argument[2]] = a1; break // Script Directory
			case "sf": menuGrid[# replyMenu_t.SFIL, argument[2]] = a1; break // Script File
			case "sn": menuGrid[# replyMenu_t.NAME, argument[2]] = a1; break // Name
			case "sd": menuGrid[# replyMenu_t.DESC, argument[2]] = a1; break // Desc
			case "sa": menuGrid[# replyMenu_t.AUTH, argument[2]] = a1; break // Auth
		}
	}
}