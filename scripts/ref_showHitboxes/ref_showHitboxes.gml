/// @function ShowHitboxes
/// @description ShowHitboxes
/// @param {real} Instance

if (argument_count != 1)  return lua_show_error("ShowHitboxes: Expected 1 arguments, got " + string(argument_count));
showHitboxes(argument[0]);
