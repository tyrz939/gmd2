/// @function d3d_addSetAlpha
/// @description d3d_addSetAlpha
/// @param Alpha

global.scene3dIterator++
var _i = global.scene3dIterator

ds_grid_resize(scene3d, _i+1, 10)

scene3d[# _i, 0] = d3dTypes.alpha
scene3d[# _i, 1] = argument0