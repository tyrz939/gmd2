/// @description  sprite_get_texture(image:index, subimg:number)
/// @function  sprite_get_texture
/// @param image:index
/// @param subimg:number
if (argument_count != 2) return lua_show_error("sprite_get_texture: Expected 2 arguments, got " + string(argument_count));
sprite_get_texture(argument0, argument1);