var state = argument0;
// Move
lua_add_function(state, "MoveTween", ref_moveTween);
lua_add_function(state, "MoveSetTween", ref_moveSetTween);

// Turn
lua_add_function(state, "TurnTween", ref_turnTween);
lua_add_function(state, "TurnSetTween", ref_turnSetTween);

// Turn
lua_add_function(state, "EnemyShotCollisionCircle", ref_enemyShotCollisionCircle);
lua_add_function(state, "EnemyShotCollisionRectangle", ref_enemyShotCollisionRectangle);
lua_add_function(state, "EnemyPlayerCollisionCircle", ref_enemyPlayerCollisionCircle);
lua_add_function(state, "EnemyPlayerCollisionRectangle", ref_enemyPlayerCollisionRectangle);

// Pickups
lua_add_function(state, "PickupCreate", ref_pickupCreate);

// Boss Control
lua_add_function(state, "SpellcardSetup", ref_spellcardSetup);
lua_add_function(state, "NonspellSetup", ref_nonspellSetup);
lua_add_function(state, "HPBarType", ref_HPBarType);

// Create
lua_add_function(state, "BossCreate", ref_bossCreate);
lua_add_function(state, "EnemyCreate", ref_enemyCreate);