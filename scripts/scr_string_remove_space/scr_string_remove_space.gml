/// @function scr_string_remove_space(String, Return_Type)
/// @description Play SFX
/// @param {string} String
/// @param {real} Return_Type

var st = 1;
var ed = 0;
var p = 0;
var chk = false;
var exec = array_create(20, "")
var string_to_convert = string(argument0 + " ")
var commentPos = string_pos("#", string_to_convert)
if commentPos != 0 {
	string_to_convert = string_copy(string_to_convert, 0, commentPos)
}

for(var i = 1; i <= string_length(string_to_convert); i++) {
    if (string_char_at(string_to_convert, i) == " " || string_char_at(string_to_convert, i) == "	") && (string_char_at(string_to_convert, i -1) != " " || string_char_at(string_to_convert, i -1) != "	"){
        chk = true;
    }
    if chk == true {
        chk = false;
        while string_char_at(string_to_convert, st) == " " || string_char_at(string_to_convert, st) == "	" {
            st++;
            ed--;
        }
        exec[p] = string_copy(string_to_convert, st, ed);
        p++;
        i++;
        st = i;
        ed = 0;
    }
    ed++;
}

if argument1 == 0 {
	return exec;	
} else if argument1 == 1 {
	var ret = "";
	for(i = 0; i < array_length_1d(exec); i++) {
		ret += exec[i];
	}
	return ret;
}