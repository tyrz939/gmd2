/// @function shotCreate
/// @description Shot Creater
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 5 {
	if argument[5] != false {
		play_sfx(argument[5], false)
	}
}

// Delay Timer
if argument_count > 6 {global.__shotDelay = argument[6]}
else {global.__shotDelay = global.__shotDefaultDelay}

global.__shotType =			argument[0]
var __shotX =				argument[1]
var __shotY =				argument[2]
global.__shotSpeed =		argument[3]
global.__shotDirection =	argument[4]

// Picks up if you're outside the bounds of the grid
if global.__shotType > global.shotAddIterator {
	if global.shotAddIterator == -1 {
		lua_show_error("shotCreate: Shot Type out of bounds. Trying to create shot "
				+ string(global.__shotType) + " but no shots are available")
	} else {
		lua_show_error("shotCreate: Shot Type out of bounds. Trying to create shot "
				+ string(global.__shotType) + " but only 0-" + string(global.shotAddIterator)
				+ " shots are available")
	}
	return noone
}

global.__shotSpriteIndex =	game.shotCreateGrid[# global.__shotType, shotCreation.sprite]
global.__shotImageIndex =	game.shotCreateGrid[# global.__shotType, shotCreation.subimage]
global.__shotColor =		game.shotCreateGrid[# global.__shotType, shotCreation.color]
global.__shotHitType =		game.shotCreateGrid[# global.__shotType, shotCreation.hitboxType]
global.__shotHitRadius =	game.shotCreateGrid[# global.__shotType, shotCreation.radius]
global.__shothitWidth =		game.shotCreateGrid[# global.__shotType, shotCreation.width]
global.__shothitHeight =	game.shotCreateGrid[# global.__shotType, shotCreation.height]
global.__shotDrawRotated =	game.shotCreateGrid[# global.__shotType, shotCreation.drawRotated]
global.__shotDrawRounded =	game.shotCreateGrid[# global.__shotType, shotCreation.drawRounded]
global.__LaserSpawnSprite =	game.shotCreateGrid[# global.__shotType, shotCreation.spawnSprite]
global.__LaserSpawnSubimage=game.shotCreateGrid[# global.__shotType, shotCreation.spawnSubimage]

var __depth = image_get_width(global.__shotSpriteIndex) + image_get_height(global.__shotSpriteIndex)

// Create Shot
var t = global.__shotCreateType
global.__shotCreateType = -1
switch t {
	case bulletType_t.shotGroup:		return instance_create_depth(__shotX, __shotY, SHOT_DEPTH + __depth, obj_bulletGroup)
	case bulletType_t.laserGroup:		return instance_create_depth(__shotX, __shotY, SHOT_DEPTH + __depth, obj_cLaserGroup)
	case bulletType_t.staticLaserGroup:	return instance_create_depth(__shotX, __shotY, SHOT_DEPTH + __depth, obj_sLaser)
	case bulletType_t.delayLaser:		return instance_create_depth(__shotX, __shotY, SHOT_DEPTH + __depth, obj_delayLaser)
	default:							return instance_create_depth(__shotX, __shotY, SHOT_DEPTH + __depth, obj_bullet)
}