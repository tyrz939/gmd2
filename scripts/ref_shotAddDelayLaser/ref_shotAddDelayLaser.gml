/// @function ShotAddDelayLaser
/// @description ShotAddDelayLaser
/// @param {real} sprite
/// @param {real} subimage

if (argument_count != 2)  return lua_show_error("ShotAddDelayLaser: Expected 2 arguments, got " + string(argument_count));
return shotAddDelayLaser(argument[0], argument[1]);