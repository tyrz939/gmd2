/// @function shotAddCircle
/// @description shotAddCircle
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} radius
/// @param {real} drawRotated
/// @param {real} drawRounded

if (argument_count != 6)  return lua_show_error("shotAddCircle: Expected 6 arguments, got " + string(argument_count));
return shotAddCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5]);