/// @description MOD_GetChannelVolume(channel)
/// @function MOD_GetChannelVolume
/// @param channel

return ModPlug_GetChannelVolume(argument0);