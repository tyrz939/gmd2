/// @function GUISetAnchor
/// @description GUISetAnchor
/// @param {real} x
/// @param {real} y

if (argument_count != 2)  return lua_show_error("GUISetAnchor: Expected 2 arguments, got " + string(argument_count));

obj_GUI.anchX = argument[0]
obj_GUI.anchY = argument[1]