/******************************************************************
    Generates the global particle types for the particle system!
******************************************************************/

// Start
global.partSys = part_system_create()
part_system_depth(global.partSys, SHOT_DEPTH + 1 );

/**********************
	Particle Types
**********************/

	// Shot die
	global.partShotDie = part_type_create();
	part_type_sprite(global.partShotDie, spr_despawn1, true, true, false);
	part_type_size(global.partShotDie,1,1,0,0);
	part_type_scale(global.partShotDie,0.5,0.5);
	part_type_alpha1(global.partShotDie,0.75);
	part_type_speed(global.partShotDie,0,0,0,0);
	part_type_direction(global.partShotDie,0,0,0,0);
	part_type_orientation(global.partShotDie,0,0,0,0,1);
	part_type_blend(global.partShotDie,true);
	part_type_life(global.partShotDie,16,16);
	
		// Shot die
	global.partPlayerDie = part_type_create();
//	part_type_sprite(global.partPlayerDie, spr_despawn1, true, true, false);
	part_type_shape(global.partPlayerDie, pt_shape_sphere);
	part_type_size(global.partPlayerDie,1,1,0.5,0);
	part_type_scale(global.partPlayerDie,1,1);
	part_type_alpha1(global.partPlayerDie,1);
	part_type_speed(global.partPlayerDie,0,0,0,0);
	part_type_direction(global.partPlayerDie,0,0,0,0);
	part_type_orientation(global.partPlayerDie,0,0,0,0,0);
	part_type_blend(global.partPlayerDie,true);
	part_type_life(global.partPlayerDie,10,10);