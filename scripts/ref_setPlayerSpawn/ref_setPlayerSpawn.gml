/// @function SetPlayerSpawn
/// @description SetPlayerSpawn

if (argument_count != 4)  return lua_show_error("SetPlayerSpawn: Expected 4 arguments, got " + string(argument_count));

game.playerSpawnX = argument[0]
game.playerSpawnY = argument[1]
game.playerGoToX = argument[2]
game.playerGoToY = argument[3]