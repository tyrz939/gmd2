/// @function shotAutoDeleteEdges
/// @description Shot Creater
/// @param {real} left
/// @param {real} top
/// @param {real} right
/// @param {real} bottom

global.shotDeleteLeft = -argument0
global.shotDeleteTop = -argument1
global.shotDeleteRight = argument2 + global.playAreaWidth
global.shotDeleteBottom = argument3 + global.playAreaHeight