gml_pragma( "global", "config();");
gml_pragma( "global", "__init_d3d();");
//gml_pragma( "PNGCrush")
//gml_release_mode(true);
layer_force_draw_depth(true, 0); // force all layers to draw at depth 0

// Misc
global.name = "GameMaker Danmaku 2"

global.debug = false

// Replay Path
global.replayPath = "replay/"

// Global Variables
global.playerNumber = -1
global.difficulty = -1

// For Sound
global.prevSfx = -1

// Graphic Settings
gpu_set_tex_mip_bias(0);
gpu_set_tex_min_mip(0);
gpu_set_tex_max_mip(4);
draw_set_circle_precision(64);

// Menu Config
global.menuSpacing = 40
global.menuPulseColor = make_color_rgb(255, 95, 63)
global.menuTextColor = c_ltgray
global.menuGrayColor = c_dkgray
global.menuPulse = false

// Lua
global.__prevLuaError = ""

// Pickup item enum
enum pickup_t {
	POWER,
	POWER8,
	STAR,
	LIFE,
	BOMB,
	FULL,
	SCORE
}

// Bullet Create Type enum
enum bulletType_t {
	shotGroup,
	laserGroup,
	staticLaserGroup,
	delayLaser
}

// Depths
#macro BG_DEPTH 16000
#macro DEATH_EFFECT_DEPTH 9000
#macro BOMB_DEPTH 900
#macro ENEMY_DEPTH 880
#macro PLAYER_DEPTH 860
#macro PLAYER_SHOT_DEPTH 840
#macro PLAYER_ORB_DEPTH 820
#macro PLAYER_LASER_DEPTH 800
#macro PICKUP_DEPTH 780
#macro SHOT_DEPTH 10
#macro HITBOX_DEPTH 5
#macro BOSS_HP_DEPTH 2

#macro LOAD_DEPTH -100
#macro BOSS_INFO_DEPTH -100
#macro INTERFACE_DEPTH -200

// Delete Type
#macro DELTYPE_INSTANT 0
#macro DELTYPE_ANIMATED 1
#macro DELTYPE_ITEM 2

// Hitbox Type
#macro CIRCLE 0
#macro OVAL 1
#macro RECTANGLE 2
#macro LASER 3
#macro STATICLASER 4