enum d3dTypes {
	Wall,
	Floor,
	blend,
	alpha,
	color
}

surface3d = noone
xx = 0
yy = 0
zz = 0
yaw = 248
pitch = 25
fogColor = c_black
backColor = c_black
fogStart = 1024
fogEnd = 4096
	
global.scene3dIterator = -1
scene3d = ds_grid_create(1, 10)
ds_grid_set_region(scene3d, 0, 0, 10, 0, -1)