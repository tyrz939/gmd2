/// @function EnemyPlayerCollisionRectangle
/// @description EnemyPlayerCollisionRectangle
/// @param {real} Instance
/// @param {real} Width
/// @param {real} Height

if (argument_count != 3)  return lua_show_error("EnemyPlayerCollisionRectangle: Expected 3 arguments, got " + string(argument_count));

return enemyPlayerCollisionRectangle(argument[0], argument[1], argument[2])