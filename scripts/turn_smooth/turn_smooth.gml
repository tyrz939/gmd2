/// @description  turn_smooth( angle, desired angle, speed );
/// @function  turn_smooth
/// @param Angle
/// @param Desired_Angle
/// @param Speed
return(argument0 + max(min((((argument1  - argument0 + 540) mod 360) - 180), argument2), -1 * argument2))