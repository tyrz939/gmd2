/// @function SetPlayerBoundary
/// @description SetPlayerBoundary

if (argument_count != 4)  return lua_show_error("SetPlayerBoundary: Expected 4 arguments, got " + string(argument_count));

game.boundaryLeft = argument[0]
game.boundaryRight = argument[1]
game.boundaryTop = argument[2]
game.boundaryBottom = argument[3]