/// @function shotGroupReplaceAll
/// @description shotGroupReplaceAll
/// @param {real} instance
/// @param {real} Type

var __groupID = argument[0], __type = argument[1]

if instance_exists(__groupID) {
	with __groupID {
		if object_index == obj_bulletGroup {
			var __c, __i, __inst = ShotCreateCircle(__type, x, y, 0, 0, __amount, 0)
			for(__i = 0; __i < __amount; __i++) {
				for(var __c = 0; __c < ds_grid_width(grid); __c++) {
					__inst.grid[# __c, __i] = grid[# __c, __i]
				}
			}
			__inst.timerStep = timerStep
			instance_destroy()
			return __inst
		} else {
			print_error("shotGroupReplaceAll: Object isn't a bullet group")
			return false
		}
	}
} else {
	print_error("shotGroupReplaceAll: Bullet group doesn't exist")
	return false
}