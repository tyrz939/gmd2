if !instance_exists(obj_drawHitboxes) && argument0 {
	instance_create_depth(0,0,-1000,obj_drawHitboxes)
	println("Showing Hitboxes")
}
	
if !argument0 {
	with obj_drawHitboxes {
		println("Hiding Hitboxes")
		instance_destroy()
	}
}