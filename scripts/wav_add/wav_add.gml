/// @function wav_add
/// @description wav_add
/// @param {real} wav
/// @param {string} tag

var wavBuffer
if file_exists_ns(argument0) {
	wavBuffer = buffer_load_ns(argument0)

	// RIFF Type Check
	buffer_seek(wavBuffer, buffer_seek_start, 8)
	var fileType = ""
	repeat(4) {fileType += ansi_char(buffer_read(wavBuffer, buffer_u8))}
	if fileType != "WAVE" {
		println("Sound isn't WAVE: " + string(argument0))
		return -1
	}
	//println(fileType)
	
	// Compression Check (1 = PCM (no compression))
	buffer_seek(wavBuffer, buffer_seek_start, 20)
	var isPCM = buffer_read(wavBuffer, buffer_u8)
	if isPCM != 1 {
		println("Sound isn't PCM: " + string(argument0))
		return -1
	}
	//println(isPCM)
	
	// Sound Channels
	buffer_seek(wavBuffer, buffer_seek_start, 22)
	var channels = buffer_read(wavBuffer, buffer_u8) -1 // -1 to match gamemakers audio_mono/audio_stereo (0/1)
	//println(channels)
	
	// Sample Rate
	buffer_seek(wavBuffer, buffer_seek_start, 24)
	var sampleRate = buffer_read(wavBuffer, buffer_u8)
	sampleRate |= buffer_read(wavBuffer, buffer_u8) <<8
	sampleRate |= buffer_read(wavBuffer, buffer_u8) <<16
	sampleRate |= buffer_read(wavBuffer, buffer_u8) <<24
	//println(sampleRate)
	
	// Sound Data Length
	buffer_seek(wavBuffer, buffer_seek_start, 40)
	var dataLength = buffer_read(wavBuffer, buffer_u8)
	dataLength |= buffer_read(wavBuffer, buffer_u8) <<8
	dataLength |= buffer_read(wavBuffer, buffer_u8) <<16
	dataLength |= buffer_read(wavBuffer, buffer_u8) <<24
	//println(dataLength)
	
	buffer_seek(wavBuffer, buffer_seek_start, 34)
	var bitsPerSample
	if buffer_read(wavBuffer, buffer_u8) == 16	{bitsPerSample = buffer_s16}
	else										{bitsPerSample = buffer_u8}
	
	// Saving the buffer to the sound ds_grid
	ds_grid_resize(soundGrid, 3, ds_grid_height(soundGrid) +1)
	soundGrid[# 0, ds_grid_height(soundGrid)-1] = wavBuffer
	soundGrid[# 1, ds_grid_height(soundGrid)-1] = audio_create_buffer_sound(wavBuffer, bitsPerSample, sampleRate, 44, dataLength, channels)
	soundGrid[# 2, ds_grid_height(soundGrid)-1] = argument1
	
	return true
} else {
	println("Sound Doesn't Exists: " + string(argument0))
	return -1
}