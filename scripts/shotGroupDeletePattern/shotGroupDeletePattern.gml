/// @function shotGroupDeletePattern
/// @description shotGroupDeletePattern
/// @param {real} instance
/// @param {real} live
/// @param {real} delete

var __groupID = argument0

if instance_exists(__groupID) {
	with __groupID {
		if object_index == obj_bulletGroup || object_index == obj_cLaserGroup {
			var __live = 0, __delete = argument2
			for(var __i = 0; __i < __amount; __i++) {
				if __live > 0 {
					__live--
					if __live == 0 {__delete = argument2}
				} else if __delete > 0 {
					grid[# __alive, __i] = false
					__amountLeft--
					__delete--
					if __delete == 0 {__live = argument1}
				}
			}
			return true
		} else {
			print_error("shotGroupDeletePattern: Object isn't a bullet group")
			return false
		}
	}
} else {
	print_error("shotGroupDeletePattern: Bullet group doesn't exist")
	return false
}