/// @function shotGroupSetDirection
/// @description shotGroupSetDirection
/// @param {real} instance
/// @param {real} bulletNumber
/// @param {real} direction

if (argument_count != 3)  return lua_show_error("shotGroupSetDirection: Expected 3 arguments, got " + string(argument_count));

return shotGroupSetDirection(argument[0], argument[1], argument[2]);