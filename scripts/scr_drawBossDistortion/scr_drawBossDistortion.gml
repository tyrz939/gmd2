gml_pragma("forceinline")

if instance_exists(obj_boss) {
	var surfacePoints = surface_create(global.playAreaWidth*global.backgroundScale, global.playAreaHeight*global.backgroundScale)
	surface_set_target(surfacePoints)
	draw_clear_alpha(c_black, 1);
	
	gpu_set_blendmode(bm_add);
	with obj_boss {
		if distortion {
			draw_sprite_ext(spr_boss_distortion_bg, 0, x*global.backgroundScale, y*global.backgroundScale, global.backgroundScale*distortionScale, global.backgroundScale*distortionScale, 0, c_white, 1)
		}
	}
	gpu_set_blendmode(bm_normal);
	
	surface_reset_target();
	if !surface_exists(surfaceDistortion) {
		surfaceDistortion = surface_create(global.playAreaWidth*global.backgroundScale, global.playAreaHeight*global.backgroundScale)
	}
	surface_set_target(surfaceDistortion)
	draw_clear_alpha(0, 0);
	
	//Draw app surf
	shader_set(sh_haze);

	shader_set_uniform_f(uniTime, frame*20);
	gpu_set_texrepeat_ext(uniSamp, true);
	var NTex = sprite_get_texture(spr_haze_noise, 0);
	texture_set_stage(uniSamp, NTex);
	shader_set_uniform_f(uniSampSize, texture_get_width(NTex), texture_get_height(NTex));

	shader_set_uniform_f(uniSpeed, 1/100);
	shader_set_uniform_f(uniSize, 4/1000);
	shader_set_uniform_f(uniFreq, 4/2.5);

	draw_surface(surface3d, 0, 0);

	shader_reset();

	//Points
	shader_set(sh_mask);
	gpu_set_blendmode_ext(bm_zero, bm_src_color);
	draw_surface(surfacePoints, 0, 0);
	gpu_set_blendmode(bm_normal);
	shader_reset();
	surface_reset_target()
	surface_free(surfacePoints);
	
	surface_set_target(surface3d)
	gpu_set_blendmode_ext(bm_src_alpha_sat, bm_inv_src_alpha);
	draw_surface(surfaceDistortion, 0, 0);
	gpu_set_blendmode(bm_add);
	draw_surface(surfaceDistortion, 0, 0);
	gpu_set_blendmode(bm_normal);
	surface_reset_target()
}