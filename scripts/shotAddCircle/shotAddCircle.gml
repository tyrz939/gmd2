/// @function shotAddCircle
/// @description shotAddCircle
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} radius
/// @param {real} drawRotated
/// @param {real} drawRounded

return shotAdd(argument0, argument1, argument2, CIRCLE, argument3, 0, 0, argument4, argument5, 0, 0)