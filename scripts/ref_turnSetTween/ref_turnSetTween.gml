/// @function TurnSetTween
/// @description moveTween
/// @param {real} TweenType

if (argument_count != 1)  return lua_show_error("TurnSetTween: Expected 1 arguments, got " + string(argument_count));
var t = tweenCheck(argument0)
if t != false {global.tweenTurn = t}
else {lua_show_error("TurnSetTween: No Tween of type '" + string(argument0) + "' exists");}