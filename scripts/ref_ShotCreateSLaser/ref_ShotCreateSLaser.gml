/// @function shotCreateSLaser
/// @description Static Laser
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Direction
/// @param {real} Length
/// @param {real} Width
/// @param {real} Duration
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

var __ac = argument_count
if !in_range(__ac, 7, 9) return lua_show_error("ShotCreateSLaser: Expected 7-9 arguments, got " + string(__ac));

if __ac == 7 {
	return ShotCreateSLaser(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6]);
} else if __ac == 8 {
	return ShotCreateSLaser(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7]);
} else if __ac == 9 {
	return ShotCreateSLaser(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8]);
}