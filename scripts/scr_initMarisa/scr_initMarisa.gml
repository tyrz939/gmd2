playerOrbs = game.marisaOrb
playerOrbSubimage = 0
playerHitbox = spr_hitbox
spriteAnimationType = 0
//global.shotPowerMax = 4
//moveSpeed = 5
//bombSpeedPenalty = 0.2
//focusSpeed = 2
//shotCD = 3

// Player

characterName = "Marisa Kirisame"
playerDeathColor = c_blue
shotPowerMax = 4
orbArray = array_create(shotPowerMax, noone)
moveSpeed = 5
bombSpeedPenalty = 0.2
focusSpeed = 2
shotDelay = 3
shotCounter = 0
shotDelayAlt = 12
shotCounterAlt = 0

game.hitboxRadius = 2
game.grazeRadius = 15
		
// Step event script
stepScript = scr_stepMarisa
		
// Orbs
// 2d Array of orb positions
var orb1 = 0, orb2 = 1, orb3 = 2, orb4 = 3
var pow1 = 0, pow2 = 1, pow3 = 2, pow4 = 3
	// Unfocused
	orbX[pow1, orb1] = 0;
	orbX[pow2, orb1] = -16;		orbX[pow2, orb2] = 16;
	orbX[pow3, orb1] = -32;		orbX[pow3, orb2] = 32;		orbX[pow3, orb3] = 0;
	orbX[pow4, orb1] = -32;		orbX[pow4, orb2] = 32;		orbX[pow4, orb3] = -16;	orbX[pow4, orb4] = 16;

	orbY[pow1, orb1] = -32;
	orbY[pow2, orb1] = -32;		orbY[pow2, orb2] = -32;
	orbY[pow3, orb1] = 0;		orbY[pow3, orb2] = 0;		orbY[pow3, orb3] = -32;
	orbY[pow4, orb1] = 0;		orbY[pow4, orb2] = 0;		orbY[pow4, orb3] = -32;	orbY[pow4, orb4] = -32;
	// Focused
	forbX[pow1, orb1] = 0;
	forbX[pow2, orb1] = -8;	forbX[pow2, orb2] = 8;
	forbX[pow3, orb1] = -16;	forbX[pow3, orb2] = 16;		forbX[pow3, orb3] = 0;
	forbX[pow4, orb1] = -24;	forbX[pow4, orb2] = 24;		forbX[pow4, orb3] = -8;	forbX[pow4, orb4] = 8;

	forbY[pow1, orb1] = -24;
	forbY[pow2, orb1] = -24;	forbY[pow2, orb2] = -24;
	forbY[pow3, orb1] = -24;	forbY[pow3, orb2] = -24;	forbY[pow3, orb3] = -32;
	forbY[pow4, orb1] = -24;	forbY[pow4, orb2] = -24;	forbY[pow4, orb3] = -32;forbY[pow4, orb4] = -32;
	// Laser Direction
	dirL[pow1, orb1] = 90;
	dirL[pow2, orb1] = 90;	dirL[pow2, orb2] = 90;
	dirL[pow3, orb1] = 105;	dirL[pow3, orb2] = 75;	dirL[pow3, orb3] = 90;
	dirL[pow4, orb1] = 105;	dirL[pow4, orb2] = 75;	dirL[pow4, orb3] = 90;	dirL[pow4, orb4] = 90;

/**********************
	Particle Types
**********************/

	// Shot1 die
	global.partMarisaShot1Die = part_type_create();
	part_type_shape(global.partMarisaShot1Die, pt_shape_line);
	part_type_size(global.partMarisaShot1Die,1,1,0,0);
	part_type_scale(global.partMarisaShot1Die,1,1);
	part_type_alpha1(global.partMarisaShot1Die,1);
	part_type_speed(global.partMarisaShot1Die,5,5,0,0);
	part_type_direction(global.partMarisaShot1Die,90,90,0,0);
	part_type_orientation(global.partMarisaShot1Die,0,0,0,0,0);
	part_type_blend(global.partMarisaShot1Die,true);
	part_type_life(global.partMarisaShot1Die,12,12);

	// Shot2 die
	global.partMarisaShot2Die = part_type_create();
	part_type_shape(global.partMarisaShot2Die, pt_shape_line);
	part_type_size(global.partMarisaShot2Die,1,1,0,0);
	part_type_scale(global.partMarisaShot2Die,1,1);
	part_type_alpha1(global.partMarisaShot2Die,0.5);
	part_type_speed(global.partMarisaShot2Die,0,0,0,0);
	part_type_direction(global.partMarisaShot2Die,0,0,0,0);
	part_type_orientation(global.partMarisaShot2Die,0,0,0,0,0);
	part_type_blend(global.partMarisaShot2Die,true);
	part_type_life(global.partMarisaShot2Die,16,16);

	// Laser Impact
	global.partMarisaLaserImpact = part_type_create();
	part_type_shape(global.partMarisaLaserImpact, pt_shape_line);
	part_type_size(global.partMarisaLaserImpact,1,1,0,0);
	part_type_scale(global.partMarisaLaserImpact,1,1);
	part_type_alpha1(global.partMarisaLaserImpact,1);
	part_type_speed(global.partMarisaLaserImpact,5,5,0,0);
	part_type_direction(global.partMarisaLaserImpact,90,90,0,0);
	part_type_orientation(global.partMarisaLaserImpact,0,0,0,0,1);
	part_type_blend(global.partMarisaLaserImpact,true);
	part_type_life(global.partMarisaLaserImpact,12,12);