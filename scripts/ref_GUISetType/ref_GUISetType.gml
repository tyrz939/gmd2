/// @function GUISetType
/// @description GUISetType
/// @param {real} x
/// @param {real} y

if (argument_count != 1)  return lua_show_error("GUISetType: Expected 1 arguments, got " + string(argument_count));

GUIAddElement(argument[0], 0, 0, 0);