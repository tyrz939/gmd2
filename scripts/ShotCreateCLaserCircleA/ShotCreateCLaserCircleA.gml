/// @function shotCreateCircleA
/// @description Creates a circle of shots with Acceleration/Deceleration
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} StepSpeedChange
/// @param {real} FinalSpeed
/// @param {real} [opt]SpeedChangeDelay
/// @param {real} [opt]Sound

// Sound
if argument_count > 12 {
	if argument[12] != false {
		play_sfx(argument[12], false)
	}
}

var __inst = ShotCreateCLaserCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], false)
with __inst {
	for(var i = 0; i < __amount; i++) {
		grid[# __accelStepSpeed, i] = argument[9]
		grid[# __accelFinalSpeed, i] = argument[10]
		
		// Work out how long it will take
		grid[# __accelTime, i] = abs((argument[3] - grid[# __accelFinalSpeed, i]) / grid[# __accelStepSpeed, i])
		
		// Accelerating, 1. Decelerating 2
		if grid[# __accelFinalSpeed, i] > grid[# __spd, i] {
			grid[# __accelState, i] = 1
		} else {
			grid[# __accelState, i] = 2
		}
		// Delay timer before starting
		if argument_count > 11 {
			grid[# __accelDelay, i] = argument[11]
		} else {
			grid[# __accelDelay, i] = 0
		}
	}
}
return __inst