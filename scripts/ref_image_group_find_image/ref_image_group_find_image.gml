/// @description  image_group_find_image(string:name, string:id)
/// @function  image_group_find_image
/// @param string:name
/// @param  string:id
if (argument_count != 2) return lua_show_error("image_group_find_image: Expected 2 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_group_find_image: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
if !(is_string(argument1)) return lua_show_error("image_group_find_image: Expected a string for argument1 (string:id), got " + lua_print_value(argument1));

for(var i = 0; i < ds_grid_height(importGrid); i++) {
	if importGrid[# 1, i] == argument0 && importGrid[# 2, i] == argument1 {
		return importGrid[# 0, i]
	}
}
return -1

//return image_group_find_image(argument0, argument1);
