/// @function scr_rebind_return(Key, Conflict Check)
/// @description Check for conflicts when rebinding keys
/// @param {real} Key
/// @param {real} Conflict Check

var r = "2";
var convert = false;

var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghi`";

var c = chr(argument0);
for( var i = 0; i <= string_length(chars); i++ ) {
    if string_char_at(chars, i) == c {
        r = c;
        if i >= 36 {
            convert = true;
        }
    }
}

if convert {
    if r == "a" r = "NP 1";
    if r == "b" r = "NP 2";
    if r == "c" r = "NP 3";
    if r == "d" r = "NP 4";
    if r == "e" r = "NP 5";
    if r == "f" r = "NP 6";
    if r == "g" r = "NP 7";
    if r == "h" r = "NP 8";
    if r == "i" r = "NP 9";
    if r == "`" r = "NP 0";
}

if argument0 == vk_space r = "Space Bar";
if argument0 == vk_shift r = "Shift";
if argument0 == vk_alt r = "Alt";
if argument0 == vk_control r = "Ctrl";

if !argument1 {
    return r;
} else {
    if global.Up == argument0 r = "1";
    if global.Down == argument0 r = "1";
    if global.Left == argument0 r = "1";
    if global.Right == argument0 r = "1";
    if global.B1 == argument0 r = "1";
    if global.B2 == argument0 r = "1";
    if global.B3 == argument0 r = "1";
    if global.B4 == argument0 r = "1";
    return r;
}