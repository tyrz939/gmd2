/// @description  image_system_init()
/// @function  image_system_init
if (argument_count != 0) return lua_show_error("image_system_init: Expected 0 arguments, got " + string(argument_count));
image_system_init();

