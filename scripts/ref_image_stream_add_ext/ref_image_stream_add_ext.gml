/// @description  image_stream_add_ext(string:name, string:id, string:file, subimage:number, xorig:number, yorig:number, xstart:number, ystart:number, width:number, height:number, frames_per_row:number, xsep:number, ysep:number)
/// @function  image_stream_add_ext
/// @param string:name
/// @param  string:id
/// @param  string:file
/// @param  subimage:number
/// @param  xorig:number
/// @param  yorig:number
/// @param  xstart:number
/// @param  ystart:number
/// @param  width:number
/// @param  height:number
/// @param  frames_per_row:number
/// @param  xsep:number
/// @param  ysep:number
if (argument_count != 13) return lua_show_error("image_stream_add_ext: Expected 13 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_stream_add_ext: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
if !(is_string(argument1)) return lua_show_error("image_stream_add_ext: Expected a string for argument1 (string:id), got " + lua_print_value(argument1));
if !(is_string(argument2)) return lua_show_error("image_stream_add_ext: Expected a string for argument2 (string:file), got " + lua_print_value(argument2));
if !(is_real(argument3) || is_int64(argument3)) return lua_show_error("image_stream_add_ext: Expected a number for argument3 (subimage:number), got " + lua_print_value(argument3));
if !(is_real(argument4) || is_int64(argument4)) return lua_show_error("image_stream_add_ext: Expected a number for argument4 (xorig:number), got " + lua_print_value(argument4));
if !(is_real(argument5) || is_int64(argument5)) return lua_show_error("image_stream_add_ext: Expected a number for argument5 (yorig:number), got " + lua_print_value(argument5));
if !(is_real(argument6) || is_int64(argument6)) return lua_show_error("image_stream_add_ext: Expected a number for argument6 (xstart:number), got " + lua_print_value(argument6));
if !(is_real(argument7) || is_int64(argument7)) return lua_show_error("image_stream_add_ext: Expected a number for argument7 (ystart:number), got " + lua_print_value(argument7));
if !(is_real(argument8) || is_int64(argument8)) return lua_show_error("image_stream_add_ext: Expected a number for argument8 (width:number), got " + lua_print_value(argument8));
if !(is_real(argument9) || is_int64(argument9)) return lua_show_error("image_stream_add_ext: Expected a number for argument9 (height:number), got " + lua_print_value(argument9));
if !(is_real(argument10) || is_int64(argument10)) return lua_show_error("image_stream_add_ext: Expected a number for argument10 (frames_per_row:number), got " + lua_print_value(argument10));
if !(is_real(argument11) || is_int64(argument11)) return lua_show_error("image_stream_add_ext: Expected a number for argument11 (xsep:number), got " + lua_print_value(argument11));
if !(is_real(argument12) || is_int64(argument12)) return lua_show_error("image_stream_add_ext: Expected a number for argument12 (ysep:number), got " + lua_print_value(argument12));

global.__importScript = 2
importAddQueue(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7, argument8, argument9, argument10, argument11, argument12, 0, 0, 0);