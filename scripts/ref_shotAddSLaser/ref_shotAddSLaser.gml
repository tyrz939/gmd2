/// @function shotAddSLaser
/// @description shotAddshotAddSLaser
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} Width (hitbox)
/// @param {real} spawnSprite
/// @param {real} spawnSubimage

if (argument_count != 6)  return lua_show_error("shotAddSLaser: Expected 6 arguments, got " + string(argument_count));
return shotAddSLaser(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5]);