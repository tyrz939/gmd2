/// @function shotSetBlend
/// @description shotSetBlend
/// @param {real} Instance
/// @param {real} BlendType

if instance_exists(argument0) {
	with argument0 {
		if object_get_parent(object_index) != obj_bulletParent {
			print_error("ShotSetBlend: Instance isn't a Bullet")
			return false
		}
		blendType = argument1
		return true
	}
}
return print_error("ShotSetBlend: Bullet doesn't exist")