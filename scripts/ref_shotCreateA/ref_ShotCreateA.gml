/// @description  ShotCreateA
/// @function  ShotCreateA
/// @param  Type:number
/// @param  x:number
/// @param  y:number
/// @param  Speed:number
/// @param  Direction:number
/// @param  stepSpeedChange:number
/// @param  finalSpeed:number
/// @param  speedChangeDelay:number
/// @param  Sound:number
/// @param  Delay:number
var __ac = argument_count
if !in_range(__ac, 7, 10) return lua_show_error("ShotCreateA: Expected 8-11 arguments, got " + string(__ac));

if __ac == 7 {
	return ShotCreateA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6]);
} else if __ac == 8 {
	return ShotCreateA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7]);
} else if __ac == 9 {
	return ShotCreateA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8]);
} else if __ac == 10 {
	return ShotCreateA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9]);
}