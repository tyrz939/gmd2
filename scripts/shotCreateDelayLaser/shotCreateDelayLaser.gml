/// @function shotCreateDelayLaser
/// @description shotCreateDelayLaser
/// @param {real} Type
/// @param {real} Length
/// @param {real} Width
/// @param {real} Bullet
/// @param {real} Delay

var __shotType = argument[0]
global.__LaserLength = argument[1]
global.__LaserWidth = argument[2]
var __bullet = argument[3]
var __delayTime = argument[4]

global.__shotCreateType = bulletType_t.delayLaser
var __inst = ShotCreate(__shotType, 0, 0, 0, 0, false, 0)

if __bullet.object_index == obj_bulletGroup {
	with __inst {
		ds_grid_resize(lasers, 3, __bullet.__amount)
		for(var i = 0; i < __bullet.__amount; i++) {
			lasers[# 0, i] = __bullet.grid[# __bullet.__xx, i]
			lasers[# 1, i] = __bullet.grid[# __bullet.__yy, i]
			lasers[# 2, i] = __bullet.grid[# __bullet.__dir, i]
		}
	}
} else if __bullet.object_index == obj_cLaserGroup {
	with __inst {
		ds_grid_resize(lasers, 3, __bullet.__amount)
		for(var i = 0; i < __bullet.__amount; i++) {
			lasers[# 0, i] = __bullet.grid[# __bullet.__gridWidth - __bullet._stepSize, i]
			lasers[# 1, i] = __bullet.grid[# __bullet.__gridWidth - __bullet._stepSize+__bullet.__yy, i]
			lasers[# 2, i] = __bullet.grid[# __bullet.__dir, i]
		}
	}
} else {
	with __inst {
		lasers[# 0, 0] = __bullet.x
		lasers[# 1, 0] = __bullet.y
		lasers[# 2, 0] = __bullet.direction
	}
}

with __inst {
	grid[# 0, 0] = __bullet
	grid[# 1, 0] = __delayTime
	instance_deactivate_object(__bullet)
}
return __inst