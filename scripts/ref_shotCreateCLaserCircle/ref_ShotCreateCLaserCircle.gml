/// @function ShotCreateCLaserCircle
/// @description Creates a circle of lasers
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound

var __ac = argument_count
if !in_range(__ac, 9, 10) return lua_show_error("ShotCreateCLaserCircle: Expected 9-10 arguments, got " + string(__ac));

if __ac == 9 {
	return ShotCreateCLaserCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8]);
} else if __ac == 10 {
	return ShotCreateCLaserCircle(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9]);
}