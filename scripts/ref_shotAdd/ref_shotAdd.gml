/// @function shotAdd
/// @description shotAdd
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color
/// @param {real} hitboxType
/// @param {real} radius
/// @param {real} width
/// @param {real} height
/// @param {real} drawRotated
/// @param {real} drawRounded

if (argument_count != 11)  return lua_show_error("shotAdd: Expected 11 arguments, got " + string(argument_count));
return shotAdd(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10]);