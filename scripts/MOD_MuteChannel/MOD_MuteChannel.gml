/// @description MOD_MuteChannel(channel, mute)
/// @function MOD_MuteChannel
/// @param channel
/// @param mute

return ModPlug_MuteChannel(argument0, argument1);