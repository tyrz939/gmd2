/// @function d3d_addSetBlend
/// @description d3d_addSetBlend
/// @param Blend

global.scene3dIterator++
var _i = global.scene3dIterator

ds_grid_resize(scene3d, _i+1, 10)

scene3d[# _i, 0] = d3dTypes.blend
scene3d[# _i, 1] = argument0