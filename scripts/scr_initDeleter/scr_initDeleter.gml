enum bulletRemover_t {
	HITBOX,
	X,
	Y,
	RADIUS,
	WIDTH,
	HEIGHT,
	ANGLE,
	COLOR,
	TIMER,
	TYPE,
	DELETE_TYPE,
	PARENT_ID,
	ID,
	ALIVE,
	LENGTH
}

brIDnext = 1

bulletRemover = ds_grid_create(1, bulletRemover_t.LENGTH)

// Player Hitbox
bulletRemover[# 0, bulletRemover_t.ALIVE] = 1
bulletRemover[# 0, bulletRemover_t.COLOR] = -1
bulletRemover[# 0, bulletRemover_t.DELETE_TYPE] = -1
bulletRemover[# 0, bulletRemover_t.ID] = -1
bulletRemover[# 0, bulletRemover_t.PARENT_ID] = -1