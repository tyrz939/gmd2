/// @function shotCreateA
/// @description Creates a shot with Acceleration/Deceleration
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} StepSpeedChange
/// @param {real} FinalSpeed
/// @param {real} [opt]SpeedChangeDelay
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 8 {
	if argument[8] != false {
		play_sfx(argument[8], false)
	}
}
// Delay Timer
if argument_count > 9 {
	global.__shotDelay = argument[9]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}
var __inst = ShotCreate(argument[0], argument[1], argument[2], argument[3], argument[4], false, global.__shotDelay)
with __inst {
	accelStepSpeed = argument[5]
	accelFinalSpeed = argument[6]
	accelTime = abs((argument[3] - accelFinalSpeed) / accelStepSpeed)
	if accelFinalSpeed > spd {
		accelState = 1
	} else {
		accelState = 2
	}
	if argument_count > 7 {
		accelDelay = argument[7]
	} else {
		accelDelay = 0
	}
}
return __inst