//Options
global.music_vol = 1
global.sfx_vol = 0.8
global.fullscreen = false
global.vsync = true

global.textureFilterMode = tf_linear
global.afLevel = 1
global.upscale = false
global.scalingTypeGame = 0
global.scalingTypeGUI = 0
global.backgroundScale = 1

global.res_x = 640
global.res_y = 480
global.window_width = 1280
global.window_height = 960
global.gui_width = 1280
global.gui_height = 960
global.showfps = false
//Key Binds
global.Up = ord("W")
global.Down = ord("S")
global.Left = ord("A")
global.Right = ord("D")
global.B1 = vk_shift
global.B2 = ord("Z")
global.B3 = ord("X")
global.B4 = ord("C")

var file = working_directory + "/config.txt"
if file_exists_ns(file) {
	file = file_text_open_read_ns(file, -1)
	var w, exec
	var _c = 0
	while !file_text_eof_ns(file) {
		w = file_text_read_line_ns(file)
		scr_loadConfig(w)
	}
	file_text_close_ns(file)
}