// Create
enum shotCreation {
	sprite,
	subimage,
	color,
	hitboxType,
	radius,
	width,
	height,
	drawRotated,
	drawRounded,
	spawnSprite,
	spawnSubimage,
	length
}

enum shotGroupType {
	circle,
	arc
}

// Grid (DB of shots)
global.shotAddIterator = -1
shotCreateGrid = ds_grid_create(1, shotCreation.length)