return @"
-- Wait Function for coroutines
function wait(__a)
	local __i
	for __i = 1, __a do
		coroutine.yield()
	end
end

-- GM lengthdir_x equivalent
function lengthdir_x(__l, __d)
	return __l * cos (__d * math.pi / 180)
end

-- GM lengthdir_y equivalent
function lengthdir_y(__l, __d)
	return __l * -sin (__d * math.pi / 180)
end

-- Boss HP Bar types
	HP_CIRCLE = 0
	HP_BAR = 1

-- Shot Type Macros

	-- Delete Type
	DELTYPE_INSTANT = 0
	DELTYPE_ANIMATED = 1
	DELTYPE_ITEM = 2

	-- Color
	CIRCLE = 0
	OVAL = 1
	RECTANGLE = 2
	CLASER = 3
	SLASER = 4
	
	-- Blend Modes
	bm_normal = 0
	bm_add = 1
	bm_max = 2
	bm_subtract = 3
"