var t = false
switch argument0 {
	case "EaseLinear":			t = EaseLinear			break
	case "EaseInQuad":			t = EaseInQuad			break
	case "EaseOutQuad":			t = EaseOutQuad			break
	case "EaseInOutQuad":		t = EaseInOutQuad		break
	case "EaseInCubic":			t = EaseInCubic			break
	case "EaseOutCubic":		t = EaseOutCubic		break
	case "EaseInOutCubic":		t = EaseInOutCubic		break
	case "EaseInQuart":			t = EaseInQuart			break
	case "EaseOutQuart":		t = EaseOutQuart		break
	case "EaseInOutQuart":		t = EaseInOutQuart		break
	case "EaseInQuint":			t = EaseInQuint			break
	case "EaseOutQuint":		t = EaseOutQuint		break
	case "EaseInOutQuint":		t = EaseInOutQuint		break
	case "EaseInSine":			t = EaseInSine			break
	case "EaseOutSine":			t = EaseOutSine			break
	case "EaseInOutSine":		t = EaseInOutSine		break
	case "EaseInCirc":			t = EaseInCirc			break
	case "EaseOutCirc":			t = EaseOutCirc			break
	case "EaseInOutCirc":		t = EaseInOutCirc		break
	case "EaseInExpo":			t = EaseInExpo			break
	case "EaseOutExpo":			t = EaseOutExpo			break
	case "EaseInOutExpo":		t = EaseInOutExpo		break
	case "EaseInElastic":		t = EaseInElastic		break
	case "EaseOutElastic":		t = EaseOutElastic		break
	case "EaseInOutElastic":	t = EaseInOutElastic	break
	case "EaseInBack":			t = EaseInBack			break
	case "EaseOutBack":			t = EaseOutBack			break
	case "EaseInOutBack":		t = EaseInOutBack		break
	case "EaseInBounce":		t = EaseInBounce		break
	case "EaseOutBounce":		t = EaseOutBounce		break
	case "EaseInOutBounce":		t = EaseInOutBounce		break
}
return t