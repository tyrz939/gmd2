/// @description scr_loadInit( path )
/// @function scr_loadInit
/// @param path

var file = argument0 + "/init.txt"
if file_exists_ns(file) {
	file = file_text_open_read_ns(file, -1)
	var w, exec
	var _c = 0
	while !file_text_eof_ns(file) {
		w = file_text_read_line_ns(file)
		exec = scr_string_remove_space(w, 0)
		
		switch exec[0] {
			case "scriptFile": global.scriptFile = exec[2]; break;
			case "gameWidth": global.res_x = real(exec[2]); break;
			case "gameHeight": global.res_y = real(exec[2]); break;
			case "windowWidth": global.window_width = real(exec[2]); break;
			case "windowHeight": global.window_height = real(exec[2]); break;
			case "guiWidth": global.gui_width = real(exec[2]); break;
			case "guiHeight": global.gui_height = real(exec[2]); break;
		}
	}
	file_text_close_ns(file)
}


