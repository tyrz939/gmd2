/// @description  image_stream_start(string:name, width:number, height:number, sep:number)
/// @function  image_stream_start
/// @param string:name
/// @param  width:number
/// @param  height:number
/// @param  sep:number
if (argument_count != 4) return lua_show_error("image_stream_start: Expected 4 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_stream_start: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
if !(is_real(argument1) || is_int64(argument1)) return lua_show_error("image_stream_start: Expected a number for argument1 (width:number), got " + lua_print_value(argument1));
if !(is_real(argument2) || is_int64(argument2)) return lua_show_error("image_stream_start: Expected a number for argument2 (height:number), got " + lua_print_value(argument2));
if !(is_real(argument3) || is_int64(argument3)) return lua_show_error("image_stream_start: Expected a number for argument3 (sep:number), got " + lua_print_value(argument3));
image_stream_start(argument0, argument1, argument2, argument3);

