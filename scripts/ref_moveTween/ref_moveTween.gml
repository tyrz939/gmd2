/// @function moveTween
/// @description moveTween
/// @param {real} Instance
/// @param {real} x
/// @param {real} y
/// @param {real} Time
/// @param {real} [opt]TweenType

if (!in_range(argument_count, 4, 5)) return lua_show_error("MoveTween: Expected 4-5 arguments, got " + string(argument_count));
if (!instance_exists(argument[0])) return lua_show_error("MoveTween: Instance doesn't exist");

if argument_count == 5 {
	with argument[0] {
		return TweenEasyMove(argument[0].x, argument[0].y, argument[1], argument[2], 0, argument[3], tweenCheck(argument[4]))
	}
}

with argument[0] {
	return TweenEasyMove(argument[0].x, argument[0].y, argument[1], argument[2], 0, argument[3], global.tweenMove)
}