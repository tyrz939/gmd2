/// @function replayLoad
/// @description Replay Loader
/// @param {real} replayNumber
/// @param {real} Type

var _file = replayNameGen(argument0)
if file_exists_ns(_file) {
	var _replayBuffer = -1, _replayBufferDecompressed = -1
	_replayBuffer = buffer_load_ns(_file)
	_replayBufferDecompressed = buffer_decompress(_replayBuffer)
	
	buffer_seek(_replayBufferDecompressed, buffer_seek_start, 0)
	var _replayString = buffer_read(_replayBufferDecompressed, buffer_string)
	
	var _replayParse1 = replayParser(_replayString)
	replayInterpreter(_replayParse1, argument1, argument0)
	

	buffer_delete(_replayBuffer)
	buffer_delete(_replayBufferDecompressed)
	return true
} else {
	println("No replay found")
	show_message("No replay found")
}
