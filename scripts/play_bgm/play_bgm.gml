/// @function play_sfx(Music)
/// @description Play SFX
/// @param {real} sound_id

if global.music_vol > 0 {
	audio_group_stop_all(BGM);
	audio_play_sound(argument0, 1000, true);
	global.current_music = argument0;
}