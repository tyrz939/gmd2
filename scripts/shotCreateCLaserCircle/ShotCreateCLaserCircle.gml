/// @function shotCreateCLaserCircle
/// @description Creates a circle of lasers
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound

// Sound
if argument_count > 9 {
	if argument[9] != false {
		play_sfx(argument[9], false)
	}
}

var __shotType = argument[0]
var __shotX = argument[1]
var __shotY = argument[2]
var __shotSpeed = argument[3]
var __shotDirection = argument[4]

global.__shotGroupAmount = argument[5]
global.__LaserLength = argument[6]
global.__LaserWidth = argument[7]
global.__shotGroupDistFromCenter = argument[8]

global.__shotCreateType = bulletType_t.laserGroup
global.__shotGroupType = shotGroupType.circle
return ShotCreate(__shotType, __shotX, __shotY, __shotSpeed, __shotDirection, false)