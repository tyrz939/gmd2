/// @description  ShotCreateCircleA
/// @function  ShotCreateCircleA
/// @param  Type:number
/// @param  x:number
/// @param  y:number
/// @param  Speed:number
/// @param  Direction:number
/// @param  Amount:number
/// @param  DistFromCenter:number
/// @param  stepSpeedChange:number
/// @param  finalSpeed:number
/// @param  speedChangeDelay:number
/// @param  Sound:number
/// @param  Delay:number
var __ac = argument_count
if !in_range(__ac, 9, 12) {return lua_show_error("ShotCreateCircleA: Expected 9-12 arguments, got " + string(__ac))}

if __ac == 9 {
	return ShotCreateCircleA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8]);
} else if __ac == 10 {
	return ShotCreateCircleA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9]);
} else if __ac == 11 {
	return ShotCreateCircleA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10]);
} else if __ac == 12 {
	return ShotCreateCircleA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11]);
}