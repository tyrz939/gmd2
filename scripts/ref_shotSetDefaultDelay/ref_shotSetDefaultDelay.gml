/// @function shotSetDefaultDelay
/// @description shotSetDefaultDelay
/// @param {real} DelayTime

if (argument_count != 1)  return lua_show_error("shotSetDefaultDelay: Expected 1 arguments, got " + string(argument_count));
shotSetDefaultDelay(argument[0]);
