var file = "config.txt"
if file_exists_ns(file) {
	file_delete_ns(file)	
}
file = file_text_open_write_ns(file, -1)
var w

// Game Settings
file_text_write_line_ns(file, "music_vol " + string(global.music_vol *100))
file_text_write_line_ns(file, "sfx_vol " + string(global.sfx_vol *100))

if global.fullscreen { w = "true" } else { w = "false" }
file_text_write_line_ns(file, "fullscreen " + w)

if global.vsync { w = "true" } else { w = "false" }
file_text_write_line_ns(file, "vsync " + w)

if global.textureFilterMode == tf_linear { w = "trilinear" } else { w = "anisotropic" }
file_text_write_line_ns(file, "textureFilterMode " + w)
file_text_write_line_ns(file, "afLevel " + string(global.afLevel))

if global.upscale { w = "true" } else { w = "false" }
file_text_write_line_ns(file, "upscale " + w)

file_text_write_line_ns(file, "scalingTypeGame " + string(global.scalingTypeGame))
file_text_write_line_ns(file, "scalingTypeGUI " + string(global.scalingTypeGUI))
file_text_write_line_ns(file, "backgroundScale " + string(global.backgroundScale))

if global.showfps == 2 { w = "hellaTrue" } else if global.showfps == 1 { w = "true" } else { w = "false" }
file_text_write_line_ns(file, "showFPS " + w)

// KeyBinds
file_text_write_line_ns(file, "up " + string(global.Up))
file_text_write_line_ns(file, "down " + string(global.Down))
file_text_write_line_ns(file, "left " + string(global.Left))
file_text_write_line_ns(file, "right " + string(global.Right))
file_text_write_line_ns(file, "b1 " + string(global.B1))
file_text_write_line_ns(file, "b2 " + string(global.B2))
file_text_write_line_ns(file, "b3 " + string(global.B3))
file_text_write_line_ns(file, "b4 " + string(global.B4))

file_text_close_ns(file)