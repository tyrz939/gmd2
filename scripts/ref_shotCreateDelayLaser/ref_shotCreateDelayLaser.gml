/// @function shotCreateDelayLaser
/// @description shotCreateDelayLaser
/// @param {real} Type
/// @param {real} Length
/// @param {real} Width
/// @param {real} Bullet
/// @param {real} Delay

if (argument_count != 5)  return lua_show_error("ShotCreateDelayLaser: Expected 5 arguments, got " + string(argument_count));

return shotCreateDelayLaser(argument[0], argument[1], argument[2], argument[3], argument[4])