/// @function shotGroupSetDirectionPoint
/// @description shotGroupSetDirectionPoint
/// @param {real} instance
/// @param {real} x
/// @param {real} y

if (argument_count != 3)  return lua_show_error("shotGroupSetDirectionPoint: Expected 3 arguments, got " + string(argument_count));

return shotGroupSetDirectionPoint(argument[0], argument[1], argument[2]);