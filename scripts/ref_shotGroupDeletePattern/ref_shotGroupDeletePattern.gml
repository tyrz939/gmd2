/// @function shotGroupDeletePattern
/// @description shotGroupDeletePattern
/// @param {real} instance
/// @param {real} live
/// @param {real} delete

if (argument_count != 3)  return lua_show_error("shotGroupDeletePattern: Expected 3 arguments, got " + string(argument_count));

return shotGroupDeletePattern(argument[0], argument[1], argument[2]);