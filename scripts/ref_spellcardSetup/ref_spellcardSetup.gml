/// @function spellcardSetup
/// @description spellcardSetup
/// @param boss
/// @param timer
/// @param bonus
/// @param survival
/// @param HP

if (argument_count != 5)  return lua_show_error("SpellcardSetup: Expected 5 arguments, got " + string(argument_count));

return spellcardSetup(argument[0], argument[1], argument[2], argument[3], argument[4]);