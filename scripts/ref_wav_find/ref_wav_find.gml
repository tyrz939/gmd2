/// @description  wav_find
/// @function  wav_find
/// @param string:name

if (argument_count != 1) return lua_show_error("wav_find: Expected 1 arguments, got " + string(argument_count));

for(var i = 0; i < ds_grid_height(soundGrid); i++) {
	if soundGrid[# 2, i] == argument0 {
		return soundGrid[# 1, i]
	}
}
return -1