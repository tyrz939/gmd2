/// @function spellcardSetup
/// @description spellcardSetup
/// @param boss
/// @param timer
/// @param bonus
/// @param survival
/// @param HP

var _boss = argument[0], _timer = argument[1], _bonus = argument[2], _survival = argument[3], _hp = argument[4]

with _boss {
	bossState = bossState_t.SPELLCARD
	
	// Timer
	bossPhaseTimer = _timer * 60
	bossPhaseTimerStart = _timer * 60
	
	// Set bonus
	spellBonus = _bonus
	spellBonusStart = _bonus
	
	// Set survival
	if _survival {
		healthBarShow = false
		hp = 1
		maxhp = _hp
		spellSurvival = true
	} else {
		healthBarShow = true
	}
	
	// Set HP
	if hp > 0 {
		var _ratio = hp / maxhp
		hp = _hp
		maxhp = _hp / _ratio
		bossSpellBreakHP = _hp
	} else {
		hp = _hp
		maxhp = _hp
		bossSpellBreakHP = _hp
	}
	
	return bossPhaseID
}

return -1