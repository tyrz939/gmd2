/// @function PlayFieldSetup
/// @description PlayFieldSetup
/// @param {real} StartX
/// @param {real} StartY
/// @param {real} Width
/// @param {real} Height

if (argument_count != 4)  return lua_show_error("PlayFieldSetup: Expected 4 arguments, got " + string(argument_count));
playFieldSetup(argument[0], argument[1], argument[2], argument[3]);
