/// @function nonspellSetup
/// @description nonspellSetup
/// @param boss
/// @param timer
/// @param HP
/// @param breakHP

if (argument_count != 4)  return lua_show_error("NonspellSetup: Expected 4 arguments, got " + string(argument_count));

return nonspellSetup(argument[0], argument[1], argument[2], argument[3]);