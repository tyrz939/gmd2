/// @function shotGroupSetDirectionPoint
/// @description shotGroupSetDirectionPoint
/// @param {real} instance
/// @param {real} x
/// @param {real} y

var __groupID = argument0, __xface = argument1, __yface = argument2

if instance_exists(__groupID) {
	with __groupID {
		if object_index == obj_bulletGroup {
			for(var __i = 0; __i < __amount; __i++) {
				grid[# __dir, __i] = point_direction(grid[# __xx, __i], grid[# __yy, __i], __xface, __yface)
			}
			return true
		} else if object_index == obj_cLaserGroup {
			for(var __i = 0; __i < __amount; __i++) {
				grid[# __dir, __i] = point_direction(grid[# __gridWidth - _stepSize, __i], grid[# __gridWidth - _stepSize+__yy, __i], __xface, __yface)
			}
		} else {
			print_error("shotGroupSetDirectionPoint: Object isn't a bullet group")
			return false
		}
	}
} else {
	print_error("shotGroupSetDirectionPoint: Bullet group doesn't exist")
	return false
}