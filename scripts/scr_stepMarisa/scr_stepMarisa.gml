/*************
Player's Orbs
*************/
orbCount = min(floor(game.shotPower), orbCount)
while orbCount < floor(game.shotPower) {
	orbCount++
	orbArray[orbCount-1] = instance_create_depth(0, 0, PLAYER_ORB_DEPTH, obj_playerOrb)
	var _inst = instance_create_depth(orbArray[orbCount-1].x, orbArray[orbCount-1].y, PLAYER_LASER_DEPTH, obj_marisaLaser)
	_inst.lockPositionTo = orbArray[orbCount-1]
}

/********
Shooting
********/
if shoot_press {
	if shotCounter == shotDelay {
		playerShotCreate(x - 10, y, 90, 20, obj_marisaShot1)
		playerShotCreate(x + 10, y, 90, 20, obj_marisaShot1)
		play_sfx(sfx_playerShot, false)
	}
	shotCounter--;
	if shotCounter <= 0 {shotCounter = shotDelay}
	if focus {
		var _altSpd = 5
		switch orbCount {
			case 1: 
				if shotCounterAlt == shotDelayAlt {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_marisaShot2)
				}
				break
			case 2:
				if shotCounterAlt == shotDelayAlt {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_marisaShot2)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_marisaShot2)
				}
				break
			case 3:
				if shotCounterAlt == shotDelayAlt {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_marisaShot2)
					playerShotCreate(orbArray[2].x, orbArray[2].y, 90, _altSpd, obj_marisaShot2)
				}
				if shotCounterAlt == shotDelayAlt-3 {
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_marisaShot2)
				}
				break
			case 4:
				if shotCounterAlt == shotDelayAlt {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_marisaShot2)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_marisaShot2)
				}
				if shotCounterAlt == shotDelayAlt-3 {
					playerShotCreate(orbArray[2].x, orbArray[2].y, 90, _altSpd, obj_marisaShot2)
					playerShotCreate(orbArray[3].x, orbArray[3].y, 90, _altSpd, obj_marisaShot2)
				}
				break
		}
		shotCounterAlt--;
		if shotCounterAlt <= 0 {shotCounterAlt = shotDelayAlt}
	} else {
		shotCounterAlt = shotDelayAlt
	}
} else {shotCounter = shotDelay; shotCounterAlt = shotDelayAlt}

/*******
Bombing
*******/
if bomb_press && game.bombs > 0 && !inBomb{
	instance_create_depth(x, y, BOMB_DEPTH, obj_marisaBomb1)
	game.bombs--
}