/*************
Player's Orbs
*************/
orbCount = min(floor(game.shotPower), orbCount)
while orbCount < floor(game.shotPower) {
	orbCount++
	orbArray[orbCount-1] = instance_create_depth(0, 0, PLAYER_ORB_DEPTH, obj_playerOrb)
}
orbImageAngle -= 5
with obj_playerOrb {
	image_angle = other.orbImageAngle
}

/********
Shooting
********/
if shoot_press {
	if shotCounter == shotDelay {
		/*---- Reimu Main Shot ----*/
		playerShotCreate(x - 10, y, 90, 20, obj_reimuShot1)
		playerShotCreate(x + 10, y, 90, 20, obj_reimuShot1)
		play_sfx(sfx_playerShot, false)
	}
	shotCounter--;
	if shotCounter <= 0 {shotCounter = shotDelay}
	if !focus {
		/*---- Reimu Unfocused Shot ----*/
		shotCounterAlt2 = shotDelayAlt2
		var _altSpd = 5
		switch orbCount {
			case 1: 
				if shotCounterAlt1 == shotDelayAlt1 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_reimuShot3)
				}
				break
			case 2:
				if shotCounterAlt1 == shotDelayAlt1 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 135, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 45, _altSpd, obj_reimuShot3)
				}
				break
			case 3:
				if shotCounterAlt1 == shotDelayAlt1 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 135, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[2].x, orbArray[2].y, 90, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 45, _altSpd, obj_reimuShot3)
				}
				break
			case 4:
				if shotCounterAlt1 == shotDelayAlt1 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 135, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[3].x, orbArray[3].y, 60, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 45, _altSpd, obj_reimuShot3)
					playerShotCreate(orbArray[2].x, orbArray[2].y, 120, _altSpd, obj_reimuShot3)
				}
				break
		}
		
		shotCounterAlt1--;
		if shotCounterAlt1 <= 0 {shotCounterAlt1 = shotDelayAlt1}
	} else {
		/*---- Reimu Focused Shot ----*/
		shotCounterAlt1 = shotDelayAlt1
				var _altSpd = 40
		switch orbCount {
			case 1: 
				if shotCounterAlt2 == shotDelayAlt2 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_reimuShot2)
				}
				break
			case 2:
				if shotCounterAlt2 == shotDelayAlt2 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_reimuShot2)
				}
				break
			case 3:
				if shotCounterAlt2 == shotDelayAlt2 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[2].x, orbArray[2].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_reimuShot2)
				}
				break
			case 4:
				if shotCounterAlt2 == shotDelayAlt2 {
					playerShotCreate(orbArray[0].x, orbArray[0].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[3].x, orbArray[3].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[1].x, orbArray[1].y, 90, _altSpd, obj_reimuShot2)
					playerShotCreate(orbArray[2].x, orbArray[2].y, 90, _altSpd, obj_reimuShot2)
				}
				break
		}
		shotCounterAlt2--;
		if shotCounterAlt2 <= 0 {shotCounterAlt2 = shotDelayAlt2}
	}
} else {shotCounter = shotDelay; shotCounterAlt = shotDelayAlt1; shotCounterAlt2 = shotDelayAlt2}

/*******
Bombing
*******/
if bomb_press && game.bombs > 0 && !inBomb{
	instance_create_depth(x, y, BOMB_DEPTH, obj_reimuBomb1)
	game.bombs--
}