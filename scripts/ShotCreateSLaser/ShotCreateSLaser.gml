/// @function shotCreateSLaser
/// @description Static Laser
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Direction
/// @param {real} Length
/// @param {real} Width
/// @param {real} Duration
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 7 {
	if argument[7] != false {
		play_sfx(argument[7], false)
	}
}
// Delay Timer
if argument_count > 8 {
	global.__shotDelay = argument[8]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}

var __shotType = argument[0]
var __shotX = argument[1]
var __shotY = argument[2]
var __shotDirection = argument[3]

global.__LaserLength = argument[4]
global.__LaserWidth = argument[5]
global.__LaserDuration = argument[6]

global.__shotCreateType = bulletType_t.staticLaserGroup

ShotCreate(__shotType, __shotX, __shotY, 0, __shotDirection, false, global.__shotDelay)