/// @function turnTween
/// @description turnTween
/// @param {real} Instance
/// @param {real} dir1
/// @param {real} dir2
/// @param {real} delay
/// @param {real} Time
/// @param {real} [opt]TweenType

if (!in_range(argument_count, 5, 6))  return lua_show_error("TurnTween: Expected 5-6 arguments, got " + string(argument_count));
if (!instance_exists(argument[0])) return lua_show_error("TurnTween: Instance doesn't exist");

if argument_count == 6 {
	with argument[0] {
		return TweenEasyTurn(argument[1], argument[2], argument[3], argument[4], tweenCheck(argument[5]))
	}
}

with argument[0] {
	return TweenEasyTurn(argument[1], argument[2], argument[3], argument[4], global.tweenMove)
}