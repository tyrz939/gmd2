/// @function EnemyShotCollisionCircle
/// @description EnemyShotCollisionCircle
/// @param {real} Instance
/// @param {real} Radius

if (argument_count != 2)  return lua_show_error("EnemyShotCollisionCircle: Expected 2 arguments, got " + string(argument_count));

return enemyShotCollisionCircle(argument[0], argument[1])