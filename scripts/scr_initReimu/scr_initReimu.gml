player.playerOrbs = game.reimuOrb
player.playerOrbSubimage = 0
playerHitbox = spr_hitbox
spriteAnimationType = 0
//global.shotPowerMax = 4
//moveSpeed = 5
//bombSpeedPenalty = 0.2
//focusSpeed = 2
//shotCD = 3

// Player

characterName = "Reimu Hakurei"
playerDeathColor = c_red
shotPowerMax = 4
orbArray = array_create(shotPowerMax, noone)
moveSpeed = 4.5
bombSpeedPenalty = 1
focusSpeed = 2
shotDelay = 3
shotCounter = 0
shotDelayAlt1 = 5
shotDelayAlt2 = 3
shotCounterAlt1 = 0
shotCounterAlt2 = 0
orbImageAngle = 0

game.hitboxRadius = 2
game.grazeRadius = 15
		
// Step event script
stepScript = scr_stepReimu
		
// Orbs
// 2d Array of orb positions
var orb1 = 0, orb2 = 1, orb3 = 2, orb4 = 3
var pow1 = 0, pow2 = 1, pow3 = 2, pow4 = 3
	// Unfocused
	orbX[pow1, orb1] = 0;
	orbX[pow2, orb1] = -32;		orbX[pow2, orb2] = 32;
	orbX[pow3, orb1] = -32;		orbX[pow3, orb2] = 32;		orbX[pow3, orb3] = 0;
	orbX[pow4, orb1] = -32;		orbX[pow4, orb2] = 32;		orbX[pow4, orb3] = -16;	orbX[pow4, orb4] = 16;

	orbY[pow1, orb1] = -32;
	orbY[pow2, orb1] = 0;		orbY[pow2, orb2] = 0;
	orbY[pow3, orb1] = 16;		orbY[pow3, orb2] = 16;		orbY[pow3, orb3] = 32;
	orbY[pow4, orb1] = 16;		orbY[pow4, orb2] = 16;		orbY[pow4, orb3] = 32;	orbY[pow4, orb4] = 32;
	// Focused
	forbX[pow1, orb1] = 0;
	forbX[pow2, orb1] = -8;		forbX[pow2, orb2] = 8;
	forbX[pow3, orb1] = -16;	forbX[pow3, orb2] = 16;		forbX[pow3, orb3] = 0;
	forbX[pow4, orb1] = -24;	forbX[pow4, orb2] = 24;		forbX[pow4, orb3] = -8;	forbX[pow4, orb4] = 8;

	forbY[pow1, orb1] = -24;
	forbY[pow2, orb1] = -24;	forbY[pow2, orb2] = -24;
	forbY[pow3, orb1] = -24;	forbY[pow3, orb2] = -24;	forbY[pow3, orb3] = -32;
	forbY[pow4, orb1] = -24;	forbY[pow4, orb2] = -24;	forbY[pow4, orb3] = -32;forbY[pow4, orb4] = -32;
	// Laser Direction
	dirL[pow1, orb1] = 90;
	dirL[pow2, orb1] = 90;	dirL[pow2, orb2] = 90;
	dirL[pow3, orb1] = 105;	dirL[pow3, orb2] = 75;	dirL[pow3, orb3] = 90;
	dirL[pow4, orb1] = 105;	dirL[pow4, orb2] = 75;	dirL[pow4, orb3] = 90;	dirL[pow4, orb4] = 90;

/**********************
	Particle Types
**********************/

	// Reimu Main Shot
	global.partReimuShot1Die = part_type_create();
	part_type_shape(global.partReimuShot1Die, pt_shape_disk);
	part_type_size(global.partReimuShot1Die,1,1,0,0);
	part_type_scale(global.partReimuShot1Die,1,1);
	part_type_alpha1(global.partReimuShot1Die,1);
	part_type_speed(global.partReimuShot1Die,5,5,0,0);
	part_type_direction(global.partReimuShot1Die,90,90,0,0);
	part_type_orientation(global.partReimuShot1Die,0,0,0,0,0);
	part_type_blend(global.partReimuShot1Die,true);
	part_type_life(global.partReimuShot1Die,12,12);

	// Reimu alt focus
	global.partReimuShot2Die = part_type_create();
	part_type_shape(global.partReimuShot2Die, pt_shape_disk);
	part_type_size(global.partReimuShot2Die,1,1,0,0);
	part_type_scale(global.partReimuShot2Die,1,1);
	part_type_alpha2(global.partReimuShot2Die,1, 0);
	part_type_speed(global.partReimuShot2Die,5,5,0,0);
	part_type_direction(global.partReimuShot2Die,90,90,0,0);
	part_type_orientation(global.partReimuShot2Die,-5,5,0,0,0);
	part_type_blend(global.partReimuShot2Die,true);
	part_type_life(global.partReimuShot2Die,16,16);

	// Reimu Homing
	global.partReimuShot3Die = part_type_create();
	part_type_shape(global.partReimuShot3Die, pt_shape_disk);
	part_type_size(global.partReimuShot3Die,1,1,0,0);
	part_type_scale(global.partReimuShot3Die,1,1);
	part_type_alpha1(global.partReimuShot3Die,1);
	part_type_speed(global.partReimuShot3Die,5,5,0,0);
	part_type_direction(global.partReimuShot3Die,90,90,0,0);
	part_type_orientation(global.partReimuShot3Die,0,0,0,0,1);
	part_type_blend(global.partReimuShot3Die,true);
	part_type_life(global.partReimuShot3Die,12,12);

	// Reimu bomb big
	global.partReimuBomb1 = part_type_create();
	part_type_shape(global.partReimuBomb1, pt_shape_sphere);
	part_type_size(global.partReimuBomb1,1,1,0,0);
	part_type_scale(global.partReimuBomb1,4,4);
	part_type_alpha2(global.partReimuBomb1,1, 0);
	part_type_speed(global.partReimuBomb1,0,0,0,0);
	part_type_direction(global.partReimuBomb1,0,0,0,0);
	part_type_orientation(global.partReimuBomb1,0,0,0,0,1);
	part_type_blend(global.partReimuBomb1,true);
	part_type_life(global.partReimuBomb1,12,12);
	
	// Reimu bomb small
	global.partReimuBomb11 = part_type_create();
	part_type_shape(global.partReimuBomb11, pt_shape_disk);
	part_type_size(global.partReimuBomb11,0.5,0.5,0,0);
	part_type_scale(global.partReimuBomb11,1,1);
	part_type_alpha3(global.partReimuBomb11,1, 1, 0);
	part_type_speed(global.partReimuBomb11,4,5,0,0);
	part_type_direction(global.partReimuBomb11,0,360,0,0);
	part_type_orientation(global.partReimuBomb11,0,0,0,0,1);
	part_type_blend(global.partReimuBomb11,true);
	part_type_life(global.partReimuBomb11,30,30);