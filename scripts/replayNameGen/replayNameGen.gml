/// @function replayNameGen
/// @description Replay Name Gen
/// @param {real} replayNumber
return global.replayPath + "replay" + string(argument0+1) + ".rpx"
