var exec = scr_string_remove_space(argument0, 0)

switch exec[0] {
	case "music_vol": global.music_vol = real(exec[1]) /100; global.music_vol = clamp(global.music_vol, 0, 1); break;
	case "sfx_vol": global.sfx_vol = real(exec[1]) /100; global.sfx_vol = clamp(global.sfx_vol, 0, 1); break;
	case "fullscreen": if exec[1] == "true" {global.fullscreen = true} else {global.fullscreen = false} break;
	case "vsync":  if exec[1] == "true" {global.vsync = true} else {global.vsync = false} break;
	case "textureFilterMode":  if exec[1] == "trilinear" {global.textureFilterMode = tf_linear} else {global.textureFilterMode = tf_anisotropic} break;
	case "afLevel":  global.afLevel = real(exec[1]) break;
	case "upscale":  if exec[1] == "true" {global.upscale = true} else {global.upscale = false} break;
	case "scalingTypeGame":  global.scalingTypeGame = real(exec[1]) break;
	case "scalingTypeGUI":  global.scalingTypeGUI = real(exec[1]) break;
	case "backgroundScale":  global.backgroundScale = real(exec[1]) break;
	case "showFPS": if exec[1] == "hellaTrue" {global.showfps = 2} else if exec[1] == "true" {global.showfps = 1} else {global.showfps = 0} break;
	case "up": global.Up = real(exec[1]) break;
	case "down": global.Down = real(exec[1]) break;
	case "left": global.Left = real(exec[1]) break;
	case "right": global.Right = real(exec[1]) break;
	case "b1": global.B1 = real(exec[1]) break;
	case "b2": global.B2 = real(exec[1]) break;
	case "b3": global.B3 = real(exec[1]) break;
	case "b4": global.B4 = real(exec[1]) break;
}