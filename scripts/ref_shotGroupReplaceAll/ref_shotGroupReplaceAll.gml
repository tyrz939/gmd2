/// @function shotGroupReplaceAll
/// @description shotGroupReplaceAll
/// @param {real} instance
/// @param {real} Type


if (argument_count != 2)  return lua_show_error("shotGroupReplaceAll: Expected 2 arguments, got " + string(argument_count));

return shotGroupReplaceAll(argument[0], argument[1]);