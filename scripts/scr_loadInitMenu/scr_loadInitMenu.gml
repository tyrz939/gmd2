/// @description scr_loadInitMenu( path, array_position )
/// @function scr_loadInitMenu
/// @param path
/// @param array_position

var file = argument0 + "/init.txt"
var i = argument1
if file_exists_ns(file) {
	file = file_text_open_read_ns(file, -1)
	var w, exec
	var _c = 0
	while !file_text_eof_ns(file) {
		w = file_text_read_line_ns(file)
		exec = scr_string_remove_space(w, 0)
		if exec[0] == "scriptName" {
			menu[i] = ""
			for(var a = 2; a < array_length_1d(exec); a++) {
				if exec[a] != "" {menu[i] += exec[a] + " "}
			}
		} else if exec[0] == "scriptDescription" {
			desc[i] = ""
			for(var a = 2; a < array_length_1d(exec); a++) {
				if exec[a] != "" {desc[i] += exec[a] + " "}
			}
		} else if exec[0] == "scriptAuther" {
			auth[i] = ""
			for(var a = 2; a < array_length_1d(exec); a++) {
				if exec[a] != "" {auth[i] += exec[a] + " "}
			}
		} else if exec[0] == "scriptFile" {
			for(var a = 2; a < array_length_1d(exec); a++) {
				sfile[i] += exec[a] + " "
			}
		}
	}
	file_text_close_ns(file)
}



