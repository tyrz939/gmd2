gml_pragma("forceinline")
/***********************
Bullet Remover Cleanup
***********************/
var _currentHeight = 1
var _gridWidth = ds_grid_width(bulletRemover)
var _point = 0
var _r, _o

// Count Down for timed removal
for( _o = 1; _o < _gridWidth; _o++ ) {
	if ( bulletRemover[# _o, bulletRemover_t.TIMER] > 0 )		{
		bulletRemover[# _o, bulletRemover_t.TIMER]--
	} else if ( bulletRemover[# _o, bulletRemover_t.TIMER] == 0 ) {
		bulletRemover[# _o, bulletRemover_t.ALIVE] = 0
	}
}

// Clean up "dead" removers and condense the array
var _resizeGrid = false
for( _o = 1; _o < _gridWidth; _o++ ) {
	// Clean up
	if bulletRemover[# _o, bulletRemover_t.ALIVE] == 0 {
		_resizeGrid = true
		_point = 0
		// Find next alive Bullet Remover (if any)
		while (( _o + _point < _gridWidth ) && ( bulletRemover[# _o + _point, bulletRemover_t.ALIVE] == 0 )) {
			_point++
		}
		
		if (( _point != 0 ) && ( _o + _point < _gridWidth )) {
			for(_r = 0; _r < bulletRemover_t.LENGTH; _r++) {
				bulletRemover[# _o, _r] = bulletRemover[# _o + _point, _r]
				bulletRemover[# _o + _point, _r] = 0
			}
		}
	}
	
	// Saves the new usable "grid height"
	if ( bulletRemover[# _o, bulletRemover_t.ALIVE] == 1 ) {_currentHeight = _o +1}
}
if ( _resizeGrid ) {ds_grid_resize(bulletRemover, _currentHeight, bulletRemover_t.LENGTH)}