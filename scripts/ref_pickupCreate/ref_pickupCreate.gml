/// @function pickupCreate
/// @description pickupCreate
/// @param {real} x
/// @param {real} y
/// @param {real} Type

if (argument_count != 3)  return lua_show_error("PickupCreate: Expected 3 arguments, got " + string(argument_count));
pickupCreate(argument[0], argument[1], argument[2]);
