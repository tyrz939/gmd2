/// @function replaySave
/// @description Replay Saver
/// @param {string} string
/// @param {real} length

var s = ""
var arg = string(argument0)
repeat argument1 - string_length(arg) {
	s += " "
}
return arg + s