/// @description  ShotCreateArcA
/// @function  ShotCreateArcA
/// @param  Type:number
/// @param  x:number
/// @param  y:number
/// @param  Speed:number
/// @param  Direction:number
/// @param  Amount:number
/// @param  Spread:number
/// @param  DistFromCenter:number
/// @param  stepSpeedChange:number
/// @param  finalSpeed:number
/// @param  speedChangeDelay:number
/// @param  Sound:number
/// @param  Delay:number
var __ac = argument_count
if !in_range(__ac, 10, 13) return lua_show_error("ShotCreateArcA: Expected 10-13 arguments, got " + string(__ac));

if __ac == 10 {
	return ShotCreateArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9]);
} else if __ac == 11 {
	return ShotCreateArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10]);
} else if __ac == 12 {
	return ShotCreateArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11]);
} else if __ac == 13 {
	return ShotCreateArcA(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], argument[8], argument[9], argument[10], argument[11], argument[12]);
}