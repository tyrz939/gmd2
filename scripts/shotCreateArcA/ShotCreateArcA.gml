/// @function shotCreateArcA
/// @description Creates an arc of shots with Acceleration/Deceleration
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Spread
/// @param {real} DistanceFromCenter
/// @param {real} StepSpeedChange
/// @param {real} FinalSpeed
/// @param {real} [opt]SpeedChangeDelay
/// @param {real} [opt]Sound
/// @param {real} [opt]Delay

// Sound
if argument_count > 11 {
	if argument[11] != false {
		play_sfx(argument[11], false)
	}
}
// Delay Timer
if argument_count > 12 {
	global.__shotDelay = argument[12]
} else {
	global.__shotDelay = global.__shotDefaultDelay
}
var __inst = ShotCreateArc(argument[0], argument[1], argument[2], argument[3], argument[4], argument[5], argument[6], argument[7], false, global.__shotDelay)
with __inst {
	for(var i = 0; i < __amount; i++) {
		grid[# __accelStepSpeed, i] = argument[8]
		grid[# __accelFinalSpeed, i] = argument[9]
		
		// Work out how long it will take
		grid[# __accelTime, i] = abs((argument[3] - grid[# __accelFinalSpeed, i]) / grid[# __accelStepSpeed, i])
		
		// Accelerating, 1. Decelerating 2
		if grid[# __accelFinalSpeed, i] > grid[# __spd, i] {
			grid[# __accelState, i] = 1
		} else {
			grid[# __accelState, i] = 2
		}
		// Delay timer before starting
		if argument_count > 10 {
			grid[# __accelDelay, i] = argument[10]
		} else {
			grid[# __accelDelay, i] = 0
		}
	}
}
return __inst