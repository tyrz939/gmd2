/// @function shotAddCLaser
/// @description shotAddshotAddCLaserOval
/// @param {real} sprite
/// @param {real} subimage
/// @param {real} color

return shotAdd(argument0, argument1, argument2, LASER, 0, 0, 0, -1, 0, 0, 0)