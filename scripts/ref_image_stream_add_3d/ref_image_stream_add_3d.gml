/// @description  image_stream_add_3d(string:name, string:id, string:file, subimage:number, xorig:number, yorig:number)
/// @function  image_stream_add_3d
/// @param string:name
/// @param  string:id
/// @param  string:file
/// @param  subimage:number
/// @param  xorig:number
/// @param  yorig:number
if (argument_count != 6) return lua_show_error("image_stream_add: Expected 6 arguments, got " + string(argument_count));
if !(is_string(argument0)) return lua_show_error("image_stream_add: Expected a string for argument0 (string:name), got " + lua_print_value(argument0));
if !(is_string(argument1)) return lua_show_error("image_stream_add: Expected a string for argument1 (string:id), got " + lua_print_value(argument1));
if !(is_string(argument2)) return lua_show_error("image_stream_add: Expected a string for argument2 (string:file), got " + lua_print_value(argument2));
if !(is_real(argument3) || is_int64(argument3)) return lua_show_error("image_stream_add: Expected a number for argument3 (subimage:number), got " + lua_print_value(argument3));
if !(is_real(argument4) || is_int64(argument4)) return lua_show_error("image_stream_add: Expected a number for argument4 (xorig:number), got " + lua_print_value(argument4));
if !(is_real(argument5) || is_int64(argument5)) return lua_show_error("image_stream_add: Expected a number for argument5 (yorig:number), got " + lua_print_value(argument5));

global.__importScript = 3
importAddQueue(argument0, argument1, argument2, argument3, argument4, argument5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);