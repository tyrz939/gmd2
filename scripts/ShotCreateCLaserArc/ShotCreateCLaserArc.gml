/// @function ShotCreateCLaserArc
/// @description Creates a arc of lasers
/// @param {real} Type
/// @param {real} x
/// @param {real} y
/// @param {real} Speed
/// @param {real} Direction
/// @param {real} Amount
/// @param {real} Spread
/// @param {real} Length
/// @param {real} Width
/// @param {real} DistanceFromCenter
/// @param {real} [opt]Sound

// Sound
if argument_count > 10 {
	if argument[10] != false {
		play_sfx(argument[10], false)
	}
}

var __shotType = argument[0]
var __shotX = argument[1]
var __shotY = argument[2]
var __shotSpeed = argument[3]
var __shotDirection = argument[4]

global.__shotGroupAmount = argument[5]
global.__shotGroupSpread = argument[6]
global.__LaserLength = argument[7]
global.__LaserWidth = argument[8]
global.__shotGroupDistFromCenter = argument[9]

global.__shotCreateType = bulletType_t.laserGroup
global.__shotGroupType = shotGroupType.arc
__shotDirection -= (global.__shotGroupSpread / 2) - (global.__shotGroupSpread / global.__shotGroupAmount) / 2
return ShotCreate(__shotType, __shotX, __shotY, __shotSpeed, __shotDirection, false)