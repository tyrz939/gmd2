/// @description MOD_SetChannelVolume(channel, volume)
/// @function MOD_SetChannelVolume
/// @param channel
/// @param volume

return ModPlug_SetChannelVolume(argument0, argument1);