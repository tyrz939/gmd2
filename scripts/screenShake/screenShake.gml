/// @function shakeScreen
/// @description shakeScreen
/// @param {real} Amplitude
/// @param {real} Duration
gml_pragma("forceinline")

with drawGame {
	shakeAmplitude += argument0
	shakeDuration = shakeAmplitude / argument1
}