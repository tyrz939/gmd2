/// @function EnemyPlayerCollisionCircle
/// @description EnemyPlayerCollisionCircle
/// @param {real} Instance
/// @param {real} Radius

if (argument_count != 2)  return lua_show_error("EnemyPlayerCollisionCircle: Expected 2 arguments, got " + string(argument_count));

return enemyPlayerCollisionCircle(argument[0], argument[1])