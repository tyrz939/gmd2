{
    "id": "7897d870-0b46-43fe-ad23-11d7a27e534b",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "LibModPlug",
    "IncludedResources": [
        "Scripts\\MOD_Player\\MOD_LoadSong",
        "Scripts\\MOD_Player\\MOD_Play",
        "Scripts\\MOD_Player\\MOD_Pause",
        "Scripts\\MOD_Player\\MOD_Stop",
        "Scripts\\MOD_Player\\MOD_GetName",
        "Scripts\\MOD_Player\\MOD_GetLength",
        "Scripts\\MOD_Player\\MOD_GetTempo",
        "Scripts\\MOD_Player\\MOD_SetTempo",
        "Scripts\\MOD_Player\\MOD_NumChannels",
        "Scripts\\MOD_Player\\MOD_MuteChannel",
        "Objects\\MOD_Player\\obj_mod_player",
        "Objects\\MOD_Player\\obj_mod_player_demo"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "JLibModPlug",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 202,
    "date": "2017-36-19 10:04:25",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "328950c8-5927-4702-94d0-0c0632c1381c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 2097160,
            "filename": "libmodplug.ext",
            "final": "",
            "functions": [
                {
                    "id": "d6ec824d-d6f3-4880-9983-76e617d8908c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        2,
                        2,
                        1
                    ],
                    "externalName": "JModPlug_Load",
                    "help": "ModPlug_Load(repeat_count, size, buffer_address)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Load",
                    "returnType": 2
                },
                {
                    "id": "9ab77d6a-c90d-4b55-8192-ed401872e52a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "JModPlug_Read",
                    "help": "ModPlug_Read(buffer_address)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Read",
                    "returnType": 2
                },
                {
                    "id": "f210b2ea-619b-4be6-9880-5da7d12b6041",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_GetName",
                    "help": "ModPlug_GetName()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetName",
                    "returnType": 1
                },
                {
                    "id": "9c93b5ba-a865-452e-ad57-86b50fdd1304",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_Unload",
                    "help": "ModPlug_Unload()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Unload",
                    "returnType": 2
                },
                {
                    "id": "19f46279-8b48-4ebf-bcb8-69dd70869bc0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_GetLength",
                    "help": "ModPlug_GetLength()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetLength",
                    "returnType": 2
                },
                {
                    "id": "95f5e82c-108e-47b8-aa54-690466565493",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_Stop",
                    "help": "ModPlug_Stop()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Stop",
                    "returnType": 2
                },
                {
                    "id": "a372f26d-0487-4f49-bba9-3625ee79d61c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_GetTempo",
                    "help": "ModPlug_GetTempo()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetTempo",
                    "returnType": 2
                },
                {
                    "id": "a153d67f-63ae-4cdb-a00e-28fc80f2c04a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "JModPlug_NumChannels",
                    "help": "ModPlug_NumChannels()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_NumChannels",
                    "returnType": 2
                },
                {
                    "id": "3f728ec3-fc93-48b6-8f54-af2f04cb21cf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "JModPlug_SetTempo",
                    "help": "ModPlug_SetTempo(tempo)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_SetTempo",
                    "returnType": 2
                },
                {
                    "id": "4ccaa6d2-c02d-487c-8a8c-3291b55d189e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "JModPlug_MuteChannel",
                    "help": "ModPlug_MuteChannel()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_MuteChannel",
                    "returnType": 2
                },
                {
                    "id": "77e996bb-b354-4642-9d09-3aa5b1313aa2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "JModPlug_GetChannelVolume",
                    "help": "ModPlug_GetChannelVolume(channel)",
                    "hidden": false,
                    "kind": 4,
                    "name": "ModPlug_GetChannelVolume",
                    "returnType": 2
                },
                {
                    "id": "e7554794-2bf7-4034-87e4-14a503a9622d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "JModPlug_SetChannelVolume",
                    "help": "ModPlug_SetChannelVolume(channel, volume)",
                    "hidden": false,
                    "kind": 4,
                    "name": "ModPlug_SetChannelVolume",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 4,
            "order": [
                "d6ec824d-d6f3-4880-9983-76e617d8908c",
                "9ab77d6a-c90d-4b55-8192-ed401872e52a",
                "f210b2ea-619b-4be6-9880-5da7d12b6041",
                "9c93b5ba-a865-452e-ad57-86b50fdd1304",
                "19f46279-8b48-4ebf-bcb8-69dd70869bc0",
                "95f5e82c-108e-47b8-aa54-690466565493",
                "a372f26d-0487-4f49-bba9-3625ee79d61c",
                "a153d67f-63ae-4cdb-a00e-28fc80f2c04a",
                "3f728ec3-fc93-48b6-8f54-af2f04cb21cf",
                "4ccaa6d2-c02d-487c-8a8c-3291b55d189e",
                "77e996bb-b354-4642-9d09-3aa5b1313aa2",
                "e7554794-2bf7-4034-87e4-14a503a9622d"
            ],
            "origname": "extensions\\LibModPlug.ext",
            "uncompress": false
        },
        {
            "id": "b334361a-7d76-4246-b9e8-c0ae2e9a3a6c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                {
                    "id": "4595c71e-13fb-45f5-b6aa-851e5cb1ca8d",
                    "modelName": "GMProxyFile",
                    "mvc": "1.0",
                    "TargetMask": 6,
                    "proxyName": "libmodplug.dll"
                },
                {
                    "id": "21e39a86-d067-438e-9e3d-cccfae84376b",
                    "modelName": "GMProxyFile",
                    "mvc": "1.0",
                    "TargetMask": 1,
                    "proxyName": "libmodplug.dylib"
                },
                {
                    "id": "343fef8e-6733-4853-af62-0ea206563ceb",
                    "modelName": "GMProxyFile",
                    "mvc": "1.0",
                    "TargetMask": 7,
                    "proxyName": "libmodplug.so"
                }
            ],
            "constants": [
                
            ],
            "copyToTargets": 1048770,
            "filename": "libmodplug.dll",
            "final": "",
            "functions": [
                {
                    "id": "d82dd327-9b75-45b4-bcda-085b958cdb1a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_GetName",
                    "help": "ModPlug_GetName()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetName",
                    "returnType": 1
                },
                {
                    "id": "e473eaa2-8f42-4216-8715-fa1ac0b4b94d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_Unload",
                    "help": "ModPlug_Unload()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Unload",
                    "returnType": 2
                },
                {
                    "id": "f381c9a0-b54d-4681-bccc-1d78e83293dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_GetLength",
                    "help": "ModPlug_GetLength()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetLength",
                    "returnType": 2
                },
                {
                    "id": "1d25140d-2daa-4049-8fc0-3081851ce389",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "ModPlug_SetTempo",
                    "help": "ModPlug_SetTempo(tempo)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_SetTempo",
                    "returnType": 2
                },
                {
                    "id": "66f4c518-8d07-461c-95fd-70a0bfd403be",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_GetTempo",
                    "help": "ModPlug_GetTempo()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_GetTempo",
                    "returnType": 2
                },
                {
                    "id": "12a9d768-983a-4296-8542-441997b250f7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_NumChannels",
                    "help": "ModPlug_NumChannels()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_NumChannels",
                    "returnType": 2
                },
                {
                    "id": "4ebf77f1-958f-4c03-b1a3-cddbb8d20dc3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        2,
                        2,
                        1
                    ],
                    "externalName": "ModPlug_LoadStr",
                    "help": "ModPlug_Load(repeat_count, size, buffer_address)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Load",
                    "returnType": 2
                },
                {
                    "id": "c86d3824-6c57-457b-a58b-5b3238c39f82",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "ModPlug_ReadStr",
                    "help": "ModPlug_Read(buffer_address)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Read",
                    "returnType": 2
                },
                {
                    "id": "1b6ab429-5f43-4505-a0bc-8b1c38bed55d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ModPlug_Stop",
                    "help": "ModPlug_Stop()",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_Stop",
                    "returnType": 2
                },
                {
                    "id": "1f3ba74d-41a8-4a1f-99aa-46329a4dc0f4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "ModPlug_MuteChannel",
                    "help": "ModPlug_MuteChannel(channel, mute)",
                    "hidden": false,
                    "kind": 12,
                    "name": "ModPlug_MuteChannel",
                    "returnType": 2
                },
                {
                    "id": "e93dc870-c579-4d51-aa22-97f8331570ba",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "ModPlug_GetChannelVolume",
                    "help": "ModPlug_GetChannelVolume(channel)",
                    "hidden": false,
                    "kind": 1,
                    "name": "ModPlug_GetChannelVolume",
                    "returnType": 2
                },
                {
                    "id": "b9162a3b-2203-484d-b16a-182b9569fd0c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "ModPlug_SetChannelVolume",
                    "help": "ModPlug_SetChannelVolume(channel, volume)",
                    "hidden": false,
                    "kind": 1,
                    "name": "ModPlug_SetChannelVolume",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                "d82dd327-9b75-45b4-bcda-085b958cdb1a",
                "e473eaa2-8f42-4216-8715-fa1ac0b4b94d",
                "f381c9a0-b54d-4681-bccc-1d78e83293dd",
                "1d25140d-2daa-4049-8fc0-3081851ce389",
                "66f4c518-8d07-461c-95fd-70a0bfd403be",
                "12a9d768-983a-4296-8542-441997b250f7",
                "4ebf77f1-958f-4c03-b1a3-cddbb8d20dc3",
                "c86d3824-6c57-457b-a58b-5b3238c39f82",
                "1b6ab429-5f43-4505-a0bc-8b1c38bed55d",
                "1f3ba74d-41a8-4a1f-99aa-46329a4dc0f4",
                "e93dc870-c579-4d51-aa22-97f8331570ba",
                "b9162a3b-2203-484d-b16a-182b9569fd0c"
            ],
            "origname": "extensions\\libmodplug.dll",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.gamephase.modplayer",
    "productID": "F79C97955FD1AF4686EB4EF4EF90F0DB",
    "sourcedir": "",
    "version": "1.1.0"
}