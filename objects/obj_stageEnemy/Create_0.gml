spd = 0

grazeAdd = 0 // Because I'm using shot collision for player hit detection
shotColor = -1
shotType = -1

hitCircle = 0
hitWidth = 0
hitHeight = 0

pHitCircle = 0
pHitWidth = 0
pHitHeight = 0

// Bounding box for Marisa Lasers
mask_index = spr_bboxCircle

sprite = noone
hp = 1000
takeDmg = 0


timer = 0

// Color Overlay
	scr_shader_overlay_init()
	overlayDraw = false
	overlayColor = c_blue
	overlayAlpha = 0