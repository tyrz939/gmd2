if !game.pause {
	instance_destroy()
	exit
}

scr_menu_step()

var push = scr_menu_select()

if push == 1 {
    play_sfx(sfx_menuSelect, false)
	switch mpos {
		case 0:
			game.unpauseNextFrame = true;
			instance_destroy();
			break;
		case 1:
			instance_create_depth(0, 0, depth, obj_replayMenu);
			instance_deactivate_object(self)
			//instance_destroy();
			break;
		case 2:
			room_goto(rm_menu);
			instance_destroy();
			break;
	}
}