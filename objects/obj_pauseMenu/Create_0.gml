menu[0] = "Resume Game"
menu[1] = "Save Replay and Quit"
menu[2] = "Quit"

gray = array_create(array_length_1d(menu), false);

push = 0;
space = 50;
mpos = 0;

// For flashy selection
alpha = 1;
alpha_dir = 0;

gameToGuiScaleWidth = global.gui_width / global.res_x
gameToGuiScaleWidth = global.gui_height / global.res_y

x = (global.playAreaStartX + global.playAreaWidth / 2) * gameToGuiScaleWidth
y = (global.playAreaStartY + global.playAreaHeight / 2) * gameToGuiScaleWidth

if game.frame == game.replayEndFrame {
	gray[0] = true
	mpos = 2
}
if global.replay {
	gray[1] = true
}