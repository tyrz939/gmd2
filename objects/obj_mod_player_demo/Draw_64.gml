// Text
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(font_terminal);
draw_set_alpha(1);
draw_set_color(c_ltgray);
/// Draw song information
var xd = 70
draw_text(xd, 40, title);
draw_text(xd, 55, "Tempo: " + string(song_tempo));
draw_text(xd, 70, "Channels: " + string(song_channels));
draw_text(xd, 85, "Length: " + song_length);

var channels = "Playing channels: ";
var channels_vol = "Volume: ";
for(var i=0; i<song_channels; i++) {
  channels += string(!mute[i]) + " ";
  channels_vol += string(volume[i]) + " ";
}
draw_text(xd, 100, channels);
draw_text(xd, 115, channels_vol);

for(var i=0; i<song_channels; i++) {
	MOD_SetChannelVolume(i, round(global.music_vol * 64));
	volume[i] = MOD_GetChannelVolume(i);
}