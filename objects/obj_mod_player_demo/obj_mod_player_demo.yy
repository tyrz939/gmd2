{
    "id": "2af52c4b-0ed1-4411-a0ea-582bd0878a93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mod_player_demo",
    "eventList": [
        {
            "id": "dc1d7f29-5d10-40fe-9d5b-555fd3d1868a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2af52c4b-0ed1-4411-a0ea-582bd0878a93"
        },
        {
            "id": "c2024697-7cbd-4fdf-a9f7-58149693738a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2af52c4b-0ed1-4411-a0ea-582bd0878a93"
        },
        {
            "id": "263f096b-236b-4dca-92f0-8e26e63e0f41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2af52c4b-0ed1-4411-a0ea-582bd0878a93"
        },
        {
            "id": "a9b1ef97-a17e-4929-909f-a68d19c0a5b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2af52c4b-0ed1-4411-a0ea-582bd0878a93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}