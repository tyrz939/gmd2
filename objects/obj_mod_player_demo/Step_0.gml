/// Perform various mod functions

// Play
if(keyboard_check_pressed(vk_enter))
  MOD_Play();

// Pause 
if(keyboard_check_pressed(vk_space)) {
  pause = !pause;
  MOD_Pause(pause);
}
/*
// Stop
if(keyboard_check_pressed(vk_escape))
  MOD_Stop();
  
// Set tempo
if(keyboard_check_pressed(vk_up)) {
  if(song_tempo<400)
    song_tempo++;
  MOD_SetTempo(song_tempo);
}
if(keyboard_check_pressed(vk_down)) {
  if(song_tempo>10)
    song_tempo--;
  MOD_SetTempo(song_tempo);
}*/

// Mute / Unmute / Get channel volume
/*for(var i=0; i<song_channels; i++) {

  if(i <= 8) { // Limit to keyboard keys 1-9
    if(keyboard_check_pressed(ord(string(i+1)))) {
      mute[i] = !mute[i];
      MOD_MuteChannel(i,mute[i]);
	  volume[i] = MOD_GetChannelVolume(i);
    }
  }
}*/

// Set all channel volumes to 10
