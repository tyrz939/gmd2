
song_tempo = 0;
song_channels = 0;
song_length = "";
pause = false;
for(var i=31; i>=0; i--) {
  mute[i] = false;
	volume[i] = 0;
}

// Load a song
// You can download the Scream Tracker mod I used for testing here:
// https://modarchive.org/index.php?request=view_by_moduleid&query=60395
// and include it in your project. Rename it to "second_pm.s3m", seems like
// GMS can add an underscore to the beginning of an included file if the name
// starts with a digit.
//var sound_id = MOD_LoadSong("second_pm.s3m", -1);
var sound_id = MOD_LoadSong("STRCRIS2.it", -1);

// Check if succesful
title = "none";
if(sound_id)
{
  title = MOD_GetName();
  MOD_Play();
  song_channels = MOD_NumChannels();
  song_length = string(MOD_GetLength()/1000) + "s";
  song_tempo = MOD_GetTempo();
}

for(var i=0; i<song_channels; i++) {
	MOD_SetChannelVolume(i, 64);
}