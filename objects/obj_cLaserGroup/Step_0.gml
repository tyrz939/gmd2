if !surface_exists(_surf) {
	var w, h
	w = image_get_width(sprite)
	h = image_get_height(sprite)

	_surf = surface_create(w, h)
	surface_set_target(_surf)
	draw_image_ext(sprite, subimage, w/2, h/2, 1, 1, 0, c_white, 1)
	surface_reset_target()
}