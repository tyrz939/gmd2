// Move
x += (spd * cos (direction * pi / 180)) * game.gamespeed
y += (spd * -sin (direction * pi / 180)) * game.gamespeed

// Bounding box for Marisa Lasers
if hitCircle > 0 {
	mask_index = spr_bboxCircle
	image_xscale = (hitCircle /32) *2
	image_yscale = (hitCircle /32) *2
} else if hitWidth > 0 && hitHeight > 0 {
	mask_index = spr_bboxRectangle
	image_xscale = (hitWidth /32) *2
	image_yscale = (hitHeight /32) *2
}

// Using shot collision because... why not. It's good
if pHitCircle > 0 {
	var _c = shotCollisionCircle(x, y, pHitCircle)
	if _c == 0 {
		playerHit()
	}
} else if pHitWidth > 0 && pHitHeight > 0 {
	var _c = shotCollisionRectangle(x, y, pHitWidth, pHitHeight, direction)
	if _c == 0 {
		playerHit()
	}
}

// Animation
if animationSpeed > 0 && image_exists(sprite) {
		subImage += animationSpeed / 60
	if floor(subImage) >= image_get_number(sprite) {
		subImage = 0
	}
}

// Tween Y position drawing
yDir += 2 * game.gamespeed
yDraw = y + lengthdir_y(5, yDir)

// HP Bar fill animation
if hp > 0 && healthBarShow {
	healthBarFill = clamp(healthBarFill + 1/60, 0, 1)
}


timer++

// Nonspell/Spellcard Shared
if bossState == bossState_t.NONSPELL || bossState == bossState_t.SPELLCARD {
	// Take Damage
	if !spellSurvival {hp = clamp(hp - takeDmg, 0, maxhp)}
	// Phase Counter
	bossPhaseTimer--
	// Bonus Reducer
	if bossPhaseTimerStart > bossPhaseTimer + 300 {
		spellBonus -= (0.75 * spellBonusStart) / (bossPhaseTimerStart - 300)
	}
	// Phase Change
	if (hp <= bossSpellBreakHP && bossState == bossState_t.NONSPELL) || hp == 0 || bossPhaseTimer <= 0 {
		// Spellcard Get
		if bossPhaseTimer > 0 && bossState == bossState_t.SPELLCARD {
			scoreAdd(round(spellBonus))
		}
		spellSurvival = false
		spellName = "SET ME"
		bossPhaseID++
		play_sfx(sfx_phaseBreak, false)
		deleteShotAll(DELTYPE_ANIMATED)
		if bossState == bossState_t.SPELLCARD {
			healthBarShow = false
			healthBarFill = 0
			bossSpellBreakHP = 0
			hp = 0
			maxhp = 0
		} else if bossState == bossState_t.NONSPELL {
			hp = bossSpellBreakHP
		}
		bossState = bossState_t.IDLE
	}
}
// Flash when taking damage
if takeDmg != 0 && !overlayDraw {
	overlayDraw = true
	overlayColor = c_blue
	overlayAlpha = 0.5
} else {
	overlayDraw = false
}
takeDmg = 0

// State Unique
switch bossState {
	case bossState_t.IDLE:
		
		break
	case bossState_t.DIALOG:
		
		break
	case bossState_t.NONSPELL:
		
		break
	case bossState_t.SPELLCARD:
		
		break
}