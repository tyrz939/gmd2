//scr_title_draw();

draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_font(global.font_MainMenu);

var m;
for (m =0; m < array_length_1d(menu); m+=1) {
    //Menu Shadow
/*    draw_set_alpha(0.5);
    draw_set_colour(c_black);
    draw_text(x+2,y+2+(m*space),string(menu[m]));*/
    //Menu
    draw_set_alpha(1);
    if gray[m] {
        draw_set_colour(global.menuGrayColor);
    } else {
        draw_set_colour(global.menuTextColor);
    }
    draw_text(x,y+(m*space),string(menu[m]));
    //Pulsing Selection
    if m == mpos {
        draw_set_alpha(alpha);
        draw_set_colour(global.menuPulseColor);
        draw_text(x,y+(m*space),string(menu[m]));
        draw_set_alpha(1);
    }
}

draw_set_font(font_fps);
draw_set_halign(fa_right);
draw_set_alpha(0.5);
draw_set_color(c_black);
var dx = global.gui_width - 5;
var dy = global.gui_height - 15;
draw_text(dx +1,dy +2,global.name + " v" + GM_version);
draw_set_alpha(1);
draw_set_color(c_white);
draw_text(dx,dy,global.name + " v" + GM_version);