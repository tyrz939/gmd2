if image_exists(game.backgroundGUI) {
	draw_image(game.backgroundGUI, 0, 0, 0)
} else {
	// Solid color background draw
	draw_set_color(bgCol1)
	// Left
	draw_rectangle(0, 0, drawGame.GUIPlayAreaStartX, global.gui_height, false)
	// Right
	draw_rectangle(drawGame.GUIPlayAreaEndX, 0, global.gui_width, global.gui_height, false)
	// Top
	draw_rectangle(drawGame.GUIPlayAreaStartX, 0, drawGame.GUIPlayAreaEndX, drawGame.GUIPlayAreaStartY, false)
	// Bottom
	draw_rectangle(drawGame.GUIPlayAreaStartX, drawGame.GUIPlayAreaEndY, drawGame.GUIPlayAreaEndX, global.gui_height, false)
	
	// Rectangle around play area
	draw_set_color(bgCol2)
	draw_rectangle(drawGame.GUIPlayAreaStartX, drawGame.GUIPlayAreaStartY,
				drawGame.GUIPlayAreaEndX, drawGame.GUIPlayAreaEndY, true)
	draw_set_color(c_white)
}

draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(global.font_MainMenu);
// TODO WRITE A GUI
var xa, ya, col, o

// Temp?
var wideWidth = 384

var narrowHeight = 32

if guiGrid[# 0, 0] != -1 {
	for( var i = 0; i < ds_grid_height(guiGrid); i++ ) {
		xa = anchX + guiGrid[# guiGrid_t.X, i]
		ya = anchY + guiGrid[# guiGrid_t.Y, i]
		col = guiGrid[# guiGrid_t.COLOR, i]
	
		switch guiGrid[# guiGrid_t.TYPE, i] {
			case "wide":
				guiType = wide
				break
			case "narrow":
				guiType = narrow
				break
			case "difficulty":
				draw_sprite(spr_GUIDifficulty, global.difficulty, xa, ya)
				break
			case "score":
				if guiType == wide {
					draw_set_color(col)
					draw_set_halign(fa_left)
					draw_text(xa, ya, "Score")
					draw_set_halign(fa_right)
					draw_text(xa + wideWidth, ya, string_insert_comma(string(score)))
				} else {
					draw_set_halign(fa_center)
					draw_set_color(c_white)
					draw_text(xa, ya, "Score")
					draw_set_color(col)
					draw_text(xa, ya + narrowHeight, string_insert_comma(string(score)))
				}
				break
			case "player":
				if guiType == wide {
					draw_set_halign(fa_left);
					draw_set_color(col)
					draw_text(xa, ya, "Player")
					draw_set_halign(fa_right);
					if in_range(lives, 0, 8) {
						xa += wideWidth
						ya += 16
						for(o = 0; o < 8; o++) {
						    if o >= -clamp(lives, 0, 8) + 8 {
						        draw_sprite_ext(spr_GUIHeart, 0, xa - (o*24) -14, ya, 1, 1, 0, col, 1);
						    } else {
						        draw_sprite_ext(spr_GUIHeart, 0, xa - (o*24) -14, ya, 1, 1, 0, c_black, 1);
						    }
						}
					} else {
						draw_text(xa + wideWidth, ya, string(lives))
					}
				} else {
					draw_set_halign(fa_center);
					draw_set_color(c_white)
					draw_text(xa, ya, "Player")
					if in_range(lives, 0, 8) {
						xa += 24*4
						ya += 16 + narrowHeight
						for(o = 0; o < 8; o++) {
						    if o >= -clamp(lives, 0, 8) + 8 {
						        draw_sprite_ext(spr_GUIHeart, 0, xa - (o*24) -14, ya, 1, 1, 0, col, 1);
						    } else {
						        draw_sprite_ext(spr_GUIHeart, 0, xa - (o*24) -14, ya, 1, 1, 0, c_black, 1);
						    }
						}
					} else {
					draw_set_color(col)
						draw_text(xa + wideWidth, ya, string(lives))
					}
				}
				break
			case "spellcard":
				if guiType == wide {
					draw_set_halign(fa_left);
					draw_set_color(col)
					draw_text(xa, ya, "Spellcard")
					draw_set_halign(fa_right);
					if in_range(game.bombs, 0, 8) {
						xa += wideWidth
						ya += 16
						for(o = 0; o < 8; o++) {
						    if o >= -clamp(game.bombs, 0, 8) + 8 {
						        draw_sprite_ext(spr_GUIStar, 0, xa - (o*24) -14, ya, 1, 1, 0, col, 1);
						    } else {
						        draw_sprite_ext(spr_GUIStar, 0, xa - (o*24) -14, ya, 1, 1, 0, c_black, 1);
						    }
						}
					} else {
						draw_text(xa + wideWidth, ya, string(game.bombs))
					}
				} else {
					draw_set_halign(fa_center);
					draw_set_color(c_white)
					draw_text(xa, ya, "Spellcard")
					if in_range(game.bombs, 0, 8) {
						xa += 24*4
						ya += 16 + narrowHeight
						for(o = 0; o < 8; o++) {
						    if o >= -clamp(game.bombs, 0, 8) + 8 {
						        draw_sprite_ext(spr_GUIStar, 0, xa - (o*24) -14, ya, 1, 1, 0, col, 1);
						    } else {
						        draw_sprite_ext(spr_GUIStar, 0, xa - (o*24) -14, ya, 1, 1, 0, c_black, 1);
						    }
						}
					} else {
						draw_set_color(col)
						draw_text(xa + wideWidth, ya, string(game.bombs))
					}
				}
				break
			case "power":
				if guiType == wide {
					draw_set_color(col)
					draw_set_halign(fa_left);
					draw_text(xa, ya, "Power")
					draw_set_halign(fa_right);
					draw_text(xa + wideWidth, ya, string_format(game.shotPower, 1, 2) + "/" + string_format(player.shotPowerMax, 1, 2))
				} else {
					draw_set_halign(fa_center)
					draw_set_color(c_white)
					draw_text(xa, ya, "Power")
					draw_set_color(col)
					draw_text(xa, ya + narrowHeight, string_format(game.shotPower, 1, 2) + "/" + string_format(player.shotPowerMax, 1, 2))
				}
				break
			case "graze":
				if guiType == wide {
					draw_set_color(col)
					draw_set_halign(fa_left);
					draw_text(xa, ya, "Graze")
					draw_set_halign(fa_right);
					draw_text(xa + wideWidth, ya, string_insert_comma(string(floor(game.grazeCount))))
				} else {
					draw_set_halign(fa_center)
					draw_set_color(c_white)
					draw_text(xa, ya, "Graze")
					draw_set_color(col)
					draw_text(xa, ya + narrowHeight, string_insert_comma(string(floor(game.grazeCount))))
				}
				break
			default:
				print_error("Gui option unknown '" + string(guiGrid[# guiGrid_t.TYPE, i]) + "'")
		}
	}
	draw_set_color(c_white)
}

with obj_boss {
	var _xxs, _xxs, _yys1, _yys0, _yys
	/********* Draw Marker at Bottom of Screen *********/
	_xxs =  (global.playAreaStartX * drawGame.__xyOffset + x * drawGame.__xyOffset) / drawGame.__guiScale
	_yys0 = global.gui_height
	_yys1 = drawGame.GUIPlayAreaEndY
	
	draw_primitive_begin(pr_trianglestrip);
		draw_vertex_color(_xxs-(32 * drawGame.__guiScale), _yys0, c_red, 0);
		draw_vertex_color(_xxs-(32 * drawGame.__guiScale), _yys1, c_red, 0);
		
		draw_vertex_color(_xxs, _yys0, c_red, 0.5);
		draw_vertex_color(_xxs, _yys1, c_red, 0.5);
		
		draw_vertex_color(_xxs+(32 * drawGame.__guiScale), _yys0, c_red, 0);
		draw_vertex_color(_xxs+(32 * drawGame.__guiScale), _yys1, c_red, 0);
	draw_primitive_end();
	
	/********* Draw Spellcard Information *********/
	if bossState == bossState_t.SPELLCARD {
		draw_set_halign(fa_right)
		draw_set_color(c_white)
		draw_set_valign(fa_top);
		draw_set_font(global.font_MainMenu);
		
		_xxs = drawGame.GUIPlayAreaEndX
		if healthBarType == 0	{_yys = global.playAreaStartY}
		else					{_yys = global.playAreaStartY + 24}
		
		var col1 = make_color_rgb(255, 127, 127)
		var col2 = make_color_rgb(255, 191, 191)
		draw_primitive_begin(pr_trianglestrip);
			draw_vertex_color(_xxs-15, _yys + 56, col1, 1);
			draw_vertex_color(_xxs-15, _yys + 60, col1, 1);
			
			draw_vertex_color(_xxs-500, _yys + 56, col2, 1);
			draw_vertex_color(_xxs-500, _yys + 60, col2, 1);
			draw_vertex_color(_xxs-600, _yys + 56, col2, 0);
			draw_vertex_color(_xxs-600, _yys + 60, col2, 0);
		draw_primitive_end();
		
		draw_text(_xxs - 15, _yys + 20, spellName)
		
		draw_set_halign(fa_left)
		draw_set_font(font_terminal);
		draw_set_color(col1)
		draw_text(_xxs - 192, _yys + 64, "Bonus")
		draw_set_color(c_white)
		draw_text(_xxs - 128, _yys + 64, round(spellBonus))
		
	}
	
	/********* Draw Boss Spell Timer *********/
	if bossState == bossState_t.NONSPELL || bossState == bossState_t.SPELLCARD {
		draw_set_color(c_white)
		draw_set_valign(fa_top);
		draw_set_font(global.font_MainMenu);
		
		if healthBarType == 0 {
			draw_set_halign(fa_center)
			// Y Pos
			if bossState == bossState_t.NONSPELL	{_yys = global.playAreaStartY + 16} 
			else									{_yys = global.playAreaStartY + 64}
			// X pos
			_xxs =  (global.playAreaStartX * drawGame.__gameScale + (global.playAreaWidth / 2) * drawGame.__gameScale) / drawGame.__guiScale
		} else {
			draw_set_halign(fa_right)
			// Y Pos
			_yys = global.playAreaStartY + 10
			// X pos
			_xxs = drawGame.GUIPlayAreaEndX - 10
		}
		
		draw_text(_xxs, _yys, floor(bossPhaseTimer / 60))
	}
}