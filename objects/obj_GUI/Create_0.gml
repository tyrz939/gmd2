enum guiGrid_t {
	TYPE,
	X,
	Y,
	COLOR,
	LENGTH
}

guiGrid = ds_grid_create(guiGrid_t.LENGTH, 1)
guiGrid[# 0, 0] = -1
gridIterator = -1

anchX = 0
anchY = 0

wide = 0
narrow = 1

guiType = wide

bgCol1 = make_color_rgb(25, 0, 25)
bgCol2 = c_blue
	