event_inherited()

var inst = instance_nearest(x, y, obj_enemy)

// 3 jumps and you're out
if inst != prevInst {jumps++}
prevInst = inst
if spd < 10 {spd += 0.1}

if inst != noone && jumps <= 3 {
	var dirEnemy = point_direction(x, y, inst.x, inst.y)
	direction = turn_smooth( direction, dirEnemy, turnSpeed * spd )
}

turnSpeed += 3/180

image_angle = direction