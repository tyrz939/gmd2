/// @description Game Step Event

/********************
Main Loop Equivalent
********************/

if unpauseNextFrame {
	pause = false
	unpauseNextFrame = false
	part_system_automatic_update(global.partSys, true)
}

if (input.esc && !pause) || os_is_paused() || frame == replayEndFrame {
	pause = true
	if !instance_exists(obj_ingameMenuParent) {
		var _xs = 50, _ys = 150
		instance_create_depth(_xs, _ys, INTERFACE_DEPTH, obj_pauseMenu)
	}
}

if !pause {
	frame++
	random_set_seed(frame)
} else {
	part_system_automatic_update(global.partSys, false)
}

if !pause {
	/**********
	Lua Runner
	**********/
	live_call("step")
	scr_stepDeleter()
	
	/************************
	Game Object Event Runner
	************************/
	with player					{event_user(0)}
	with obj_playerOrb			{event_user(0)}
	with obj_playerShotParent	{event_user(0)}
	with obj_deleteTimer		{event_user(0)}
	with obj_enemy				{event_user(0)}
	with obj_bulletParent		{event_user(0)}
	with obj_pickup				{event_user(0)}
	
	with obj_SharedTweener		{event_user(0)}
	with drawGame				{event_user(0)}
	
//	if !instance_exists(obj_mod_player_demo) {
//		instance_create_depth(0, 0, 0, obj_mod_player_demo)
//	}
}