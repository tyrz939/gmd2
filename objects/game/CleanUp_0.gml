image_system_cleanup()
live_cleanup()
surface_free(surface3d)
part_system_clear(global.partSys);

ds_grid_destroy(shotCreateGrid)

ds_grid_destroy(scene3d)

ds_grid_destroy(bulletRemover)

// Loaded Asset Stuffs
ds_grid_destroy(importGrid)

// Loaded Sound Cleanup
if soundGrid[# 0, 0] != 0 { // Dodge check to see if you've set sounds
	for(var i = 0; i < ds_grid_height(soundGrid); i++) {
		audio_free_buffer_sound(soundGrid[# 1, i])
		buffer_delete(soundGrid[# 0, i])
	}
	ds_grid_destroy(soundGrid)
}