// Misc
	depth = 0
	random_set_seed(0)

// Initialize Systems
	scr_initParticles()
	scr_initShots()
	instance_create_depth(0, 0, INTERFACE_DEPTH, obj_GUI)
	instance_create_depth(0, 0, BOSS_HP_DEPTH, obj_bossHPBar)
	instance_create_depth(0, 0, BG_DEPTH, draw3DBGandDistortion)
	
	soundGrid = ds_grid_create(3, 0)

// Setting veriables for the game elements.
	
	// Stage
	stageNumber = 0
	stageObject = noone
	stagePosition = 0

	// DEFAULTS
	backgroundGUI = -1
	bossHPBarType = 0
	
	shotSetDefaultDelay(10)
	playFieldSetup(32, 16, 384, 448)
	shotAutoDeleteEdges(64, 64, 64, 64) // Do not run before the one above
	
	boundaryLeft = noone
	boundaryRight = noone
	boundaryTop = noone
	boundaryBottom = noone

	playerSpawnX = noone
	playerSpawnY = noone
	playerGoToX = noone
	playerGoToY = noone
	
	// Game
	pause = false
	unpauseNextFrame = false
    godMode = false
	frame = 0
	replay = 0
	gamespeed = 1
	score = 0
	grazeCount = 0
	replayEndFrame = -1 // Replay end frame
	global.tweenMove = EaseOutQuad
	global.tweenTurn = EaseInOutQuad
	
	lives = 2
	bombs = 2
	shotPower = 0
	continues = 2
	pointOfCollect = 130
	
	// Bullet Remover Array / Variables
	scr_initDeleter()
	
	// 3D Variables
	scr_init3d()
	
	// Boss Distortion
	scr_initBossDistortion()
	
// LUA / Loading
	loading = true
	loadingState = 0
	_li = 0
	game_set_speed(300, gamespeed_fps);
	instance_create_depth(0, 0, LOAD_DEPTH, obj_loading)
	visible = false
	with obj_GUI {visible = false}
	loadingPrint("Initializing Lua")