{
    "id": "24839646-76a3-4042-a79a-0be5f0869532",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mod_player",
    "eventList": [
        {
            "id": "6888ace5-55e2-4f94-bbb3-5ac346a9e452",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24839646-76a3-4042-a79a-0be5f0869532"
        },
        {
            "id": "f9ee6697-8376-40e1-a3db-ed54874ef827",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24839646-76a3-4042-a79a-0be5f0869532"
        },
        {
            "id": "9526f6fd-0b89-429a-9e6a-8b300365d3ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 7,
            "m_owner": "24839646-76a3-4042-a79a-0be5f0869532"
        },
        {
            "id": "c0ec2f07-9682-46a4-828d-e4e921c0ec91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "24839646-76a3-4042-a79a-0be5f0869532"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}