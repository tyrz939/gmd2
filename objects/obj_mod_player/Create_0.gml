
// Instance variables
snd_queue = noone;
buffer_mod = noone;
buffer_mod_address = 0;
buffer_mod_size = 0;
playing = false;
end_reached = true;

// Local variables
var i;

// Buffer size in the dll is 760, so we need to match that
buffer_size = 760; // this used to be 16384 in version 1.0 of the extension, smaller buffers even out fps

// The number of audio buffers
buffer_count = 7;

// Buffer index is used to keep track of current buffer to fill / add to queue
buffer_index = 0;

// Sample rate is hardcoded to 44100 in the dll
sample_rate = 44100;

// Create the audio buffers
for( i=0; i<buffer_count; i++ ) {
	buf[i] = buffer_create(buffer_size, buffer_fixed, 2);
	buf_pointer[i] = string(buffer_get_address(buf[i])); 
	buffer_fill(buf[i], 0, buffer_u8, 0, buffer_size);
}

count = 0