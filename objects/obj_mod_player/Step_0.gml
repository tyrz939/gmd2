

// The following is a check to refill audio buffers
// if playing has stopped unexpectedly from moving
// the main window in Windows etc.
if( playing && !end_reached && !audio_is_playing(snd_queue) ) {
	var bytesReturned = 0;
  
	// Fill all the audio buffers with song data and add to queue
	for( i=0; i<buffer_count; i++ ) {
		bytesReturned = ModPlug_Read(buf_pointer[i]);
		if( bytesReturned == 0 ) {
			end_reached = true;
			break;
		} else {
			audio_queue_sound(snd_queue, buf[i], 0, bytesReturned);
		}
	}
	// Play the queue
	audio_play_sound(snd_queue, 10, false);
}

