if delay > timerSpawn {
	timerSpawn++
	// Move
	x += ((spd/2) * cos (direction * pi / 180)) * game.gamespeed
	y += ((spd/2) * -sin (direction * pi / 180)) * game.gamespeed
	image_alpha += delayAlphaDiff / delay
	image_xscale -= (delayScale-1) / delay
	image_yscale -= (delayScale-1) / delay
} else {
	timerStep++
	// Acceleration / Deceleration
	if accelState != 0 {
		// Wait for delay time
		if accelDelay <= timerStep {
			// Add to the speed (if numbers negative, add will decelerate)
			spd += accelStepSpeed
			// Change state to 0 when done
			if timerStep >= accelTime + accelDelay {
				accelState = 0
				spd = accelFinalSpeed
			}
		}
	}
	// Move
	x += (spd * cos (direction * pi / 180)) * game.gamespeed
	y += (spd * -sin (direction * pi / 180)) * game.gamespeed

	// Collision
	if instance_exists(player) {
		var __inst = player.id
	} else {
		var __inst = noone
	}
	if __inst != noone {
		switch hitType {
			case CIRCLE:
				var __c = shotCollisionCircle(x, y, hitRadius)
				if __c != noone {
					if __c == 0 {
						playerHit()
					}
					if deleteType >= 1 {
						part_particles_create_colour(global.partSys, x, y, global.partShotDie, shotColor, 1)
					}
					instance_destroy()
				}
				break;
			case OVAL:
				var __c = shotCollisionOval(x, y, hitWidth, hitHeight, direction)
				if __c != noone {
					if __c == 0 {
						playerHit()
					}
					if deleteType >= 1 {
						part_particles_create_colour(global.partSys, x, y, global.partShotDie, shotColor, 1)
					}
					instance_destroy()
				}
				break;
			case RECTANGLE:
				var __c = shotCollisionRectangle(x, y, hitWidth, hitHeight, direction)
				if __c != noone {
					if __c == 0 {
						playerHit()
					}
					if deleteType >= 1 {
						part_particles_create_colour(global.partSys, x, y, global.partShotDie, shotColor, 1)
					}
					instance_destroy()
				}
				break;
		}
	}

	//Clean up once out of play field
	if autoDelete {
		if x < global.shotDeleteLeft || x > global.shotDeleteRight || y < global.shotDeleteTop || y > global.shotDeleteBottom {
		    instance_destroy()
		}
	}
}