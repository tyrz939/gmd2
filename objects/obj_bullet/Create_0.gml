image_speed = 0
autoDelete = true
deleteType = 0
grazeAdd = 0.05

shotType = global.__shotType

spd = global.__shotSpeed
direction = global.__shotDirection

sprite = global.__shotSpriteIndex
subimage = global.__shotImageIndex
shotColor = merge_color(global.__shotColor, c_white, 0.5)

hitType = global.__shotHitType
hitRadius = global.__shotHitRadius
hitWidth = global.__shothitWidth
hitHeight = global.__shothitHeight

drawRotated = global.__shotDrawRotated
drawRounded = global.__shotDrawRounded
blendType = bm_normal

// 0 = no, 1 = accelerating, 2 = decelerating
accelState = 0
accelStepSpeed = 0
accelDelay = 0

timerSpawn = 0
timerStep = 0
delay = global.__shotDelay
delayScale = 2.5
delayAlphaDiff = 0.75

__amountLeft = 1

if delay > 0 {
	image_alpha = 1 - delayAlphaDiff
	image_xscale = delayScale
	image_yscale = delayScale
}