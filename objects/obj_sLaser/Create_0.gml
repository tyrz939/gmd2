image_speed = 0
autoDelete = true
deleteType = 0
grazeAdd = 0.05
spd = 0

_length = global.__LaserLength
_width = global.__LaserWidth
_duration = global.__LaserDuration

direction = global.__shotDirection

sprite = global.__shotSpriteIndex
subimage = global.__shotImageIndex
shotColor = merge_color(global.__shotColor, c_white, 0.5)
shotType = global.__shotType

hitType = global.__shotHitType
hitRadius = global.__shotHitRadius
hitWidth = global.__shothitWidth
hitHeight = _length / 2
timerSpawn = 0
timerStep = 0
blendType = bm_add

spriteHeight = image_get_height(sprite)
spriteWidth = image_get_width(sprite)

delay = global.__shotDelay

// Expanding the laser
aniSpeed = 0.05
aniFinalScale = _width / spriteWidth
aniInScaleSteps = aniFinalScale * aniSpeed
// Expanding the hitbox
hitWidth = 0
hitWidthSteps = (global.__shothitWidth * aniFinalScale) * aniSpeed

// Spawn point sprite
_spawnSprite = global.__LaserSpawnSprite
_spawnSubimage = global.__LaserSpawnSubimage
_spawnAngle = 0

_spawnScale = 48 / image_get_height(_spawnSprite)
_spawnAlpha = 0
if delay > 0 {
	_spawnAlphaStep = 1/delay
} else {
	_spawnAlphaStep = 1
}


__amountLeft = 1

if delay > 0 {
	image_alpha = 1
	image_xscale = 0
	image_yscale = _length / spriteHeight
}