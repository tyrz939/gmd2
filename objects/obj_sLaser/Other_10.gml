if delay >= timerSpawn {
	timerSpawn++
	_spawnAlpha += _spawnAlphaStep
} else if _duration > timerStep {
	timerStep++
	if image_xscale < aniFinalScale {
		image_xscale += aniInScaleSteps
		hitWidth += hitWidthSteps
	}
	
	if instance_exists(player) {
		var __inst = player.id
	} else {
		var __inst = noone
	}
	var _xx = x + lengthdir_x(_length / 2, direction)
	var _yy = y + lengthdir_y(_length / 2, direction)
	
	// hitHeight and hitWidth are meant to be swapped here
	var __c = shotCollisionRectangle(_xx, _yy, hitHeight, hitWidth, direction)
	if __c != noone {
		if __c == 0 {
			playerHit()
		}
	}
} else {
	image_xscale	-= aniSpeed
	image_alpha		-= aniSpeed
	_spawnAlpha		-= aniSpeed
	if image_xscale <= 0 {instance_destroy()}
}
_spawnAngle += 15