alpha -= 2/120

image_alpha = clamp(alpha, 0, 1)

if y < 0 {instance_destroy()}

draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(font_terminal);
draw_set_alpha(1);
draw_set_color(c_ltgray);

draw_text_custom(x, y, text, image_alpha)