{
    "id": "f98e7a6d-5bbe-40b3-b240-e0bc275b5ce3",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_terminal",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Unispace",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "735d4d49-8149-40e5-9517-cb9499da987f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "844e158a-40e3-495c-a47a-b653497eb85a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 14,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "17f5bd1e-8b29-4e2d-90d6-a6f4c6e34046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 213,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ab69ed5c-7ef9-4801-ade1-d0b6601824a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2d0976b1-a6ff-4d55-8c3e-b62988b3b5a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5aa51f10-dce1-4e78-a7ea-1c37d2bca5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a8ba8a5c-ed37-4e6b-b25c-ff9db4db4490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1d8a0975-7956-4b82-8ebd-744e547b1754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 26,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1bb15c0b-38f1-4070-8deb-5ada971a4b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 197,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8a45af2d-3955-4727-813d-73dc49a8e036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 229,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d77473e7-1762-46f2-836f-21ff83658131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "497f32fc-e9fc-47f8-a794-200e1f6d28cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "66724ce6-116c-4213-99de-098830511694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6d998b95-e9bc-49cb-9d66-27baa5fa06e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "097fbc9e-2221-4dc0-a314-9ebf4f2b9ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 245,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f3698033-ea5d-4b89-95b4-0ce292a9d7ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2faff284-3436-4059-b7b9-959399369423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4306bce8-5100-4d5e-bb49-74c1f5d115fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 179,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1ec5d5fd-e551-42b0-b67f-b738591f4428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ac3e1937-d970-4ec3-af45-a6b508194224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 206,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a989715f-9e08-4bc2-a265-8ccb2826cee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d57b838e-bad9-44d7-a091-42562134e939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ae707ec8-2c1f-43a9-9216-4e03ba775357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 218,
                "y": 23
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9ec09802-cc0c-4177-842b-525b0e1dce07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "336c0133-4a18-4b38-8fae-7a551f063347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 23
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "867df5a1-bd42-4f15-9917-6e960f7c024c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3337317d-1a3c-42f1-b08d-68458acc19b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 20,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "72959e5f-69bd-44e9-83a5-262331d4b399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 8,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "93877039-d0d0-4beb-944b-e26cc9ea3593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 188,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "beff6ce8-9569-4046-b3b8-c2405436d50a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 169,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f56f6068-5c12-4bc6-af39-f314d433a008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 179,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "798ffdb8-07f8-43f2-893c-899b68e0aaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ac4e6eb9-3d45-4120-a91f-9591f14996f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9abe3d8a-cba6-4ace-a842-cc4ba8af5c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "63c82b5a-c1ce-4c2d-b682-0b4b3831a42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7d9ccc54-7fa7-49cc-b0d3-ca6d8772e29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 109,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4f46d56b-5ff5-4679-a7af-5efb33b03a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4884887f-1c80-41a8-abea-1d66a12d1e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ad2bbaf3-f950-4597-8b56-1f505e95af2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "687561a6-6e4b-4a06-8713-60057ef1f2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "40d8c06f-a224-46a9-8a39-582d85fc3176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 170,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9a8e6705-175c-4ea9-941a-2dc7c610a4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "afe73690-521b-49ea-8598-ebcbe3110c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b9afdc0f-3b06-4235-bf64-9cadd84e57f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 158,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2fe0c770-74cd-42be-93ae-850d84b328c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1fe967bb-0b46-4cdc-b947-928f0cd9a88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f7ac7526-db90-4e3e-b440-4a40476f612c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a867e0cc-e034-4fae-b93e-1350ba07543e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1079354d-344f-444e-a243-5c3475432346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c6f590df-0fab-4113-ab64-0467ab23bb6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "85dcd869-3f29-4249-b3f5-c1523267a023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "979e335a-417d-4c9c-b69c-31408109400c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4c5fb4eb-6ba4-4df4-8dd1-c9c3a3c1b060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ec61591e-e4ea-490d-b706-bfedc3662704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c4357cdd-1941-4e66-a82f-6b8aa553b68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4643fb71-7946-4b90-962c-3121b752f9d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "71511e9c-90c8-4d75-8aab-d9431551efd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "56ce9cfb-c49b-4624-b81a-2bf64bc4903e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "67bd2b43-e557-43ba-9a08-fa35c700e45a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aa6030c0-fdf7-4af2-ba66-c29e3af86ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 237,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "34990b7f-f428-4b8c-949b-cabe832f7702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f96d7794-a034-4c93-ad41-cb26c01111e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 205,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9cc3f5c3-4f54-400e-896d-373099911859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4402f510-5212-431d-9ba5-4c3db3df0276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4db08705-ec65-4801-9a12-95ba4bd53cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 221,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "721d5520-f535-4682-b497-7c4e80d7d94f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "86e79ed0-01a8-456a-bf74-b2beb6c0035c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "aee2d836-1208-401f-aa98-4bd826e4c932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 168,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "452da582-12ed-4c61-8afc-9af5de519eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9990a423-b9e9-4e0c-bc30-eb6a9e93156d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7f93a346-20f7-473d-9844-802e7e27be3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ffe4ec41-c5ef-40f5-8f64-21748c71d010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1aa1ed14-eaeb-4055-951f-9bc8c65c27a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 122,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "22135873-2524-4968-8acc-9510a2730c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "18b40c15-8277-48ad-8719-1c08d6dbefbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 149,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "93611773-939f-4d55-b9c3-36ffced28661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "800c05c0-2e8e-45df-8be7-0547f0386583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 157,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d32d6ad8-f643-4032-82f3-fae44c9f4645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "fd2abde7-ebe3-4aa7-a289-78a2d05b53b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "17d20f30-0295-4aac-85d0-8bd275bac90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "13eef812-f5d0-451c-85c7-e101282458d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "07070ee3-0c32-4c26-a3e5-7841008ad45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ba5a3c3f-d105-4970-8db7-5b6ad5c2aaba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 119,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "74de5cd5-3d43-4b4d-9fb5-84b8730864e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ec8d6e66-8094-40d6-b340-a795a66d56ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 190,
                "y": 44
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ca096ad4-7d50-486d-aaa0-a6145f2da569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1ad4eeee-8d79-4511-8c43-c460cfe38684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "43d12d72-96cd-442c-b6df-4a1026c8f9c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c307224d-a9e6-47b1-bc04-8b58713290fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d0b1e97c-233a-4689-ad39-ab69a73318a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a70cb7ae-80ef-487d-9b8a-3d410d9c7035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 201,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "99343f11-8e68-4270-b0b0-b88d085da6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 89,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "912784e0-8f60-479c-a7f1-1d0d6d14e6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 31,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1ffb068f-343d-4eec-a97f-4755f242a0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 159,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "72d766b5-35a8-4a77-b95f-7205ba4623b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "045c6d76-b8dd-43ca-a654-5a03408920da"
}