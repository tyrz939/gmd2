
local S = {}

function S.loadAssets3d(group)
	local bg = gmDir .. '3dBackground/'
	image_stream_add_3d(group, 'img3d_floor', bg .. 'floor.png', 1, 0, 0)
end

function S.setAssets3d(group)
	-- Saving pointers to the images
	floor3d = image_group_find_image(group, 'img3d_floor')
end

function S.init()
	-- Initial values
	d3dSetFog(2048, 4096, c_black, c_black)
	d3dSetX(0)
	d3dSetY(0)
	d3dSetZ(768)
	d3dSetYaw(270)
	d3dSetPitch(25)
	
	d3dAddSetBlend(bm_add)
	d3dAddSetColor(make_color_rgb(255, 63, 0))
	d3dAddSetAlpha(0.3)
	d3dAddFloor(-2048, 0, -1024, 2048, 1024*8, -1024, floor3d, 16, 32)
	d3dAddFloor(-2048, 0, -512, 2048, 1024*8, -512, floor3d, 16, 32)
	d3dAddFloor(-2048, 0, 0, 2048, 1024*8, 0, floor3d, 16, 32)
end

local d = 0
function S.step()
	d3dSetYaw(270 + lengthdir_y(15, d))
	d = d + 0.1
	
	local y = d3dGetY() + 15
	d3dSetY(y)
	if y > 2048 then d3dSetY(y - 2048) end
end

--[[function S.draw3d()
	d3d_draw_floor(-2048, 0, 0, 2048, 1024*8, 0, floor3d, 16, 32)
end]]

return S