
local S = {}

function S.loadAssets(group)
	local bg = gmDir .. 'ShotSheet/'
	image_stream_add_ext(group, 'SHOT_16x16', bg .. 'bullets.png', 16*14, 8, 8, 0, 0, 16, 16, 16, 0, 0)
	image_stream_add_ext(group, 'SHOT_W_8x8', bg .. 'bullets.png', 16, 4, 4, 0, 16*14, 8, 8, 8, 0, 0)
	image_stream_add_ext(group, 'SHOT_B_8x8', bg .. 'bullets.png', 16, 4, 4, 8*8, 16*14, 8, 8, 8, 0, 0)
	image_stream_add_ext(group, 'SHOT_S_8x8', bg .. 'bullets.png', 16, 4, 4, 8*16, 16*14, 8, 8, 8, 0, 0)
	
	image_stream_add_ext(group, 'CLASER_256x16', bg .. 'bullets.png', 16, 128, 8, 768, 0, 256, 16, 1, 0, 0)
end

function S.setAssets(group)
	-- Saving pointers to the images
	local SHOT_16x16 = image_group_find_image(group, "SHOT_16x16")
	local SHOT_W_8x8 = image_group_find_image(group, "SHOT_W_8x8")
	local SHOT_B_8x8 = image_group_find_image(group, "SHOT_B_8x8")
	local SHOT_S_8x8 = image_group_find_image(group, "SHOT_S_8x8")
	
	local CLASER_256x16 = image_group_find_image(group, "CLASER_256x16")
	
	-- Initial values
	local r = 0
	local _radius, _width, _height
	
	_radius = 0
	SLASER_GRAY = 		ShotAddSLaser(SHOT_16x16, 0, c_ltgray, 	6, SHOT_16x16, 0 + 48)
	SLASER_DKRED = 		ShotAddSLaser(SHOT_16x16, 1, c_red, 	6, SHOT_16x16, 1 + 48)
	SLASER_RED = 		ShotAddSLaser(SHOT_16x16, 2, c_red, 	6, SHOT_16x16, 2 + 48)
	SLASER_PURPLE = 	ShotAddSLaser(SHOT_16x16, 3, c_purple, 	6, SHOT_16x16, 3 + 48)
	SLASER_PINK = 		ShotAddSLaser(SHOT_16x16, 4, c_fuchsia, 6, SHOT_16x16, 4 + 48)
	SLASER_DKBLUE = 	ShotAddSLaser(SHOT_16x16, 5, c_blue, 	6, SHOT_16x16, 5 + 48)
	SLASER_BLUE = 		ShotAddSLaser(SHOT_16x16, 6, c_blue, 	6, SHOT_16x16, 6 + 48)
	SLASER_DKAQUA = 	ShotAddSLaser(SHOT_16x16, 7, c_aqua, 	6, SHOT_16x16, 7 + 48)
	SLASER_AQUA = 		ShotAddSLaser(SHOT_16x16, 8, c_aqua, 	6, SHOT_16x16, 8 + 48)
	SLASER_DKGREEN = 	ShotAddSLaser(SHOT_16x16, 9, c_green, 	6, SHOT_16x16, 9 + 48)
	SLASER_GREEN = 		ShotAddSLaser(SHOT_16x16, 10, c_lime, 	6, SHOT_16x16, 10 + 48)
	SLASER_DKYELLOW = 	ShotAddSLaser(SHOT_16x16, 11, c_yellow, 6, SHOT_16x16, 11 + 48)
	SLASER_YELLOW = 	ShotAddSLaser(SHOT_16x16, 12, c_yellow, 6, SHOT_16x16, 12 + 48)
	SLASER_BROWN = 		ShotAddSLaser(SHOT_16x16, 13, c_orange, 6, SHOT_16x16, 13 + 48)
	SLASER_ORANGE = 	ShotAddSLaser(SHOT_16x16, 14, c_orange, 6, SHOT_16x16, 14 + 48)
	SLASER_WHITE = 		ShotAddSLaser(SHOT_16x16, 15, c_white, 	6, SHOT_16x16, 15 + 48)
	r = r + 16
	_radius = 4
	SCALE_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, 270, false)
	SCALE_DKRED = 		ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, 270, false)
	SCALE_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, 270, false)
	SCALE_PURPLE = 		ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, 270, false)
	SCALE_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, 270, false)
	SCALE_DKBLUE = 		ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, 270, false)
	SCALE_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, 270, false)
	SCALE_DKAQUA = 		ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, 270, false)
	SCALE_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, 270, false)
	SCALE_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, 270, false)
	SCALE_GREEN = 		ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, 270, false)
	SCALE_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, 270, false)
	SCALE_YELLOW = 		ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, 270, false)
	SCALE_BROWN = 		ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, 270, false)
	SCALE_ORANGE = 		ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, 270, false)
	SCALE_WHITE = 		ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, 270, false)
	r = r + 16
	_radius = 4
	BALL_MO_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, -1, true)
	BALL_MO_DKRED = 	ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, -1, true)
	BALL_MO_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, -1, true)
	BALL_MO_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, -1, true)
	BALL_MO_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, -1, true)
	BALL_MO_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, -1, true)
	BALL_MO_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, -1, true)
	BALL_MO_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, -1, true)
	BALL_MO_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, -1, true)
	BALL_MO_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, -1, true)
	BALL_MO_GREEN = 	ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, -1, true)
	BALL_MO_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, -1, true)
	BALL_MO_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, -1, true)
	BALL_MO_BROWN = 	ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, -1, true)
	BALL_MO_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, -1, true)
	BALL_MO_WHITE = 	ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, -1, true)
	r = r + 16
	_radius = 4
	BALL_M_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, -1, false)
	BALL_M_DKRED = 		ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, -1, false)
	BALL_M_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, -1, false)
	BALL_M_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, -1, false)
	BALL_M_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, -1, false)
	BALL_M_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, -1, false)
	BALL_M_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, -1, false)
	BALL_M_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, -1, false)
	BALL_M_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, -1, false)
	BALL_M_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, -1, false)
	BALL_M_GREEN = 		ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, -1, false)
	BALL_M_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, -1, false)
	BALL_M_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, -1, false)
	BALL_M_BROWN = 		ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, -1, false)
	BALL_M_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, -1, false)
	BALL_M_WHITE = 		ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, -1, false)
	r = r + 16
	_width = 4; _height = 2;
	RICE_M_GRAY = 		ShotAddOval(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	RICE_M_DKRED = 		ShotAddOval(SHOT_16x16, r+1, c_red,		_width, _height, 270, false)
	RICE_M_RED = 		ShotAddOval(SHOT_16x16, r+2, c_red,		_width, _height, 270, false)
	RICE_M_PURPLE = 	ShotAddOval(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	RICE_M_PINK = 		ShotAddOval(SHOT_16x16, r+4, c_fuchsia,	_width, _height, 270, false)
	RICE_M_DKBLUE = 	ShotAddOval(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	RICE_M_BLUE = 		ShotAddOval(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	RICE_M_DKAQUA = 	ShotAddOval(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	RICE_M_AQUA = 		ShotAddOval(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	RICE_M_DKGREEN = 	ShotAddOval(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	RICE_M_GREEN = 		ShotAddOval(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	RICE_M_DKYELLOW = 	ShotAddOval(SHOT_16x16, r+11, c_yellow,	_width, _height, 270, false)
	RICE_M_YELLOW = 	ShotAddOval(SHOT_16x16, r+12, c_yellow,	_width, _height, 270, false)
	RICE_M_BROWN = 		ShotAddOval(SHOT_16x16, r+13, c_orange,	_width, _height, 270, false)
	RICE_M_ORANGE = 	ShotAddOval(SHOT_16x16, r+14, c_orange,	_width, _height, 270, false)
	RICE_M_WHITE = 		ShotAddOval(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_width = 4; _height = 2;
	KUNAI_M_GRAY = 		ShotAddOval(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	KUNAI_M_DKRED = 	ShotAddOval(SHOT_16x16, r+1, c_red,		_width, _height, 270, false)
	KUNAI_M_RED = 		ShotAddOval(SHOT_16x16, r+2, c_red,		_width, _height, 270, false)
	KUNAI_M_PURPLE = 	ShotAddOval(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	KUNAI_M_PINK = 		ShotAddOval(SHOT_16x16, r+4, c_fuchsia,	_width, _height, 270, false)
	KUNAI_M_DKBLUE = 	ShotAddOval(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	KUNAI_M_BLUE = 		ShotAddOval(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	KUNAI_M_DKAQUA = 	ShotAddOval(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	KUNAI_M_AQUA = 		ShotAddOval(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	KUNAI_M_DKGREEN = 	ShotAddOval(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	KUNAI_M_GREEN = 	ShotAddOval(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	KUNAI_M_DKYELLOW = 	ShotAddOval(SHOT_16x16, r+11, c_yellow,	_width, _height, 270, false)
	KUNAI_M_YELLOW = 	ShotAddOval(SHOT_16x16, r+12, c_yellow,	_width, _height, 270, false)
	KUNAI_M_BROWN = 	ShotAddOval(SHOT_16x16, r+13, c_orange,	_width, _height, 270, false)
	KUNAI_M_ORANGE = 	ShotAddOval(SHOT_16x16, r+14, c_orange,	_width, _height, 270, false)
	KUNAI_M_WHITE = 	ShotAddOval(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_width = 5; _height = 3;
	CRYSTAL_M_GRAY = 	ShotAddOval(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	CRYSTAL_M_DKRED = 	ShotAddOval(SHOT_16x16, r+1, c_red,		_width, _height, 270, false)
	CRYSTAL_M_RED = 	ShotAddOval(SHOT_16x16, r+2, c_red,		_width, _height, 270, false)
	CRYSTAL_M_PURPLE = 	ShotAddOval(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	CRYSTAL_M_PINK = 	ShotAddOval(SHOT_16x16, r+4, c_fuchsia,	_width, _height, 270, false)
	CRYSTAL_M_DKBLUE = 	ShotAddOval(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	CRYSTAL_M_BLUE = 	ShotAddOval(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	CRYSTAL_M_DKAQUA = 	ShotAddOval(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	CRYSTAL_M_AQUA = 	ShotAddOval(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	CRYSTAL_M_DKGREEN = ShotAddOval(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	CRYSTAL_M_GREEN = 	ShotAddOval(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	CRYSTAL_M_DKYELLOW =ShotAddOval(SHOT_16x16, r+11, c_yellow,	_width, _height, 270, false)
	CRYSTAL_M_YELLOW = 	ShotAddOval(SHOT_16x16, r+12, c_yellow,	_width, _height, 270, false)
	CRYSTAL_M_BROWN = 	ShotAddOval(SHOT_16x16, r+13, c_orange,	_width, _height, 270, false)
	CRYSTAL_M_ORANGE = 	ShotAddOval(SHOT_16x16, r+14, c_orange,	_width, _height, 270, false)
	CRYSTAL_M_WHITE = 	ShotAddOval(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_width = 4; _height = 3;
	CARD_M_GRAY = 		ShotAddRectangle(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	CARD_M_DKRED = 		ShotAddRectangle(SHOT_16x16, r+1, c_red,	_width, _height, 270, false)
	CARD_M_RED = 		ShotAddRectangle(SHOT_16x16, r+2, c_red,	_width, _height, 270, false)
	CARD_M_PURPLE = 	ShotAddRectangle(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	CARD_M_PINK = 		ShotAddRectangle(SHOT_16x16, r+4, c_fuchsia,_width, _height, 270, false)
	CARD_M_DKBLUE = 	ShotAddRectangle(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	CARD_M_BLUE = 		ShotAddRectangle(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	CARD_M_DKAQUA = 	ShotAddRectangle(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	CARD_M_AQUA = 		ShotAddRectangle(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	CARD_M_DKGREEN = 	ShotAddRectangle(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	CARD_M_GREEN = 		ShotAddRectangle(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	CARD_M_DKYELLOW =	ShotAddRectangle(SHOT_16x16, r+11, c_yellow,_width, _height, 270, false)
	CARD_M_YELLOW = 	ShotAddRectangle(SHOT_16x16, r+12, c_yellow,_width, _height, 270, false)
	CARD_M_BROWN = 		ShotAddRectangle(SHOT_16x16, r+13, c_orange,_width, _height, 270, false)
	CARD_M_ORANGE = 	ShotAddRectangle(SHOT_16x16, r+14, c_orange,_width, _height, 270, false)
	CARD_M_WHITE = 		ShotAddRectangle(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_width = 5; _height = 3;
	MISSILE_M_GRAY = 	ShotAddOval(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	MISSILE_M_DKRED = 	ShotAddOval(SHOT_16x16, r+1, c_red,		_width, _height, 270, false)
	MISSILE_M_RED = 	ShotAddOval(SHOT_16x16, r+2, c_red,		_width, _height, 270, false)
	MISSILE_M_PURPLE = 	ShotAddOval(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	MISSILE_M_PINK = 	ShotAddOval(SHOT_16x16, r+4, c_fuchsia,	_width, _height, 270, false)
	MISSILE_M_DKBLUE = 	ShotAddOval(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	MISSILE_M_BLUE = 	ShotAddOval(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	MISSILE_M_DKAQUA = 	ShotAddOval(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	MISSILE_M_AQUA = 	ShotAddOval(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	MISSILE_M_DKGREEN = ShotAddOval(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	MISSILE_M_GREEN = 	ShotAddOval(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	MISSILE_M_DKYELLOW =ShotAddOval(SHOT_16x16, r+11, c_yellow,	_width, _height, 270, false)
	MISSILE_M_YELLOW = 	ShotAddOval(SHOT_16x16, r+12, c_yellow,	_width, _height, 270, false)
	MISSILE_M_BROWN = 	ShotAddOval(SHOT_16x16, r+13, c_orange,	_width, _height, 270, false)
	MISSILE_M_ORANGE = 	ShotAddOval(SHOT_16x16, r+14, c_orange,	_width, _height, 270, false)
	MISSILE_M_WHITE = 	ShotAddOval(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_width = 5; _height = 3;
	OVAL_BM_GRAY = 		ShotAddOval(SHOT_16x16, r+0, c_ltgray,	_width, _height, 270, false)
	OVAL_BM_DKRED = 	ShotAddOval(SHOT_16x16, r+1, c_red,		_width, _height, 270, false)
	OVAL_BM_RED = 		ShotAddOval(SHOT_16x16, r+2, c_red,		_width, _height, 270, false)
	OVAL_BM_PURPLE = 	ShotAddOval(SHOT_16x16, r+3, c_purple,	_width, _height, 270, false)
	OVAL_BM_PINK = 		ShotAddOval(SHOT_16x16, r+4, c_fuchsia,	_width, _height, 270, false)
	OVAL_BM_DKBLUE = 	ShotAddOval(SHOT_16x16, r+5, c_blue,	_width, _height, 270, false)
	OVAL_BM_BLUE = 		ShotAddOval(SHOT_16x16, r+6, c_blue,	_width, _height, 270, false)
	OVAL_BM_DKAQUA = 	ShotAddOval(SHOT_16x16, r+7, c_aqua,	_width, _height, 270, false)
	OVAL_BM_AQUA = 		ShotAddOval(SHOT_16x16, r+8, c_aqua,	_width, _height, 270, false)
	OVAL_BM_DKGREEN = 	ShotAddOval(SHOT_16x16, r+9, c_green,	_width, _height, 270, false)
	OVAL_BM_GREEN = 	ShotAddOval(SHOT_16x16, r+10, c_lime,	_width, _height, 270, false)
	OVAL_BM_DKYELLOW =	ShotAddOval(SHOT_16x16, r+11, c_yellow,	_width, _height, 270, false)
	OVAL_BM_YELLOW = 	ShotAddOval(SHOT_16x16, r+12, c_yellow,	_width, _height, 270, false)
	OVAL_BM_BROWN = 	ShotAddOval(SHOT_16x16, r+13, c_orange,	_width, _height, 270, false)
	OVAL_BM_ORANGE = 	ShotAddOval(SHOT_16x16, r+14, c_orange,	_width, _height, 270, false)
	OVAL_BM_WHITE = 	ShotAddOval(SHOT_16x16, r+15, c_white,	_width, _height, 270, false)
	r = r + 16
	_radius = 4
	STAR_M_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, 270, false)
	STAR_M_DKRED = 		ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, 270, false)
	STAR_M_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, 270, false)
	STAR_M_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, 270, false)
	STAR_M_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, 270, false)
	STAR_M_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, 270, false)
	STAR_M_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, 270, false)
	STAR_M_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, 270, false)
	STAR_M_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, 270, false)
	STAR_M_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, 270, false)
	STAR_M_GREEN = 		ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, 270, false)
	STAR_M_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, 270, false)
	STAR_M_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, 270, false)
	STAR_M_BROWN = 		ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, 270, false)
	STAR_M_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, 270, false)
	STAR_M_WHITE = 		ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, 270, false)
	r = r + 16
	_radius = 4
	THING_M_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, 270, false)
	THING_M_DKRED = 	ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, 270, false)
	THING_M_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, 270, false)
	THING_M_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, 270, false)
	THING_M_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, 270, false)
	THING_M_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, 270, false)
	THING_M_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, 270, false)
	THING_M_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, 270, false)
	THING_M_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, 270, false)
	THING_M_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, 270, false)
	THING_M_GREEN = 	ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, 270, false)
	THING_M_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, 270, false)
	THING_M_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, 270, false)
	THING_M_BROWN = 	ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, 270, false)
	THING_M_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, 270, false)
	THING_M_WHITE = 	ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, 270, false)
	r = r + 16
	_radius = 6
	DOUBLEOVAL_M_GRAY = 	ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, 270, false)
	DOUBLEOVAL_M_DKRED = 	ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, 270, false)
	DOUBLEOVAL_M_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, 270, false)
	DOUBLEOVAL_M_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, 270, false)
	DOUBLEOVAL_M_PINK = 	ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, 270, false)
	DOUBLEOVAL_M_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, 270, false)
	DOUBLEOVAL_M_BLUE = 	ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, 270, false)
	DOUBLEOVAL_M_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, 270, false)
	DOUBLEOVAL_M_AQUA = 	ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, 270, false)
	DOUBLEOVAL_M_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, 270, false)
	DOUBLEOVAL_M_GREEN = 	ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, 270, false)
	DOUBLEOVAL_M_DKYELLOW = ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, 270, false)
	DOUBLEOVAL_M_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, 270, false)
	DOUBLEOVAL_M_BROWN = 	ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, 270, false)
	DOUBLEOVAL_M_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, 270, false)
	DOUBLEOVAL_M_WHITE = 	ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, 270, false)
	r = r + 16
	_radius = 4
	HEART_M_GRAY = 		ShotAddCircle(SHOT_16x16, r+0, c_ltgray,	_radius, 270, false)
	HEART_M_DKRED = 	ShotAddCircle(SHOT_16x16, r+1, c_red,		_radius, 270, false)
	HEART_M_RED = 		ShotAddCircle(SHOT_16x16, r+2, c_red,		_radius, 270, false)
	HEART_M_PURPLE = 	ShotAddCircle(SHOT_16x16, r+3, c_purple,	_radius, 270, false)
	HEART_M_PINK = 		ShotAddCircle(SHOT_16x16, r+4, c_fuchsia,	_radius, 270, false)
	HEART_M_DKBLUE = 	ShotAddCircle(SHOT_16x16, r+5, c_blue,		_radius, 270, false)
	HEART_M_BLUE = 		ShotAddCircle(SHOT_16x16, r+6, c_blue,		_radius, 270, false)
	HEART_M_DKAQUA = 	ShotAddCircle(SHOT_16x16, r+7, c_aqua,		_radius, 270, false)
	HEART_M_AQUA = 		ShotAddCircle(SHOT_16x16, r+8, c_aqua,		_radius, 270, false)
	HEART_M_DKGREEN = 	ShotAddCircle(SHOT_16x16, r+9, c_green,		_radius, 270, false)
	HEART_M_GREEN = 	ShotAddCircle(SHOT_16x16, r+10, c_lime,		_radius, 270, false)
	HEART_M_DKYELLOW = 	ShotAddCircle(SHOT_16x16, r+11, c_yellow,	_radius, 270, false)
	HEART_M_YELLOW = 	ShotAddCircle(SHOT_16x16, r+12, c_yellow,	_radius, 270, false)
	HEART_M_BROWN = 	ShotAddCircle(SHOT_16x16, r+13, c_orange,	_radius, 270, false)
	HEART_M_ORANGE = 	ShotAddCircle(SHOT_16x16, r+14, c_orange,	_radius, 270, false)
	HEART_M_WHITE = 	ShotAddCircle(SHOT_16x16, r+15, c_white,	_radius, 270, false)
	
	-- 8x8 Bullets
	_radius = 3
	BALL_WS_GRAY = 		ShotAddCircle(SHOT_W_8x8, 0, c_ltgray,	_radius, -1, false)
	BALL_WS_DKRED = 	ShotAddCircle(SHOT_W_8x8, 1, c_red,		_radius, -1, false)
	BALL_WS_RED = 		ShotAddCircle(SHOT_W_8x8, 2, c_red,		_radius, -1, false)
	BALL_WS_PURPLE = 	ShotAddCircle(SHOT_W_8x8, 3, c_purple,	_radius, -1, false)
	BALL_WS_PINK = 		ShotAddCircle(SHOT_W_8x8, 4, c_fuchsia,	_radius, -1, false)
	BALL_WS_DKBLUE = 	ShotAddCircle(SHOT_W_8x8, 5, c_blue,	_radius, -1, false)
	BALL_WS_BLUE = 		ShotAddCircle(SHOT_W_8x8, 6, c_blue,	_radius, -1, false)
	BALL_WS_DKAQUA = 	ShotAddCircle(SHOT_W_8x8, 7, c_aqua,	_radius, -1, false)
	BALL_WS_AQUA = 		ShotAddCircle(SHOT_W_8x8, 8, c_aqua,	_radius, -1, false)
	BALL_WS_DKGREEN = 	ShotAddCircle(SHOT_W_8x8, 9, c_green,	_radius, -1, false)
	BALL_WS_GREEN = 	ShotAddCircle(SHOT_W_8x8, 10, c_lime,	_radius, -1, false)
	BALL_WS_DKYELLOW = 	ShotAddCircle(SHOT_W_8x8, 11, c_yellow,	_radius, -1, false)
	BALL_WS_YELLOW = 	ShotAddCircle(SHOT_W_8x8, 12, c_yellow,	_radius, -1, false)
	BALL_WS_BROWN = 	ShotAddCircle(SHOT_W_8x8, 13, c_orange,	_radius, -1, false)
	BALL_WS_ORANGE = 	ShotAddCircle(SHOT_W_8x8, 14, c_orange,	_radius, -1, false)
	BALL_WS_WHITE = 	ShotAddCircle(SHOT_W_8x8, 15, c_white,	_radius, -1, false)
	
	_radius = 3
	BALL_BS_GRAY = 		ShotAddCircle(SHOT_B_8x8, 0, c_ltgray,	_radius, -1, false)
	BALL_BS_DKRED = 	ShotAddCircle(SHOT_B_8x8, 1, c_red,		_radius, -1, false)
	BALL_BS_RED = 		ShotAddCircle(SHOT_B_8x8, 2, c_red,		_radius, -1, false)
	BALL_BS_PURPLE = 	ShotAddCircle(SHOT_B_8x8, 3, c_purple,	_radius, -1, false)
	BALL_BS_PINK = 		ShotAddCircle(SHOT_B_8x8, 4, c_fuchsia,	_radius, -1, false)
	BALL_BS_DKBLUE = 	ShotAddCircle(SHOT_B_8x8, 5, c_blue,	_radius, -1, false)
	BALL_BS_BLUE = 		ShotAddCircle(SHOT_B_8x8, 6, c_blue,	_radius, -1, false)
	BALL_BS_DKAQUA = 	ShotAddCircle(SHOT_B_8x8, 7, c_aqua,	_radius, -1, false)
	BALL_BS_AQUA = 		ShotAddCircle(SHOT_B_8x8, 8, c_aqua,	_radius, -1, false)
	BALL_BS_DKGREEN = 	ShotAddCircle(SHOT_B_8x8, 9, c_green,	_radius, -1, false)
	BALL_BS_GREEN = 	ShotAddCircle(SHOT_B_8x8, 10, c_lime,	_radius, -1, false)
	BALL_BS_DKYELLOW = 	ShotAddCircle(SHOT_B_8x8, 11, c_yellow,	_radius, -1, false)
	BALL_BS_YELLOW = 	ShotAddCircle(SHOT_B_8x8, 12, c_yellow,	_radius, -1, false)
	BALL_BS_BROWN = 	ShotAddCircle(SHOT_B_8x8, 13, c_orange,	_radius, -1, false)
	BALL_BS_ORANGE = 	ShotAddCircle(SHOT_B_8x8, 14, c_orange,	_radius, -1, false)
	BALL_BS_WHITE = 	ShotAddCircle(SHOT_B_8x8, 15, c_white,	_radius, -1, false)
	
	_radius = 3
	BALL_S_GRAY = 		ShotAddCircle(SHOT_S_8x8, 0, c_ltgray,	_radius, -1, false)
	BALL_S_DKRED = 		ShotAddCircle(SHOT_S_8x8, 1, c_red,		_radius, -1, false)
	BALL_S_RED = 		ShotAddCircle(SHOT_S_8x8, 2, c_red,		_radius, -1, false)
	BALL_S_PURPLE = 	ShotAddCircle(SHOT_S_8x8, 3, c_purple,	_radius, -1, false)
	BALL_S_PINK = 		ShotAddCircle(SHOT_S_8x8, 4, c_fuchsia,	_radius, -1, false)
	BALL_S_DKBLUE = 	ShotAddCircle(SHOT_S_8x8, 5, c_blue,	_radius, -1, false)
	BALL_S_BLUE = 		ShotAddCircle(SHOT_S_8x8, 6, c_blue,	_radius, -1, false)
	BALL_S_DKAQUA = 	ShotAddCircle(SHOT_S_8x8, 7, c_aqua,	_radius, -1, false)
	BALL_S_AQUA = 		ShotAddCircle(SHOT_S_8x8, 8, c_aqua,	_radius, -1, false)
	BALL_S_DKGREEN = 	ShotAddCircle(SHOT_S_8x8, 9, c_green,	_radius, -1, false)
	BALL_S_GREEN = 		ShotAddCircle(SHOT_S_8x8, 10, c_lime,	_radius, -1, false)
	BALL_S_DKYELLOW = 	ShotAddCircle(SHOT_S_8x8, 11, c_yellow,	_radius, -1, false)
	BALL_S_YELLOW = 	ShotAddCircle(SHOT_S_8x8, 12, c_yellow,	_radius, -1, false)
	BALL_S_BROWN = 		ShotAddCircle(SHOT_S_8x8, 13, c_orange,	_radius, -1, false)
	BALL_S_ORANGE = 	ShotAddCircle(SHOT_S_8x8, 14, c_orange,	_radius, -1, false)
	BALL_S_WHITE = 		ShotAddCircle(SHOT_S_8x8, 15, c_white,	_radius, -1, false)
	
	
	-- Curving Lasers
	CLASER_GRAY = 		ShotAddCLaser(CLASER_256x16, 0, c_ltgray)
	CLASER_DKRED = 		ShotAddCLaser(CLASER_256x16, 1, c_red)
	CLASER_RED = 		ShotAddCLaser(CLASER_256x16, 2, c_red)
	CLASER_PURPLE = 	ShotAddCLaser(CLASER_256x16, 3, c_purple)
	CLASER_PINK = 		ShotAddCLaser(CLASER_256x16, 4, c_fuchsia)
	CLASER_DKBLUE = 	ShotAddCLaser(CLASER_256x16, 5, c_blue)
	CLASER_BLUE = 		ShotAddCLaser(CLASER_256x16, 6, c_blue)
	CLASER_DKAQUA = 	ShotAddCLaser(CLASER_256x16, 7, c_aqua)
	CLASER_AQUA = 		ShotAddCLaser(CLASER_256x16, 8, c_aqua)
	CLASER_DKGREEN = 	ShotAddCLaser(CLASER_256x16, 9, c_green)
	CLASER_GREEN = 		ShotAddCLaser(CLASER_256x16, 10, c_lime)
	CLASER_DKYELLOW = 	ShotAddCLaser(CLASER_256x16, 11, c_yellow)
	CLASER_YELLOW = 	ShotAddCLaser(CLASER_256x16, 12, c_yellow)
	CLASER_BROWN = 		ShotAddCLaser(CLASER_256x16, 13, c_orange)
	CLASER_ORANGE = 	ShotAddCLaser(CLASER_256x16, 14, c_orange)
	CLASER_WHITE = 		ShotAddCLaser(CLASER_256x16, 15, c_white)
	
	-- Delay Lasers
	DELAYLASER_GRAY = 		ShotAddDelayLaser(SHOT_16x16, 0)
	DELAYLASER_DKRED = 		ShotAddDelayLaser(SHOT_16x16, 1)
	DELAYLASER_RED = 		ShotAddDelayLaser(SHOT_16x16, 2)
	DELAYLASER_PURPLE = 	ShotAddDelayLaser(SHOT_16x16, 3)
	DELAYLASER_PINK = 		ShotAddDelayLaser(SHOT_16x16, 4)
	DELAYLASER_DKBLUE = 	ShotAddDelayLaser(SHOT_16x16, 5)
	DELAYLASER_BLUE = 		ShotAddDelayLaser(SHOT_16x16, 6)
	DELAYLASER_DKAQUA = 	ShotAddDelayLaser(SHOT_16x16, 7)
	DELAYLASER_AQUA = 		ShotAddDelayLaser(SHOT_16x16, 8)
	DELAYLASER_DKGREEN = 	ShotAddDelayLaser(SHOT_16x16, 9)
	DELAYLASER_GREEN = 		ShotAddDelayLaser(SHOT_16x16, 10)
	DELAYLASER_DKYELLOW = 	ShotAddDelayLaser(SHOT_16x16, 11)
	DELAYLASER_YELLOW = 	ShotAddDelayLaser(SHOT_16x16, 12)
	DELAYLASER_BROWN = 		ShotAddDelayLaser(SHOT_16x16, 13)
	DELAYLASER_ORANGE = 	ShotAddDelayLaser(SHOT_16x16, 14)
	DELAYLASER_WHITE = 		ShotAddDelayLaser(SHOT_16x16, 15)
	
end

return S