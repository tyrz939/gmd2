
local S = {}
local thread

function S.init()

end
function S.bossNonspell1()
	thread = task.add(function()
		local inst, inst1, inst2, inst3, dir
		while true do
			-- Static Laser
			--dir = point_direction(GetX(boss), GetY(boss), PlayerX(), PlayerY())
			--ShotCreateSLaser(SLASER_DKRED, GetX(boss), GetY(boss), dir, 600, 20, 120, false, 60)
			--wait(30)
			
			-- Circle of Bullets with deceleration
			inst1 = ShotCreateCircleA(RICE_M_YELLOW, GetX(boss), GetY(boss), 5, random(360), 40, 15, -0.1, 2, 0, false, 30)
			ShotGroupDeletePattern(inst1, 2, 2)
			inst1 = ShotCreateCircleA(RICE_M_RED, GetX(boss), GetY(boss), 5, random(360), 40, 15, -0.1, 2, 0, false, 30)
			ShotGroupDeletePattern(inst1, 2, 2)
			inst1 = ShotCreateCircleA(RICE_M_ORANGE, GetX(boss), GetY(boss), 5, random(360), 40, 15, -0.1, 2, 0, false, 30)
			ShotGroupDeletePattern(inst1, 2, 2)
			--inst1.directionS = 180
			wait(30)
			
			-- Circle of Bullets with acceleration
			inst2 = ShotCreateCircleA(BALL_M_YELLOW, GetX(boss), GetY(boss), 0.5, random(360), 25, 15, 0.1, 2)
			ShotSetBlend(inst2, bm_add)
			wait(30)
			
			-- Arc of Bullets with deceleration
			dir = point_direction(GetX(boss), GetY(boss), PlayerX(), PlayerY())
			inst3 = ShotCreateArcA(BALL_MO_RED, GetX(boss), GetY(boss), 5, dir, 10, 180, 15, -0.1, 2)
			ShotSetBlend(inst3, bm_add)
			wait(30)
			
			-- Single Bullet
			dir = point_direction(GetX(boss), GetY(boss), PlayerX(), PlayerY())
			--inst = ShotCreate(RICE_M_PINK, GetX(boss), GetY(boss), 6, dir)
			inst = ShotCreateCircle(BALL_MO_ORANGE, GetX(boss), GetY(boss), 7, dir, 12, 15)
			dl = ShotCreateDelayLaser(DELAYLASER_ORANGE, 600, 3, inst, 30)
			for i = 30, 90, 3 do
				inst = ShotCreateCircle(BALL_MO_ORANGE, GetX(boss), GetY(boss), 6 + random(3), dir + random(10) - 5, 12, 15)
				DelayLaserAdd(dl, inst, i)
			end
			wait(30)
			
			-- Shot Group Manipulation
			ShotGroupSetDirection(inst2, -1, 270)
			ShotGroupSetDirectionPoint(inst3, PlayerX(), PlayerY())
			
			-- Move Boss
			MoveTween(boss, irandom(384), irandom(250), 60)
			wait(60)
			
			-- Shot Deleter Examples
			--DeleteShotCircle(irandom(384), irandom(448), 50, 120)
			--DeleteShotRectangle(irandom(384), irandom(448), 50, 75, 60)
		end
	end)
	return thread
end

function S.bossSpellcard1()
	thread = task.add(function()
		local inst, dir
		while true do
			-- Static Laser
			for i = 1, 10 do
				PickupCreate(random(384), random(448), 0)
				PickupCreate(random(384), random(448), 6)
			end
			-- Curving Laser
			wait(30)
			inst0 = ShotCreateCLaserCircle(CLASER_RED, GetX(boss), GetY(boss), 8, random(360), 20, 30, 16, 15)
			ShotSetBlend(inst0, bm_add)
			ShotCreateDelayLaser(DELAYLASER_RED, 600, 3, inst0, 30)
			inst0 = ShotCreateCLaserCircle(CLASER_YELLOW, GetX(boss), GetY(boss), 8, random(360), 20, 30, 16, 15)
			ShotSetBlend(inst0, bm_add)
			ShotCreateDelayLaser(DELAYLASER_YELLOW, 600, 3, inst0, 30)
			--inst0.directionP = 0.5
			wait(30)
			-- Move Boss
			MoveTween(boss, irandom(384), irandom(250), 60)
			wait(60)
		end
	end)
	return thread
end

return S