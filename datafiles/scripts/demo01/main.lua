Dir = game.luaScriptDir -- Lua readable script directory
gmDir = game.gmScriptDir -- GameMaker readable script directory

task = require('lua.task') -- Task Scheduler
local shotSheet = require(Dir .. 'ShotSheet.shotsheet')
local backgroundScript = require(Dir .. '3dBackground.background')
local stage = require(Dir .. 'stage')

local thread
task.init()
mainCo = task.add(function()
	wait(120)
	boss = BossCreate(400, -100, spr_flandre)
	EnemyShotCollisionRectangle(boss, 40, 50)
	EnemyPlayerCollisionRectangle(boss, 20, 25)
	
	MoveSetTween('EaseOutQuad')
	MoveTween(boss, 192, 150, 120)
	boss.distortion = true
	boss.animationSpeed = 12
	boss.trails = true
	boss.trailsCol = make_color_rgb(255, 127, 127)
	
	wait(180)
	
	-- Nonspell
	phaseID = NonspellSetup(boss, 20, 1000, 0.3)
	thread = stage.bossNonspell1()
	while phaseID == boss.bossPhaseID do wait(1) end
	task.remove(thread)
	
	wait(60)
	
	-- Spellcard
	phaseID = SpellcardSetup(boss, 20, 2000, false, 3000)
	thread = stage.bossSpellcard1()
	while phaseID == boss.bossPhaseID do wait(1) end
	task.remove(thread)
end)

-- Loading Assets
function importAssets()
	local group = 'mainTexPage'
	-- Shot Sheet
	shotSheet.loadAssets(group)
	-- 3D
	backgroundScript.loadAssets3d(group)
	-- Interface
	image_stream_add(group, 'interface', gmDir .. 'interface.png', 1, 1, 1)
	-- Boss
	image_stream_add(group, 'flandre', gmDir .. 'flandre.png', 4, 32, 32)
	
	wav_add(gmDir .. 'test.wav', 'test')
end

-- Runs after loading
function initialize()
	-- Assigning Assets to Variables
	local group = 'mainTexPage'
	-- Shot Sheet
	shotSheet.setAssets(group)
	-- 3D
	backgroundScript.setAssets3d(group)
	-- 3D Variables
	backgroundScript.init()
	-- Background Interface
	GUISetBackground(image_group_find_image(group, 'interface'))
	-- Boss Sprite
	spr_flandre = image_group_find_image(group, 'flandre')
	
	game.snd = wav_find('test')
	
	
	-- Play area setup. StartX, StartY, Width, Hight
	PlayFieldSetup(32, 16, 384, 448)
	-- Boundary for player movement. Left, Right, Top, Bottom
	SetPlayerBoundary(10, 10, 20, 15)
	-- Player Spawn Location. X, Y
	SetPlayerSpawn(192, 448+24, 192, 448-32)

	-- Starting Values
	SetLives(2)
	SetBombs(3)
	SetPower(4)
	SetContinues(3)
	
	-- GUI Setup
	HPBarType(HP_BAR)
	GUISetAnchor(832, 0)
	GUISetType('wide')
	GUIAddDifficulty(224, 32)
	GUIAddElement('score', 32, 150, c_white)
	GUIAddElement('player', 32, 200, make_colour_rgb(255,127,191))
	GUIAddElement('spellcard', 32, 250, make_colour_rgb(159,255,159))
	GUIAddElement('power', 32, 300, make_colour_rgb(255,159,0))
	GUIAddElement('graze', 32, 350, c_ltgray)
end

-- Run every game step
function step()
	task.update(1)
	
	-- 3D Background
	backgroundScript.step()
end

--[[function draw3d()
	backgroundScript.draw3d()
end]]