Dir = game.luaScriptDir -- Lua readable script directory
gmDir = game.gmScriptDir -- GameMaker readable script directory

local shotSheet = require(Dir .. 'ShotSheet.shotsheet')
	
mainCo = coroutine.create(function()
	while true do
		wait(120)
		coroutine.yield()
	end
end)

-- Loading Assets
function importAssets()
	local group = 'mainTexPage'
	-- Shot Sheet
	shotSheet.loadAssets(group)
end

function initialize()
	-- Assigning Assets to Variables
	local group = 'mainTexPage'
	-- Shot Sheet
	shotSheet.setAssets(group)
	
	-- Play area setup. StartX, StartY, Width, Hight
	PlayFieldSetup(128, 16, 384, 448)
	-- Boundary for player movement. Left, Right, Top, Bottom
	SetPlayerBoundary(10, 10, 20, 15)
	-- Player Spawn Location. X, Y
	SetPlayerSpawn(192, 448+24, 192, 448-32)

	-- Starting Values
	game.lives = 2
	game.bombs = 3
	game.shotPower = 4
	game.continues = 3
	
	-- GUI Setup
	GUISetAnchor(1152, 0)
	GUISetType('narrow')
	GUIAddDifficulty(0, 32)
	GUIAddElement('score', 0, 128, c_white)
	GUIAddElement('player', 0, 224, make_colour_rgb(255,127,191))
	GUIAddElement('spellcard', 0, 320, make_colour_rgb(159,255,159))
	GUIAddElement('power', 0, 416, make_colour_rgb(255,159,0))
	GUIAddElement('graze', 0, 512, c_ltgray)
end

function step()
	
	assert(coroutine.resume(mainCo))
end

--[[function draw3d()
	backgroundScript.draw3d()
end]]