-- Taken from https://github.com/bjornbytes/quilt
-- Renamed quilt to task
-- Made it assert when resuming coroutine (to show errors)
local task = {}

function task.init()
  task.threads = {}
  task.delays = {}
end

function task.add(thread, ...)
  if not thread then return end
  task.threads[thread] = type(thread) == 'thread' and thread or coroutine.create(thread)
  task.delays[thread] = 0
  return thread, task.add(...)
end

function task.remove(thread, ...)
  if not thread then return end
  task.threads[thread] = nil
  task.delays[thread] = nil
  return task.remove(...)
end

function task.resume(thread, ...)
  if not thread then return end
  task.delays[thread] = 0
  return task.resume(...)
end

function task.update(dt)
  for thread, cr in pairs(task.threads) do
    if task.delays[thread] <= dt then
      local _, delay = assert(coroutine.resume(cr))
      if coroutine.status(cr) == 'dead' then
        task.remove(thread)
        if type(delay) == 'function' or type(delay) == 'thread' then
          task.add(delay)
        end
      else
        task.delays[thread] = type(delay) == 'number' and delay or 0
      end
    else
      task.delays[thread] = task.delays[thread] - dt
    end
  end
end

return task