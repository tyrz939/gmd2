{
    "id": "79abd8f7-a08e-4fc4-a829-04e26045ae18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaShot1Die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94f701cc-bd85-4028-9fcf-bf8980846c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79abd8f7-a08e-4fc4-a829-04e26045ae18",
            "compositeImage": {
                "id": "9c4dbfa5-c2cf-4a77-8dc4-38912d906b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f701cc-bd85-4028-9fcf-bf8980846c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe4d180-edc1-4c21-8a4f-48a63028a5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f701cc-bd85-4028-9fcf-bf8980846c98",
                    "LayerId": "e66a09b9-b03a-450e-b853-6ae897a52dcb"
                }
            ]
        },
        {
            "id": "578b8abd-4090-46b3-9f8e-4574c62651f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79abd8f7-a08e-4fc4-a829-04e26045ae18",
            "compositeImage": {
                "id": "f23e70d2-a93f-406c-9136-d28380add973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "578b8abd-4090-46b3-9f8e-4574c62651f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c439bf5f-8021-40d1-aeeb-db00db6a6a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "578b8abd-4090-46b3-9f8e-4574c62651f4",
                    "LayerId": "e66a09b9-b03a-450e-b853-6ae897a52dcb"
                }
            ]
        },
        {
            "id": "6d38df88-b5fb-4c32-aae6-ccdc0c5d6e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79abd8f7-a08e-4fc4-a829-04e26045ae18",
            "compositeImage": {
                "id": "b210d839-5cba-4985-bab5-7909ca2dd2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d38df88-b5fb-4c32-aae6-ccdc0c5d6e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231da51b-7b65-4896-be7e-7147d638a9ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d38df88-b5fb-4c32-aae6-ccdc0c5d6e62",
                    "LayerId": "e66a09b9-b03a-450e-b853-6ae897a52dcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e66a09b9-b03a-450e-b853-6ae897a52dcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79abd8f7-a08e-4fc4-a829-04e26045ae18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}