{
    "id": "b07eb5e8-82d5-4d0f-81c4-7c3a346fc283",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaLaser1",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01cdd364-2e7a-4b9e-83a4-1898567c8289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b07eb5e8-82d5-4d0f-81c4-7c3a346fc283",
            "compositeImage": {
                "id": "c4b67528-e61d-4364-84e3-f9e4d10a4baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01cdd364-2e7a-4b9e-83a4-1898567c8289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17ba8e0-5bdf-46a9-8fe7-117a3babaa21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cdd364-2e7a-4b9e-83a4-1898567c8289",
                    "LayerId": "4589e1d5-7872-486a-8d7c-70c873d70a04"
                },
                {
                    "id": "b11db97e-8617-4806-b236-c660d8d537b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cdd364-2e7a-4b9e-83a4-1898567c8289",
                    "LayerId": "44fe6efd-c94f-4552-a9f6-43487070c0f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "44fe6efd-c94f-4552-a9f6-43487070c0f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b07eb5e8-82d5-4d0f-81c4-7c3a346fc283",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4589e1d5-7872-486a-8d7c-70c873d70a04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b07eb5e8-82d5-4d0f-81c4-7c3a346fc283",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 256
}