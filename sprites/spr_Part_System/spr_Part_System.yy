{
    "id": "5d846907-e1ff-4936-91da-3cd461a7716e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Part_System",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "888e11a0-16d1-4bd1-868b-aeef77c44fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d846907-e1ff-4936-91da-3cd461a7716e",
            "compositeImage": {
                "id": "6d2c1bad-8101-4efc-bd81-76b58edb25d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888e11a0-16d1-4bd1-868b-aeef77c44fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0f926e-eec5-460c-b7cc-854da9bab546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888e11a0-16d1-4bd1-868b-aeef77c44fcf",
                    "LayerId": "ab8f0699-4ead-41e7-9d63-05ca6051ee56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ab8f0699-4ead-41e7-9d63-05ca6051ee56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d846907-e1ff-4936-91da-3cd461a7716e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}