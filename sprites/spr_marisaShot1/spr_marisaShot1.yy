{
    "id": "0ebbf20a-f436-4d9d-9cbe-52da59175baf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaShot1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08bb6c72-4a82-480a-841c-9a0ef50542da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ebbf20a-f436-4d9d-9cbe-52da59175baf",
            "compositeImage": {
                "id": "08b42a69-8e7a-4184-ae3e-d382573dacbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08bb6c72-4a82-480a-841c-9a0ef50542da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593bb89e-c799-41c0-a8f2-8a8ff5f2a025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08bb6c72-4a82-480a-841c-9a0ef50542da",
                    "LayerId": "6c15cb9d-711b-48dd-8301-97817f9987df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6c15cb9d-711b-48dd-8301-97817f9987df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ebbf20a-f436-4d9d-9cbe-52da59175baf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}