{
    "id": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaShot2Die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5b8e4d7-fefa-4d1b-b344-ab49caacd4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "2d812065-af84-43b1-a82b-c7af3cd4c5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b8e4d7-fefa-4d1b-b344-ab49caacd4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebd7bb58-9361-48d0-966f-c4df102024cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b8e4d7-fefa-4d1b-b344-ab49caacd4b4",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "caa65262-0f98-4266-9ad3-c769f1c7a03e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "1765c2ee-01df-4592-99ca-ec453762d236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa65262-0f98-4266-9ad3-c769f1c7a03e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c9d41c-8256-4769-908c-669edf98c0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa65262-0f98-4266-9ad3-c769f1c7a03e",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "59ae3217-f650-4c51-b69e-dfff30c3b8b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "85329095-9638-467f-9e37-082bc977b255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ae3217-f650-4c51-b69e-dfff30c3b8b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa77c16-f98b-41de-96b6-6c9b364e8884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ae3217-f650-4c51-b69e-dfff30c3b8b8",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "2a71f5ba-d2d6-4fd2-94d5-0f8e723b8577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "69cf1540-efc3-4a81-a5b4-cf421671b67d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a71f5ba-d2d6-4fd2-94d5-0f8e723b8577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e9328b4-aafa-41a5-a752-a0a8d5a48f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a71f5ba-d2d6-4fd2-94d5-0f8e723b8577",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "d0da28e7-2589-4f00-a23f-edbdbee05c59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "030db555-27b7-4f8a-92dc-0efcbd9e4038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0da28e7-2589-4f00-a23f-edbdbee05c59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e0f1e4b-f305-464c-adf6-f309d9df95be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0da28e7-2589-4f00-a23f-edbdbee05c59",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "219dce14-0a90-4c23-a7b8-25d821d67ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "bd02ee52-50f0-4c0a-b2cf-7f17e76bca93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "219dce14-0a90-4c23-a7b8-25d821d67ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9fdc9ad-5ea2-417b-9798-814b706eda5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "219dce14-0a90-4c23-a7b8-25d821d67ff0",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "0908f3b6-fb31-407c-8dcc-8c89e99c6194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "ddbae490-d43a-4e2e-b86a-42d93d795950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0908f3b6-fb31-407c-8dcc-8c89e99c6194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029335b8-8ab1-47f6-9d46-f6443a8cae3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0908f3b6-fb31-407c-8dcc-8c89e99c6194",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        },
        {
            "id": "ac8ccfae-7aec-47aa-b834-832af2f30323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "compositeImage": {
                "id": "35a467ca-e5f2-4403-97f0-c62dbb6e72d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8ccfae-7aec-47aa-b834-832af2f30323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005b530f-c567-4c62-aa6d-d71a3c6b4015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8ccfae-7aec-47aa-b834-832af2f30323",
                    "LayerId": "81eae286-e863-402c-9ee0-2c2130c5c458"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81eae286-e863-402c-9ee0-2c2130c5c458",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e615a64-0ccd-403c-9e49-45f7d92cc8a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}