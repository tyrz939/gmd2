{
    "id": "1d182194-c3ea-4d9b-ac8a-ec6d9563db2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUIHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f705bbb8-07be-438a-a489-4b59ef873f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d182194-c3ea-4d9b-ac8a-ec6d9563db2a",
            "compositeImage": {
                "id": "d0d23b9b-ef78-43bf-9c4c-6a8bfa58cdf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f705bbb8-07be-438a-a489-4b59ef873f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45330289-e72e-4493-b4ce-770649f53f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f705bbb8-07be-438a-a489-4b59ef873f55",
                    "LayerId": "bd058e65-c90f-4f1a-af61-56447acc0918"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "bd058e65-c90f-4f1a-af61-56447acc0918",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d182194-c3ea-4d9b-ac8a-ec6d9563db2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}