{
    "id": "e4b3db27-7a19-4ff7-9208-96fda64ab733",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaLaserImpact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15032fda-8c72-4428-a951-23192844b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b3db27-7a19-4ff7-9208-96fda64ab733",
            "compositeImage": {
                "id": "7a99d1a6-b084-448f-a4be-003ca9ef93c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15032fda-8c72-4428-a951-23192844b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18282de7-0660-4fe7-84aa-e6fd71dbcad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15032fda-8c72-4428-a951-23192844b254",
                    "LayerId": "822a1afa-7b51-461f-a0d5-eab8b6ae4cae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "822a1afa-7b51-461f-a0d5-eab8b6ae4cae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4b3db27-7a19-4ff7-9208-96fda64ab733",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 4
}