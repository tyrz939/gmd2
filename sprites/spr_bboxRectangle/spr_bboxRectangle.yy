{
    "id": "cad726cb-4f88-40f6-8d21-c59441ff6a8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bboxRectangle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebc2f2fa-54f3-49f5-8d66-5946b3b824a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cad726cb-4f88-40f6-8d21-c59441ff6a8a",
            "compositeImage": {
                "id": "eef807a7-d55b-43bf-bd7f-254da8e59103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc2f2fa-54f3-49f5-8d66-5946b3b824a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196745a0-25de-46d2-b3a4-a6e7140bbdf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc2f2fa-54f3-49f5-8d66-5946b3b824a7",
                    "LayerId": "5120f03f-e1fd-471d-bba9-928bfc196d4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5120f03f-e1fd-471d-bba9-928bfc196d4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cad726cb-4f88-40f6-8d21-c59441ff6a8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}