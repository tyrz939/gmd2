{
    "id": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickupArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2945c09-f1eb-4011-b7d9-447a5f9bff9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "33d1f737-6fc4-4ad9-9335-f97c55015e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2945c09-f1eb-4011-b7d9-447a5f9bff9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce61c692-30da-44a2-8bad-31f1b2820650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2945c09-f1eb-4011-b7d9-447a5f9bff9f",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "847cabd9-5238-4af5-8f22-2712d0ee6dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "dabde801-b892-4f7b-9b59-2036294129ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847cabd9-5238-4af5-8f22-2712d0ee6dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce9cf2e-a822-479a-9fed-87d52e757950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847cabd9-5238-4af5-8f22-2712d0ee6dad",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "69babc91-fa51-4382-8907-65f628f1ce6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "e885730d-5bf7-4010-ac68-ebb683d3c40d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69babc91-fa51-4382-8907-65f628f1ce6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98f44a6-d7e1-4247-9ecb-bb0bdbbeafe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69babc91-fa51-4382-8907-65f628f1ce6f",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "78d87cab-2696-4d4d-9982-fe02860f7e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "dbd7f3bb-b53d-4e11-bbcf-5308ce82e975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d87cab-2696-4d4d-9982-fe02860f7e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f2a0f50-d309-4619-b59c-149392e533d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d87cab-2696-4d4d-9982-fe02860f7e27",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "c1058436-2295-4e86-be17-2965488767af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "3e0c8352-93ef-4958-8b9c-6ce875456450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1058436-2295-4e86-be17-2965488767af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c995bf60-9ac6-4d61-aac0-26c88b94989c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1058436-2295-4e86-be17-2965488767af",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "551c5c23-3aef-4626-8a32-5755093ce85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "bb1e8c98-916b-43b5-8f3f-571b3c564be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551c5c23-3aef-4626-8a32-5755093ce85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7afc2047-6847-417e-9cdb-cac2422c56f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551c5c23-3aef-4626-8a32-5755093ce85b",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        },
        {
            "id": "83882783-5c99-4258-a160-e248ca4d38e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "compositeImage": {
                "id": "492fc8bc-e28e-47b9-bc2b-09fe49d8ad77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83882783-5c99-4258-a160-e248ca4d38e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b916a5bf-8230-43eb-b912-8a13384b8311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83882783-5c99-4258-a160-e248ca4d38e4",
                    "LayerId": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "8ddac63d-025a-4d70-9c0f-bd99b59d6f97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c38a01-fd39-441b-9f6f-19b2fac03e37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 0
}