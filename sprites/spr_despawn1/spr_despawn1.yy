{
    "id": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_despawn1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e73c083-07fb-417e-8491-6ee708650b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "5db06822-049e-4432-9aad-72c9cc35fae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e73c083-07fb-417e-8491-6ee708650b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4483e9b-ddfa-498e-a47f-20da22331d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e73c083-07fb-417e-8491-6ee708650b12",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "472846a6-1aec-431c-873a-c0f8dff2d113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "1767908e-eb83-4ab5-9cd2-3b9c4605484d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472846a6-1aec-431c-873a-c0f8dff2d113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93fbb2be-ac38-44eb-9366-4796e5c37553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472846a6-1aec-431c-873a-c0f8dff2d113",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "68cc1adb-85c0-4c09-a715-bf75f4d61af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "d4d11190-31d8-48ba-b6ab-9038d24809cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68cc1adb-85c0-4c09-a715-bf75f4d61af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44660fd2-73b8-4598-adcd-394e8b5b2f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cc1adb-85c0-4c09-a715-bf75f4d61af7",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "e2b6bbb5-3712-43e7-9f33-aba7c24cc93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "f09312b4-abd5-4efc-9b1c-3f65c70506ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b6bbb5-3712-43e7-9f33-aba7c24cc93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2a7598d-a8d7-4c57-a991-cac5790692fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b6bbb5-3712-43e7-9f33-aba7c24cc93f",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "68647f8c-4362-418c-b429-6bf3baaa2f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "202a1e79-5437-4bca-8e81-909dc928ca33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68647f8c-4362-418c-b429-6bf3baaa2f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca77f2f-c17c-48b8-8418-87b1405e8cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68647f8c-4362-418c-b429-6bf3baaa2f24",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "51a99a43-072d-46b8-a00c-38c55913fc23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "a66f54ba-28ac-4a86-bf96-e22a3664972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a99a43-072d-46b8-a00c-38c55913fc23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac7072eb-1ae4-40b8-9467-b4ab52a42c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a99a43-072d-46b8-a00c-38c55913fc23",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "933473a8-8e6a-4f6c-9a2b-59acf3d9dfdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "306b421e-9a3c-4df2-b4ea-4f5da2633eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933473a8-8e6a-4f6c-9a2b-59acf3d9dfdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c5fdef-4c32-4c0c-8ab2-e0c8662d0b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933473a8-8e6a-4f6c-9a2b-59acf3d9dfdc",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        },
        {
            "id": "5f39fe45-01cf-42bd-9500-55df3fbddbe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "compositeImage": {
                "id": "ed52e6c2-a556-46bf-8e1a-c17b65c6e30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f39fe45-01cf-42bd-9500-55df3fbddbe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af15535-eae6-4540-ac39-c8f6538091ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f39fe45-01cf-42bd-9500-55df3fbddbe5",
                    "LayerId": "25f39899-0b76-4646-bfb5-cdb1ba1791eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "25f39899-0b76-4646-bfb5-cdb1ba1791eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb96db14-1f82-4bd3-8e35-de2b9b7c5f49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}