{
    "id": "18a15acc-7c8f-4c82-bf4e-ca1368ed2b48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialogueCorner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7f7fc7e-0b51-4b1f-a6a0-f59882217fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18a15acc-7c8f-4c82-bf4e-ca1368ed2b48",
            "compositeImage": {
                "id": "8531ab52-9e1d-44d6-85ee-eb2ab7bb1894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f7fc7e-0b51-4b1f-a6a0-f59882217fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ad6100-19e4-4009-859b-89bece967f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f7fc7e-0b51-4b1f-a6a0-f59882217fe3",
                    "LayerId": "ef6a3e2d-eeac-4255-b133-d9fc45da5f61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef6a3e2d-eeac-4255-b133-d9fc45da5f61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18a15acc-7c8f-4c82-bf4e-ca1368ed2b48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}