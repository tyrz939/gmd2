{
    "id": "998cc99d-d6f6-449d-b691-1f39a213ab73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_distortion_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 2,
    "bbox_right": 254,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e24a50a-eef8-40a6-b5b8-7957560319bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "998cc99d-d6f6-449d-b691-1f39a213ab73",
            "compositeImage": {
                "id": "7633c75c-cfd0-4557-b98b-8d6ad02989b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e24a50a-eef8-40a6-b5b8-7957560319bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d3903e6-d9ea-4244-944b-81e3ecb322b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e24a50a-eef8-40a6-b5b8-7957560319bd",
                    "LayerId": "6049a64c-70c2-4afb-8245-b948c7fddfd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6049a64c-70c2-4afb-8245-b948c7fddfd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "998cc99d-d6f6-449d-b691-1f39a213ab73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}