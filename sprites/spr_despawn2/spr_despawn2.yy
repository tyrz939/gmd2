{
    "id": "72dc6137-0965-438b-914d-da2d1f291024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_despawn2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44df2470-2a1c-4b4d-9327-40bce991c53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "89b4b4fc-367a-47d6-85a1-df63c53c01a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44df2470-2a1c-4b4d-9327-40bce991c53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d8e198-0b53-4361-9294-e9ac8524a47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44df2470-2a1c-4b4d-9327-40bce991c53d",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "f5d0caed-bfa1-4edf-867a-eed6b81aeae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "802b28b8-efa6-48e2-9c1b-e2ed152b2046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d0caed-bfa1-4edf-867a-eed6b81aeae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ccbbe83-d216-4862-a623-2b6723a00c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d0caed-bfa1-4edf-867a-eed6b81aeae3",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "d861db14-bff4-4f82-9771-13910292d265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "4171a507-2e56-4f33-abd8-99ad3b4a7c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d861db14-bff4-4f82-9771-13910292d265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c142445-98ae-4128-acbe-1755a2df5bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d861db14-bff4-4f82-9771-13910292d265",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "43f58458-a90c-4f3c-9b60-3aaceef2f977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "e46d47ee-941b-4cf1-9219-5a4fa5d82f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f58458-a90c-4f3c-9b60-3aaceef2f977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86033c9e-0833-4ead-a555-cab1fdafe363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f58458-a90c-4f3c-9b60-3aaceef2f977",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "b6263bf1-39f2-4d08-925e-f9bcba74a56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "63f531a0-e77b-4f29-a6c7-4914ffae5715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6263bf1-39f2-4d08-925e-f9bcba74a56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95169174-8a89-44b9-9b6c-589408d51c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6263bf1-39f2-4d08-925e-f9bcba74a56e",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "b714438e-143e-4151-b5ff-3d712fb62136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "e921305c-c355-4871-81ec-0f39a51951ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b714438e-143e-4151-b5ff-3d712fb62136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead7997d-16a6-44f0-9431-a0cc61c7b4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b714438e-143e-4151-b5ff-3d712fb62136",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "534b5dbd-0b72-4330-a7c9-8ddb43b2f564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "67c8097b-a9de-44b7-ad91-7bade792912b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534b5dbd-0b72-4330-a7c9-8ddb43b2f564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e319c737-b644-44b9-9555-1775bfb4576b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534b5dbd-0b72-4330-a7c9-8ddb43b2f564",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        },
        {
            "id": "a44a98c4-173b-4aa3-bbd1-fe73dff138fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "compositeImage": {
                "id": "955428ef-ec36-4b25-a42e-4ad8cc573bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44a98c4-173b-4aa3-bbd1-fe73dff138fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3fd238e-e1d6-4dbb-b6a8-693277773197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44a98c4-173b-4aa3-bbd1-fe73dff138fc",
                    "LayerId": "52b6a465-e552-4751-902a-d74a4e66fa99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "52b6a465-e552-4751-902a-d74a4e66fa99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72dc6137-0965-438b-914d-da2d1f291024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}