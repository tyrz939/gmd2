{
    "id": "2b3b6312-2b67-4622-a7a4-e2c6de0cf597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_thumbnailMissing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ebcab86-9d3e-4c61-a7b5-92988963acc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b3b6312-2b67-4622-a7a4-e2c6de0cf597",
            "compositeImage": {
                "id": "b8d4cd9c-7258-47fe-9106-55a0b7bade3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebcab86-9d3e-4c61-a7b5-92988963acc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b5742c-b8e6-4097-967c-146b3f515aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebcab86-9d3e-4c61-a7b5-92988963acc5",
                    "LayerId": "7cd37e50-0bcc-4b1a-815a-ffa9a1c06d30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "7cd37e50-0bcc-4b1a-815a-ffa9a1c06d30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b3b6312-2b67-4622-a7a4-e2c6de0cf597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}