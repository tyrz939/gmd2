{
    "id": "df7194a3-d67d-409b-b916-fefe04fe4e45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaLaserStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "478e319d-055d-4e7c-8617-1e6557b05dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df7194a3-d67d-409b-b916-fefe04fe4e45",
            "compositeImage": {
                "id": "cf1e10d0-7e22-4faa-b88e-3c58d98d396a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "478e319d-055d-4e7c-8617-1e6557b05dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e152f7-6490-426a-9bf9-63220ee2eda8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "478e319d-055d-4e7c-8617-1e6557b05dab",
                    "LayerId": "7ff8ba44-1058-46de-a1e3-bd21a8a78461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7ff8ba44-1058-46de-a1e3-bd21a8a78461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df7194a3-d67d-409b-b916-fefe04fe4e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}