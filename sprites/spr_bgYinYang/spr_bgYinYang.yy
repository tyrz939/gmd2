{
    "id": "3024bb6b-29dd-43bf-b1b7-6f7ffb4e3f6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgYinYang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 947,
    "bbox_left": 12,
    "bbox_right": 947,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5149bd0e-0979-4eed-9564-95d5a90caae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3024bb6b-29dd-43bf-b1b7-6f7ffb4e3f6a",
            "compositeImage": {
                "id": "98e5e8a1-89d1-4b67-8bb6-0ac426611f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5149bd0e-0979-4eed-9564-95d5a90caae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd2e498-1375-4083-8e0f-18b1042356f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5149bd0e-0979-4eed-9564-95d5a90caae3",
                    "LayerId": "bcd5da6c-bd02-45bc-a652-435a0f6377af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "bcd5da6c-bd02-45bc-a652-435a0f6377af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3024bb6b-29dd-43bf-b1b7-6f7ffb4e3f6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 480
}