{
    "id": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4148b610-164c-4af1-98cf-160da2680975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "5ae77fa2-b91b-49b5-970c-2e21cd2603ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4148b610-164c-4af1-98cf-160da2680975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d519de5-c614-4d99-bdd7-30851c30338b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4148b610-164c-4af1-98cf-160da2680975",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "1669022b-2d09-433a-914b-b65861cb7e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "7cbc9dd7-a8be-464b-8c33-d3be7c824332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1669022b-2d09-433a-914b-b65861cb7e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f7ddd03-db1a-4dcd-808a-c5697ee25be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1669022b-2d09-433a-914b-b65861cb7e28",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "6345334a-d28d-4cfd-a4d5-5bbf18d0ef62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "13fc0d77-68fb-4050-9ee0-f11b6b61dee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6345334a-d28d-4cfd-a4d5-5bbf18d0ef62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d3a5a3-9883-4c4e-9d41-c6d3519d21de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6345334a-d28d-4cfd-a4d5-5bbf18d0ef62",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "d66f3469-4cf1-4b60-b36f-830403c9653f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "fcac0aab-762b-4db8-a9f5-585a62216705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d66f3469-4cf1-4b60-b36f-830403c9653f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72dc0286-2c3b-4b7d-abc6-b82976593b35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d66f3469-4cf1-4b60-b36f-830403c9653f",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "39cb6e6c-c550-49fa-ae39-bad5fa1f850f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "577b44c8-546f-4a7d-b4f8-aab2fd0e8576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cb6e6c-c550-49fa-ae39-bad5fa1f850f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e3c6271-da01-4001-a974-061a39140ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cb6e6c-c550-49fa-ae39-bad5fa1f850f",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "b309b92e-4035-4269-8320-ec75c7e631d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "ba737d89-156c-40e1-9a17-6048d461da44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b309b92e-4035-4269-8320-ec75c7e631d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de175e8-edaa-494e-a0ea-89165e3b6a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b309b92e-4035-4269-8320-ec75c7e631d9",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "b40a7892-e5bd-40fe-918d-2e7cb20b0bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "84f0a81e-a455-4c1e-a5ca-cbb0d7b4686f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b40a7892-e5bd-40fe-918d-2e7cb20b0bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ff2334-2799-4c2e-9878-36a38f4ac966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b40a7892-e5bd-40fe-918d-2e7cb20b0bd8",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "05db0228-ef14-4d42-81db-a1fb0a34c801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "2d562a97-7b38-44c9-98b1-e788d1a751a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05db0228-ef14-4d42-81db-a1fb0a34c801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e0d9fe-7d07-47de-b08b-8ac3b3768429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05db0228-ef14-4d42-81db-a1fb0a34c801",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "8246783b-82a6-4736-a3b8-91115ecc9f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "dfc7f13e-0a75-4fac-820e-752039b3d892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8246783b-82a6-4736-a3b8-91115ecc9f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4117c7-65ec-4687-bf86-2c5fb9bbd3f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8246783b-82a6-4736-a3b8-91115ecc9f50",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "8af0aa6e-5cf5-4fae-9c3e-553de15d2048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "e09d5593-757c-4e0a-8f8f-6cc2d0f27991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af0aa6e-5cf5-4fae-9c3e-553de15d2048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb90b764-c3a1-4dae-a713-2f49d0d871bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af0aa6e-5cf5-4fae-9c3e-553de15d2048",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "a1f75a3b-d68a-449d-b695-ffefb00391ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "e3f8aff5-7cc9-4424-b665-7da9a45b5f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f75a3b-d68a-449d-b695-ffefb00391ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7deae9-0e5c-4c44-933c-930a5b76e1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f75a3b-d68a-449d-b695-ffefb00391ea",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "ac7f7405-dfbc-46d1-9a81-640fa81f88ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "d203b512-b208-441b-982a-cb588fc47068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7f7405-dfbc-46d1-9a81-640fa81f88ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522c3bc5-9735-4620-ae3d-d43b4b58f168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7f7405-dfbc-46d1-9a81-640fa81f88ca",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "85af0d81-3603-4226-97e8-9512f4bfe049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "7e90f9ca-51d2-4f54-ba6b-e0804dedddf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85af0d81-3603-4226-97e8-9512f4bfe049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1f0f7ad-ecd4-4682-b6a8-3a124e002afd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85af0d81-3603-4226-97e8-9512f4bfe049",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "49236274-030d-4a55-893a-33a66328bafc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "5c2630f5-e9d9-46e9-86ed-f1819e1fafc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49236274-030d-4a55-893a-33a66328bafc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb73639c-5e9a-4243-ac35-1d06b7070a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49236274-030d-4a55-893a-33a66328bafc",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "ed9d1507-82bb-48f8-9a44-fb8b251517e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "91530d82-7bb6-4c8b-b8eb-9644c9ae8aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9d1507-82bb-48f8-9a44-fb8b251517e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd85f5d-98f8-425b-b41e-9293c099bc83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9d1507-82bb-48f8-9a44-fb8b251517e7",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "bd343c99-c158-4ac6-96ac-a1b55731240e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "a00b8fec-6a7b-4535-bfcb-a38e65e408dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd343c99-c158-4ac6-96ac-a1b55731240e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90155d1e-69e6-493f-a9e1-48c5863374af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd343c99-c158-4ac6-96ac-a1b55731240e",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "0fb528c1-ec63-4a7c-971b-b11f5e6135b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "1cde1443-1216-465d-9931-765ced8c6f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb528c1-ec63-4a7c-971b-b11f5e6135b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd148674-0ccc-4b35-a078-decdb03ded2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb528c1-ec63-4a7c-971b-b11f5e6135b0",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "242c0f05-d2c7-4d3d-96fa-9d317d6f0272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "0ed70688-ea8e-4dad-ba46-2ad0d82c3ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "242c0f05-d2c7-4d3d-96fa-9d317d6f0272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3f11df-d130-48cd-acfb-299a5dfd4c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "242c0f05-d2c7-4d3d-96fa-9d317d6f0272",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "cd3d641c-9b0b-4c58-8f30-8c92e00faaf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "5349aad8-e970-4634-a742-14b94eb5bd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3d641c-9b0b-4c58-8f30-8c92e00faaf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619643ec-1dec-4cb9-afa0-5ea5dd572178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3d641c-9b0b-4c58-8f30-8c92e00faaf6",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "d4e69a2e-0d8a-4de0-bf3b-82d38081668f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "f293a7d4-6dfc-482d-bb16-5c6f1e3277b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e69a2e-0d8a-4de0-bf3b-82d38081668f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48cbdfef-aec5-48d0-86d4-82824b94580c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e69a2e-0d8a-4de0-bf3b-82d38081668f",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "10a0e65f-e0e1-43ad-8133-340574237697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "23ae8dfe-aecc-4ba9-9317-3683d39ef21b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a0e65f-e0e1-43ad-8133-340574237697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81103a31-b98c-4773-836b-a0e7a10f2b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a0e65f-e0e1-43ad-8133-340574237697",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "c73bac58-57e8-46d4-928a-b676c1c64565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "a9291fc5-9685-44d0-b22d-cf96106e23ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73bac58-57e8-46d4-928a-b676c1c64565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c344699-24ef-4440-94ee-4f318388eb62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73bac58-57e8-46d4-928a-b676c1c64565",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "595bab94-b8bf-44a9-be20-a9e92ceed308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "f91e3283-fbd8-4cc4-af9d-02d87ac98017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595bab94-b8bf-44a9-be20-a9e92ceed308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db055ae-137f-4daa-8199-87ae404be70d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595bab94-b8bf-44a9-be20-a9e92ceed308",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        },
        {
            "id": "13d6f453-8659-4f20-a283-1ec450e03546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "compositeImage": {
                "id": "d0d978f2-1bc4-4e2c-903d-20d2547d10b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d6f453-8659-4f20-a283-1ec450e03546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74bf2b93-6333-4818-b912-45b99ef52d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d6f453-8659-4f20-a283-1ec450e03546",
                    "LayerId": "35eb9508-ef04-407b-a3af-05e4bb4b7452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "35eb9508-ef04-407b-a3af-05e4bb4b7452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a039afa-a41c-43c1-a683-b40e7b99e16a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 23
}