{
    "id": "7e00387e-e24e-416d-9c70-a2662b88588b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_haze_noise",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fda63c32-179a-4825-b7ce-afc3eaf13e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e00387e-e24e-416d-9c70-a2662b88588b",
            "compositeImage": {
                "id": "3f584718-40a2-4164-af18-efb60d59d74d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda63c32-179a-4825-b7ce-afc3eaf13e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "835de7b0-1813-42f9-9990-48451047340c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda63c32-179a-4825-b7ce-afc3eaf13e16",
                    "LayerId": "9c8e69aa-fd71-4614-a2bd-4f5a65ea1550"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9c8e69aa-fd71-4614-a2bd-4f5a65ea1550",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e00387e-e24e-416d-9c70-a2662b88588b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}