{
    "id": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Puff_Large",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 345,
    "bbox_left": 0,
    "bbox_right": 380,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bf6eac0-b421-41b5-82f0-c1d51f3b5a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "6e26dd32-6110-4bdb-9640-4d93743255c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf6eac0-b421-41b5-82f0-c1d51f3b5a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aad9d9a-e6df-4921-8110-36b4c0a54d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf6eac0-b421-41b5-82f0-c1d51f3b5a82",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "acfceffc-b49f-4f96-bc39-76f460ec3d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "b3149219-6a7f-486b-9431-a77d132c0b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfceffc-b49f-4f96-bc39-76f460ec3d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebbeabe-617c-45df-b8a2-10a951bc2249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfceffc-b49f-4f96-bc39-76f460ec3d24",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "2c1540c5-32bf-4309-b4fa-0b1c7c49335e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "642c59fe-0ebd-475b-b25f-d4c29d72c7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c1540c5-32bf-4309-b4fa-0b1c7c49335e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa6d6c0-9183-4f6c-b4e4-ceb7639e2cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c1540c5-32bf-4309-b4fa-0b1c7c49335e",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "64681f1d-424f-41f5-afd6-35b99c3e8809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "673affda-08b6-4633-954c-79a816f4f50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64681f1d-424f-41f5-afd6-35b99c3e8809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9015161-b87d-4024-9d5d-f1e9ec8d05c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64681f1d-424f-41f5-afd6-35b99c3e8809",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "ce20f1f2-e409-4d40-9d74-3417f91a8904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "b21c3660-344b-40df-902e-cd16b38d5b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce20f1f2-e409-4d40-9d74-3417f91a8904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e81b6b-9f5d-4453-a215-fae7aed207bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce20f1f2-e409-4d40-9d74-3417f91a8904",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "804501cb-79af-42a0-bfcb-0a175db6531c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "891cdcc2-ac6f-4ab0-9041-dd1712c18388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "804501cb-79af-42a0-bfcb-0a175db6531c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1732ea9-f25a-4f44-af1a-bac1bcd8479a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804501cb-79af-42a0-bfcb-0a175db6531c",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "05d51e04-0a3f-4135-9423-0d4ef160b8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "93375a25-78dd-4a1a-8368-9b3ded6164d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d51e04-0a3f-4135-9423-0d4ef160b8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9bcb1d-9214-4580-94e8-ef91717683c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d51e04-0a3f-4135-9423-0d4ef160b8b1",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "3f7a858f-459c-4c13-8d4f-898f85ae0934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "d6662b94-1ec4-4ddd-9efb-00386068d32a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f7a858f-459c-4c13-8d4f-898f85ae0934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0357fb34-9688-4f73-bd7c-0edfe2e743b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f7a858f-459c-4c13-8d4f-898f85ae0934",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "5115cb89-7936-495b-9616-4e1e3c299855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "4e746c84-7eef-4b92-8609-12ca7266b1e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5115cb89-7936-495b-9616-4e1e3c299855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5855efcd-1177-460d-b9c2-0f30dda23693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5115cb89-7936-495b-9616-4e1e3c299855",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "e1ed067e-bf6d-4f97-8dc3-7bc88679a350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "26905583-b1ef-48db-b5fd-7c8b2ae3e12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ed067e-bf6d-4f97-8dc3-7bc88679a350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "417c5c23-bd46-4344-a54f-b7b8c63634c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ed067e-bf6d-4f97-8dc3-7bc88679a350",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "f2f6a6aa-18c8-4af6-a950-112a296173fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "62962034-25de-415e-9e82-7411637c8452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2f6a6aa-18c8-4af6-a950-112a296173fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6cc3f8e-828f-4cce-b940-583d831d7b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2f6a6aa-18c8-4af6-a950-112a296173fa",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "93b0b28e-3d3e-4ecf-aa38-2ca5b5f5d17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "0c1b939e-48f6-4a60-881b-15f15f549375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93b0b28e-3d3e-4ecf-aa38-2ca5b5f5d17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c84f20-07f6-4ad9-b921-6a18df6a9852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93b0b28e-3d3e-4ecf-aa38-2ca5b5f5d17c",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "8a940a2a-5017-40ad-b150-5657beed930d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "0d107768-1c4f-430f-aa35-9fe9459fe9b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a940a2a-5017-40ad-b150-5657beed930d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a6f42ec-e4d4-41b2-b479-7f6e9bd2957c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a940a2a-5017-40ad-b150-5657beed930d",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "51ac0d76-1444-4b3f-82bc-90c7d469f9be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "1b041e8b-a5e8-4392-a361-341e2d62677c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ac0d76-1444-4b3f-82bc-90c7d469f9be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58042b33-2b73-4e9e-a46b-250adbfaf492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ac0d76-1444-4b3f-82bc-90c7d469f9be",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "7b09c8e9-312a-4c2a-932d-ef15a0693b14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "d4fa3b86-4ea4-490a-a404-92abcc842728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b09c8e9-312a-4c2a-932d-ef15a0693b14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe1883ef-c6ce-40ea-978c-50a8bb725052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b09c8e9-312a-4c2a-932d-ef15a0693b14",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "3531d14d-b7fd-4571-a920-860ebdfd3ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "e6f768ec-c3a8-4e77-977a-4d1faa67c525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3531d14d-b7fd-4571-a920-860ebdfd3ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b5008a-ba97-4ea0-bdc4-e89e8570a984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3531d14d-b7fd-4571-a920-860ebdfd3ef4",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "5d756c3b-118d-4306-ba74-db5e06358b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "4796a6c2-956b-4dbb-9b8b-2700eefea3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d756c3b-118d-4306-ba74-db5e06358b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a41d4bc-9dd5-438b-9e8e-1d21e6467b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d756c3b-118d-4306-ba74-db5e06358b54",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "314d8b5c-d28b-4c50-9987-a42bbd36494b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "f3e5de08-aa74-443c-9c8e-dc338a10177c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "314d8b5c-d28b-4c50-9987-a42bbd36494b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d371d784-31d0-47be-8e0a-c4b2060b4158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "314d8b5c-d28b-4c50-9987-a42bbd36494b",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        },
        {
            "id": "740e7911-9d64-4d9a-8e3e-db561c1ea5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "compositeImage": {
                "id": "b8bd7ef1-24ca-453f-aabd-f82bed01eba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740e7911-9d64-4d9a-8e3e-db561c1ea5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f4f857-971a-4144-840d-995fb017cb20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740e7911-9d64-4d9a-8e3e-db561c1ea5fa",
                    "LayerId": "79d30f96-d068-492c-a36c-ece0e1b8ee93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 346,
    "layers": [
        {
            "id": "79d30f96-d068-492c-a36c-ece0e1b8ee93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd6e6374-e285-4fbf-a797-9a9b02a12ba8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 381,
    "xorig": 190,
    "yorig": 173
}