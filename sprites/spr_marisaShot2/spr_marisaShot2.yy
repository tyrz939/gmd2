{
    "id": "82ae43f2-5d72-4f91-9da8-02f989a75f26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaShot2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f5eafc0-fb9f-4d02-b8f8-89a650106b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ae43f2-5d72-4f91-9da8-02f989a75f26",
            "compositeImage": {
                "id": "531dbd25-1b24-43fc-851c-2083e6aecc51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f5eafc0-fb9f-4d02-b8f8-89a650106b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8046b7d-3453-4e02-8d52-3a14e39819b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f5eafc0-fb9f-4d02-b8f8-89a650106b46",
                    "LayerId": "4f79d24a-76c6-43c2-99a6-7be16ceb2577"
                }
            ]
        },
        {
            "id": "89a08fbe-b31f-4cab-b6cf-3070374570c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ae43f2-5d72-4f91-9da8-02f989a75f26",
            "compositeImage": {
                "id": "10ab0b71-269e-421b-a57d-3fd0ec5e42f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a08fbe-b31f-4cab-b6cf-3070374570c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24cf1bc5-8a57-4e54-b0a8-92f6ef2b445e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a08fbe-b31f-4cab-b6cf-3070374570c0",
                    "LayerId": "4f79d24a-76c6-43c2-99a6-7be16ceb2577"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4f79d24a-76c6-43c2-99a6-7be16ceb2577",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82ae43f2-5d72-4f91-9da8-02f989a75f26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}