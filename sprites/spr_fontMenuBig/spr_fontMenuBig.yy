{
    "id": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fontMenuBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7858bb23-dc91-47ed-a3ad-5c8d5a165a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "93e98b79-3fa8-46f4-8093-c53fda109160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7858bb23-dc91-47ed-a3ad-5c8d5a165a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061c21e9-9bbc-4a13-b36d-a7c100eb6634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7858bb23-dc91-47ed-a3ad-5c8d5a165a8d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "89d3abe7-ea49-4b07-a346-47b6985ad017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "457e2c39-5e1a-42f8-aea5-f67c51d92f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d3abe7-ea49-4b07-a346-47b6985ad017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e632b7e-9679-431c-8ec1-9957747001fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d3abe7-ea49-4b07-a346-47b6985ad017",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "3c063910-4f0a-43dd-856e-fd649e77645c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "d9990fb9-46c1-41c2-a4ab-c34e3653ffb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c063910-4f0a-43dd-856e-fd649e77645c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09284ecb-794b-4600-a63d-64eef5604535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c063910-4f0a-43dd-856e-fd649e77645c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "bfa69754-a570-4e43-81e7-dc03eb8eb024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "b0f55fee-a336-4839-a918-aae576860a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa69754-a570-4e43-81e7-dc03eb8eb024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b030d4a9-6aa7-4d34-9094-7b1aec574442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa69754-a570-4e43-81e7-dc03eb8eb024",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "dbe4bf93-5441-48a4-b3bf-550f5738da8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "b227431e-5860-4616-847b-033fd331907f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe4bf93-5441-48a4-b3bf-550f5738da8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc626ad2-d09e-4e9c-a6ef-c3fd7285795d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe4bf93-5441-48a4-b3bf-550f5738da8d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "1cfa3059-9a86-4e53-b5f1-7f004fa7261a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "13b34a71-9cf2-4c68-8b0e-a42c33acf679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cfa3059-9a86-4e53-b5f1-7f004fa7261a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e787fad-16ec-4568-8a81-b8d2c87d618d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cfa3059-9a86-4e53-b5f1-7f004fa7261a",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "41528198-74fa-4945-8044-9563d9d0a6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "6f47e166-fe0c-493c-911e-fa478cd3e4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41528198-74fa-4945-8044-9563d9d0a6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1d0bdb-f88f-419c-b029-9136db4623df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41528198-74fa-4945-8044-9563d9d0a6b7",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "14fb21e8-9f99-42fa-8582-3ce999bb7adc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "fc55756e-7fae-45d7-8587-3d02c670af4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fb21e8-9f99-42fa-8582-3ce999bb7adc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9199355b-1538-4242-9d05-5d3ab4ac5dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fb21e8-9f99-42fa-8582-3ce999bb7adc",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9167d624-1efa-43f5-8e2e-1c4841fe1555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "6bc79829-c6cc-4897-aec9-46c50b4ec069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9167d624-1efa-43f5-8e2e-1c4841fe1555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce952cdf-a909-419e-8424-ab10ba3f992f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9167d624-1efa-43f5-8e2e-1c4841fe1555",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e51eab53-aefc-43e2-8da8-4934d6fdd6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "db701fe3-b2f3-43e6-9506-3781a9637cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e51eab53-aefc-43e2-8da8-4934d6fdd6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf269873-4679-46c4-8879-89b98d64d786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e51eab53-aefc-43e2-8da8-4934d6fdd6ed",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4d1793dc-cf0b-4dfa-b4f9-bd3a8a496e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "1d61e9c5-78c8-4446-a450-89bc72a0e151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1793dc-cf0b-4dfa-b4f9-bd3a8a496e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0750405-5786-4a3f-9947-bd2932c0708b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1793dc-cf0b-4dfa-b4f9-bd3a8a496e50",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "f40c1b53-5f16-42be-811e-3c9d23e01691",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "e59f36d2-ceec-4226-8535-25854afdd087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40c1b53-5f16-42be-811e-3c9d23e01691",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7557557d-6071-46e4-8831-21fd58e91473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40c1b53-5f16-42be-811e-3c9d23e01691",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9f17edd0-28fc-40c9-8e4a-74b3d16ce7d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "f758f2a8-ff07-4483-907b-6389f8b3e393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f17edd0-28fc-40c9-8e4a-74b3d16ce7d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce05047-33f9-4c05-adae-7d124b31e8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f17edd0-28fc-40c9-8e4a-74b3d16ce7d8",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "8bc05075-485d-4b4d-8cd1-988e731855ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "55e4fe35-6f54-48d0-ad85-cf0e86d42e5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc05075-485d-4b4d-8cd1-988e731855ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df63eccc-cf0b-429c-a3fb-bcc2e0ebe31e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc05075-485d-4b4d-8cd1-988e731855ba",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "432ef380-1e8a-40ee-a338-76a67fc482a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "31de9054-9afc-4a14-af10-0a56fc86baab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432ef380-1e8a-40ee-a338-76a67fc482a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "170240f2-8407-4dd4-a629-907c8208a5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432ef380-1e8a-40ee-a338-76a67fc482a7",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "0a1fdeda-6d1a-43a6-b123-c6c9d3418ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "92572fd3-0237-400c-b450-9eb847405713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a1fdeda-6d1a-43a6-b123-c6c9d3418ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be6c423-2e16-41cb-9ded-1117008586f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a1fdeda-6d1a-43a6-b123-c6c9d3418ea3",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "7ac4bf5a-7d3e-409f-bf59-8bb6527fb00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "057bf3ad-dbd4-4637-96cc-9ed89d4d95ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac4bf5a-7d3e-409f-bf59-8bb6527fb00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c220ff71-3835-47c3-8c3d-fc981852200f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac4bf5a-7d3e-409f-bf59-8bb6527fb00c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "eec2fdb4-d8a9-4eb7-939e-879f0e928b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "8b469453-8e0c-456c-b442-0827e4e8d35a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec2fdb4-d8a9-4eb7-939e-879f0e928b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97badb4e-ab61-4990-be0d-787094f21872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec2fdb4-d8a9-4eb7-939e-879f0e928b88",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "ab290785-624c-41a2-97bf-1471e2549b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "1955e00a-a447-48ce-bcb0-39b8d54fd704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab290785-624c-41a2-97bf-1471e2549b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4803b2-b2f2-4422-8c7d-e213a25be8a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab290785-624c-41a2-97bf-1471e2549b2b",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "0f078a75-e6a3-49f3-bbb7-c5e319c7999a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "8047fb6a-793f-4f68-a14a-315202886c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f078a75-e6a3-49f3-bbb7-c5e319c7999a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "608eb9b7-7047-4e84-a0d8-95c6fdbf5b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f078a75-e6a3-49f3-bbb7-c5e319c7999a",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "3f5dcde1-e4d8-4adc-afb4-b6f56e615a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "1f1eddc2-5538-4bd4-a0bf-5f65d93fba61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5dcde1-e4d8-4adc-afb4-b6f56e615a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebb9d86-b75e-41b8-86c8-6414857bddcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5dcde1-e4d8-4adc-afb4-b6f56e615a5f",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e851dc65-df4a-4151-a0a2-024d3bfc7e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ebcf4030-ccdc-45eb-bf79-b4df42e19b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e851dc65-df4a-4151-a0a2-024d3bfc7e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53c19554-bd57-4363-8721-de7905ce1382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e851dc65-df4a-4151-a0a2-024d3bfc7e1e",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e9ced86f-d419-4cb1-87ad-fcb667075d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "076b983a-4770-4f77-bec1-bc500b1a027d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ced86f-d419-4cb1-87ad-fcb667075d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b222809b-5869-4bde-b93f-860c5cf79ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ced86f-d419-4cb1-87ad-fcb667075d33",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "5e18b3af-70c8-45d3-b56d-17ca02c0886d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "33467462-8b79-49a6-879d-d2639e2b8ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e18b3af-70c8-45d3-b56d-17ca02c0886d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a6f0762-d70d-4be3-a2c8-fd47d474942c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e18b3af-70c8-45d3-b56d-17ca02c0886d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "bddf5558-e460-4467-b74c-0f65b04c4551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ad860e97-ca18-4856-a915-bfcdeebf8d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bddf5558-e460-4467-b74c-0f65b04c4551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24dddf39-571a-44a7-95c3-48c4b534439e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bddf5558-e460-4467-b74c-0f65b04c4551",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "ecaa1d5c-072d-4315-ba86-c5b253600beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "fcd5b857-64ec-4f9c-b775-4a20892f1672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecaa1d5c-072d-4315-ba86-c5b253600beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c11e695-994d-492d-be43-76eaf1dfb7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecaa1d5c-072d-4315-ba86-c5b253600beb",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "a33931c5-b4b3-461b-a37a-803a4691f27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "91d8fa47-eec3-49dc-9e1f-5780d40d1a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33931c5-b4b3-461b-a37a-803a4691f27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fbb2e54-9c5a-4c4c-a67b-0caa86244a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33931c5-b4b3-461b-a37a-803a4691f27e",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "7be30a61-783f-4b8d-8f60-6c991ce7f127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "51354b38-51f5-430d-87c9-fce586e2f420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7be30a61-783f-4b8d-8f60-6c991ce7f127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b43ec4-bc70-4f1d-9fd1-45ce567c952a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7be30a61-783f-4b8d-8f60-6c991ce7f127",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9a3767a7-a34a-4721-a12f-8dfff5c36ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "c77c19e1-5101-420e-9326-02fa8f041936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a3767a7-a34a-4721-a12f-8dfff5c36ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eef2130-2b75-4095-9a54-afa99820335f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a3767a7-a34a-4721-a12f-8dfff5c36ae7",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "b290809f-4ff5-4c5b-81bb-c7de1b9bca31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "8eb6cbdf-063f-4087-8723-2fba778b4d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b290809f-4ff5-4c5b-81bb-c7de1b9bca31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1548565d-35b4-47e1-9fa7-02afa28376da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b290809f-4ff5-4c5b-81bb-c7de1b9bca31",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "cc44997c-1fbd-4a11-b9ec-6860d40d4a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "405a09f6-2062-4dd5-88cc-02b253e9f3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc44997c-1fbd-4a11-b9ec-6860d40d4a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f2a973-05cb-4fa5-8a1e-db531aeccbe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc44997c-1fbd-4a11-b9ec-6860d40d4a0e",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "01123fa8-b668-4366-8780-3f00e513e33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "0a893b94-7e31-4b4e-9f3a-bb8fcb35164f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01123fa8-b668-4366-8780-3f00e513e33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c64fc3c-44e4-42b6-9162-d38dac1eee06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01123fa8-b668-4366-8780-3f00e513e33c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "a091f9df-5853-4dcd-a286-3eb7c120139a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ebabdb40-a8c9-4c31-b713-6b338a8a6f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a091f9df-5853-4dcd-a286-3eb7c120139a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7bf078c-affa-45c5-ade3-87a60e4ed926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a091f9df-5853-4dcd-a286-3eb7c120139a",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "5e6d0cb0-87af-4a5b-89ea-9b998bfa98d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "4493406c-97b5-4dc3-9ec0-ff0a009b189b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e6d0cb0-87af-4a5b-89ea-9b998bfa98d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ccb8d0d-e181-47cb-8617-96137b889a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e6d0cb0-87af-4a5b-89ea-9b998bfa98d1",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "25a5b808-55a8-4e7c-88bb-110242ed8fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "7c53bba5-5ee1-4b82-812a-2737c13a1fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a5b808-55a8-4e7c-88bb-110242ed8fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483b0bb4-bae8-40c9-82cf-b1a54eaa0d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a5b808-55a8-4e7c-88bb-110242ed8fba",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "d94ec74e-8004-434c-81b0-8a91b4751334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "2fdbe8f2-9ec7-4857-8ab0-ab091f65db7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94ec74e-8004-434c-81b0-8a91b4751334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b192328-01ab-40a0-b730-dfb7d6bf01f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94ec74e-8004-434c-81b0-8a91b4751334",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "5b1f5619-b759-4381-8951-c4c596eea8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "88e60f23-4939-4429-b7cf-d91ccaeeac65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1f5619-b759-4381-8951-c4c596eea8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10306db9-91cb-4e61-ab58-ebb0c535e257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1f5619-b759-4381-8951-c4c596eea8f2",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e755bb30-38a2-4e19-a495-c9d548f744eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "b4f46e9d-8616-4dc4-ac36-967f5942866a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e755bb30-38a2-4e19-a495-c9d548f744eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c77f77-7365-466a-8299-60152f326c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e755bb30-38a2-4e19-a495-c9d548f744eb",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "03983003-682c-4d74-bec9-a50b5ffce4fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "18a4145e-d89e-43ae-b8ce-bd98f3c92aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03983003-682c-4d74-bec9-a50b5ffce4fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38e623b-2b3c-4763-9502-27548b3db679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03983003-682c-4d74-bec9-a50b5ffce4fb",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "943125cd-60e4-4f71-9235-ae0acd121fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "fba774bf-d8ff-4912-b56a-b65f400a763a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943125cd-60e4-4f71-9235-ae0acd121fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054ff6bf-16a0-4629-895a-65ca58d47b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943125cd-60e4-4f71-9235-ae0acd121fcb",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "a18ec8ce-29c8-4d43-948d-b206e8212425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "97739c14-7af4-417c-9b19-c411ecdf2901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18ec8ce-29c8-4d43-948d-b206e8212425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b66d1c5-327c-4c15-a547-2947d387856f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18ec8ce-29c8-4d43-948d-b206e8212425",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "2fdfeb0a-d8dd-43c8-a3da-a4d921536e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "80e5da79-a577-460a-ab8f-36d366a05434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fdfeb0a-d8dd-43c8-a3da-a4d921536e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ad568e-1462-4bd5-800d-cf94f6215e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fdfeb0a-d8dd-43c8-a3da-a4d921536e66",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "608d4e90-6e6d-43d9-a2da-e33b23dd6793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "3595911a-d03d-49fe-bd92-1ca9d15b6d72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "608d4e90-6e6d-43d9-a2da-e33b23dd6793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72047f54-1021-4a15-b454-7420317b206c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "608d4e90-6e6d-43d9-a2da-e33b23dd6793",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "d585fcda-3dee-4df2-8eac-9c58e9ae5e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "e440f3fe-5c4b-4f89-b368-dc62d2ea87ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d585fcda-3dee-4df2-8eac-9c58e9ae5e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f565fd85-4415-4f4b-a199-87bf37828fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d585fcda-3dee-4df2-8eac-9c58e9ae5e02",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "067d3e96-641a-49ba-b422-34b582a4483d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "551bbcd1-9ea5-4b63-a7f1-284a9370735a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "067d3e96-641a-49ba-b422-34b582a4483d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb9dbbb-a072-4dd3-8b7b-0cf7d0498a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "067d3e96-641a-49ba-b422-34b582a4483d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4f65ed30-32f2-45c5-bd59-8b2a3c81512d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "c3f4a2fc-4b6f-47bb-b977-5498b542d6d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f65ed30-32f2-45c5-bd59-8b2a3c81512d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56a6fe80-12d4-4e6a-ab0c-6eab7627acec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f65ed30-32f2-45c5-bd59-8b2a3c81512d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "2b96a9f5-f6ef-47e0-b1fb-625c89ee1bb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "04e28bb2-0b9e-45ea-b63c-7421d4c09e5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b96a9f5-f6ef-47e0-b1fb-625c89ee1bb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b51be08-1284-432b-9a24-8a638a9d8c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b96a9f5-f6ef-47e0-b1fb-625c89ee1bb6",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "8bc91615-46d6-4762-a0d3-2155874451f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "2377eee6-6d1e-42ec-8dcf-99f0ab7d59d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc91615-46d6-4762-a0d3-2155874451f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494256a6-d1fe-45e9-91a2-1dc3b3f60d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc91615-46d6-4762-a0d3-2155874451f9",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "aed73c00-0b10-4a60-9b30-61fa2b60a287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "a9069c7e-f711-4b4d-8543-5895a7358df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed73c00-0b10-4a60-9b30-61fa2b60a287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad054867-1d5f-4d5d-a891-f55736999111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed73c00-0b10-4a60-9b30-61fa2b60a287",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "a50bd794-da01-4d7b-b292-e96c71423dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "8b6c1882-571c-4a76-bc52-076df13f7cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a50bd794-da01-4d7b-b292-e96c71423dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71673b5-4f18-4a5c-a5aa-f8fa210881f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a50bd794-da01-4d7b-b292-e96c71423dd5",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "91fe85e8-96ee-4ebf-b205-320688445377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "099fd5e2-1cd6-456c-822b-0c2cc76d5faf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91fe85e8-96ee-4ebf-b205-320688445377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6b427d-92f1-4849-91af-5365eae6b574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91fe85e8-96ee-4ebf-b205-320688445377",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "f0db6a5b-fef8-43f9-848a-b7281ee87234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "baf20fe4-0cd1-4d68-8c8c-45080d7e5aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0db6a5b-fef8-43f9-848a-b7281ee87234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb441265-dcf2-411b-a3c6-a920f9376b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0db6a5b-fef8-43f9-848a-b7281ee87234",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4e77495b-6bf5-4c7d-8181-287dc75ab0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "85745173-41d4-4be8-8d5d-f9c41113e8fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e77495b-6bf5-4c7d-8181-287dc75ab0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb661ec-1bbb-4702-90dd-c93970b94b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e77495b-6bf5-4c7d-8181-287dc75ab0e0",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "d5e256bd-827f-4198-a7ec-a41f9716ee64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "215df3ff-f977-4a6c-a9e0-3d0d3ffb4bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e256bd-827f-4198-a7ec-a41f9716ee64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a4c514-3567-4bd3-b389-6b7ef0a07e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e256bd-827f-4198-a7ec-a41f9716ee64",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "db4efa0c-a8e7-43b9-8b22-72fd43b34064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "8654b9dc-4f8f-4b3c-935b-cf5b1a491f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db4efa0c-a8e7-43b9-8b22-72fd43b34064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f036a52-4e36-421e-ae79-d83580df37f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db4efa0c-a8e7-43b9-8b22-72fd43b34064",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e7fa8c95-cde7-4c41-8de2-fbdc109f12c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "b9283ded-9b08-4689-af22-361b3140b576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fa8c95-cde7-4c41-8de2-fbdc109f12c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d67f58-4c71-4189-88ed-81a6d1e43367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fa8c95-cde7-4c41-8de2-fbdc109f12c9",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "544fa009-8c22-4607-ba06-61fe8af3007c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "55b900dd-68de-4999-925d-25ffcdf847b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "544fa009-8c22-4607-ba06-61fe8af3007c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9219574a-253c-4dd6-a407-0d0808321d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "544fa009-8c22-4607-ba06-61fe8af3007c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "ef4b2559-76eb-485c-b56d-b075b9db7707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "4919c12d-7a5c-46f9-aaa7-c35f62fe4843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4b2559-76eb-485c-b56d-b075b9db7707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d943f080-7cd0-416d-b163-5d5cd14bf2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4b2559-76eb-485c-b56d-b075b9db7707",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "be3263ad-fb31-4ee3-b092-e4cd75666e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "00d8342a-2a0c-432d-a954-16f9af9dc9ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3263ad-fb31-4ee3-b092-e4cd75666e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c85195-eb85-44f7-bb24-5824f4c17fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3263ad-fb31-4ee3-b092-e4cd75666e89",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "617261eb-7f24-4916-bc54-137953b72a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "4cbb0da7-b1ca-4dfa-8d49-110c15ecb6ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617261eb-7f24-4916-bc54-137953b72a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a05dec7-31b5-4691-ad44-522d05bf2109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617261eb-7f24-4916-bc54-137953b72a14",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "f70c6494-6180-4ac9-bda1-e1abd4aab1b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "582872e1-47c7-4368-b2c2-01c3f89b1683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f70c6494-6180-4ac9-bda1-e1abd4aab1b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17338db-bec0-44b1-9dab-551ab23a1f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f70c6494-6180-4ac9-bda1-e1abd4aab1b4",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "2c4f3805-6098-4caa-a1e1-4dd5e918cca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "52309f06-c935-429f-925a-472e6b96cfe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4f3805-6098-4caa-a1e1-4dd5e918cca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55179b1-ba0b-4589-871d-48b0f24675f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4f3805-6098-4caa-a1e1-4dd5e918cca3",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e7ffb020-1eea-401b-b036-5e62c25730b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "886aa94c-2e04-4a21-8654-d8092686cbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ffb020-1eea-401b-b036-5e62c25730b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9232fa5-f38c-45f1-96bd-ebae12ea4d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ffb020-1eea-401b-b036-5e62c25730b7",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "8fc7ee8b-43b6-4216-84fa-11a52d7f80d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "45864ecb-cfa2-48a2-ba7f-e7aba89fe854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc7ee8b-43b6-4216-84fa-11a52d7f80d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1f497f-2a36-4a5a-9b02-69a5ba30495d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc7ee8b-43b6-4216-84fa-11a52d7f80d4",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "383d081b-7e0b-43f0-ad7d-7d395152abf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ebaa9806-632f-40e8-87c0-99ac1f102475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383d081b-7e0b-43f0-ad7d-7d395152abf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d6f887-122b-4222-be68-cb89b9789523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383d081b-7e0b-43f0-ad7d-7d395152abf7",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9b9e5132-7231-4bb6-a00c-5f2792f75dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "27673949-ebb2-4f16-a98c-e6d9b529dec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b9e5132-7231-4bb6-a00c-5f2792f75dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c56dba-4b59-4435-a3b4-164e05fc7818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b9e5132-7231-4bb6-a00c-5f2792f75dcf",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "2debc423-5e16-4046-8240-441388969743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "4cea4331-bc67-48fc-af9f-9bf86808dfcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2debc423-5e16-4046-8240-441388969743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ce4a5f-b2f6-4ef2-8cc2-0109f9dd243b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2debc423-5e16-4046-8240-441388969743",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "22b9ed7d-d20f-4419-a129-7cfc9feb4645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "0353dd53-0011-4a44-a3bb-c27157005a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b9ed7d-d20f-4419-a129-7cfc9feb4645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7cda2e-c214-4fde-8bb3-554f032fa102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b9ed7d-d20f-4419-a129-7cfc9feb4645",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "122f43ac-be44-469c-ad2e-d7c23c54fb0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "c58ed9a7-19db-42a3-8e68-f6962fb78f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "122f43ac-be44-469c-ad2e-d7c23c54fb0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e9d4323-c04d-4851-b075-83fa0a417247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "122f43ac-be44-469c-ad2e-d7c23c54fb0d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9202d5a2-3191-42d6-ab2c-6e6e2299245f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "33933f95-2a78-4ecd-833a-6461398d100f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9202d5a2-3191-42d6-ab2c-6e6e2299245f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20a05e0-c2b7-4369-b4ec-7525b2247dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9202d5a2-3191-42d6-ab2c-6e6e2299245f",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "6ff20472-c054-466a-8e52-85f2efa2473c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "c3bd53f6-0a08-48d1-8bea-f062756d1685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff20472-c054-466a-8e52-85f2efa2473c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f905ded-f53b-4410-a062-0dd4f09c8cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff20472-c054-466a-8e52-85f2efa2473c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "81244fa7-7b7b-4000-8eb8-4c5bff9ff1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "4f6d4937-f41f-4c22-89b6-d7ffc4a3e5f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81244fa7-7b7b-4000-8eb8-4c5bff9ff1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe08e186-f1bf-431e-bf20-d86b90c53773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81244fa7-7b7b-4000-8eb8-4c5bff9ff1e5",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "eb2b53ef-0566-40fd-ba77-7d85dd98ec9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "d1d9da44-ca3e-4286-b22b-05c9210b19de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2b53ef-0566-40fd-ba77-7d85dd98ec9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebda2771-d02d-42bd-84d0-2ae3641c8d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2b53ef-0566-40fd-ba77-7d85dd98ec9c",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "85a23ab0-1158-43af-b0ab-1374ddce1789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "775c9ff0-0675-42c1-b3ce-a4614eb1151a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a23ab0-1158-43af-b0ab-1374ddce1789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fafb1085-8939-40e7-a714-b81c1402cabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a23ab0-1158-43af-b0ab-1374ddce1789",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "e82989b7-8c06-4bde-a106-29979da61a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "1e71f667-6376-4d8e-9a65-1bb0c0740b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82989b7-8c06-4bde-a106-29979da61a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7924953e-8d00-4118-b08b-d67a25166285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82989b7-8c06-4bde-a106-29979da61a40",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4759b9a6-e243-48dd-9249-3e0767d89813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "504e0592-87c0-4924-bea2-0a0f4e2a7398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4759b9a6-e243-48dd-9249-3e0767d89813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9305e73-de95-4c46-96fc-bc6b24c0731c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4759b9a6-e243-48dd-9249-3e0767d89813",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "9cf62f25-4b33-41fb-be52-c6adfff664f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "41016ac8-fa74-4907-b43d-8bf0407958c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cf62f25-4b33-41fb-be52-c6adfff664f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cac52d-6810-4667-8216-b621398913ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cf62f25-4b33-41fb-be52-c6adfff664f8",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "510ba2c1-3f44-40d4-9791-77ae9786a51d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "362f049c-ba1e-4068-b03f-86bb7134179c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510ba2c1-3f44-40d4-9791-77ae9786a51d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa252a5-ba0b-409c-9685-8e0cf3b2fb35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510ba2c1-3f44-40d4-9791-77ae9786a51d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "cdb45ae9-b993-4f55-95bb-381e3644096a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ee8ea950-572b-41d1-91e6-a74c657e50eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdb45ae9-b993-4f55-95bb-381e3644096a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e84b89-a85e-4c23-bdb7-5048c2c9d761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdb45ae9-b993-4f55-95bb-381e3644096a",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "3e62d97a-96ad-44ca-8f6f-66deeb456593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "3d1c4402-1872-4744-9386-9a13fc100d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e62d97a-96ad-44ca-8f6f-66deeb456593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca83000-9c9d-4886-9774-ad48d3e88bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e62d97a-96ad-44ca-8f6f-66deeb456593",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "a13c7892-9043-4ab4-8654-1877c1ebb48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "d21f8004-8858-43b8-93d4-eef5bee2a776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13c7892-9043-4ab4-8654-1877c1ebb48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6cc6924-86ac-4561-bb07-d2000de50d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13c7892-9043-4ab4-8654-1877c1ebb48d",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4647b6df-d10b-4f3f-8a35-f29974e898b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "1d1db2e4-5873-431f-b147-2c2bc029d47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4647b6df-d10b-4f3f-8a35-f29974e898b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b239276-379b-4a44-a4b4-234bfee28462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4647b6df-d10b-4f3f-8a35-f29974e898b5",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "17c01eb2-3b7d-451f-b9b8-9da511bc5ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "a80d2d61-5f8a-4903-9773-81e5361bf645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c01eb2-3b7d-451f-b9b8-9da511bc5ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ef6473-c327-42f7-a0ee-1b2668daba7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c01eb2-3b7d-451f-b9b8-9da511bc5ecc",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "099750d7-4d2e-4603-ac1a-eb2004944f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "0e476c53-3dfc-43d3-a812-ce5f29baf717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099750d7-4d2e-4603-ac1a-eb2004944f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3397c0-e4b5-4ae1-ad64-6af5b03e23a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099750d7-4d2e-4603-ac1a-eb2004944f3e",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "0ebf2723-926f-48d2-948c-286c770e1a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "ab83cfb2-1974-4ec0-95f1-1fbdd97ee8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebf2723-926f-48d2-948c-286c770e1a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e2fe281-dc2c-4e03-a726-2676ca69571b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebf2723-926f-48d2-948c-286c770e1a93",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "8922a296-6356-400b-a223-c2ef1ba6e533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "f443ceab-ecdf-49d1-a499-efd937c43e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8922a296-6356-400b-a223-c2ef1ba6e533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9b9894-9c20-408c-9b57-7ef29ad2ec1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8922a296-6356-400b-a223-c2ef1ba6e533",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "736f2b4e-45ef-4073-9465-267e817074c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "a7f1d22d-962a-4ff5-bc61-6f1eb6713d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "736f2b4e-45ef-4073-9465-267e817074c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af5f743-dec9-4c50-a649-385e35729e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "736f2b4e-45ef-4073-9465-267e817074c4",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4ef5512e-ccda-4bca-82d9-59a23b2d30eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "aff5a980-bd1d-44c5-81a3-4841c77bb400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef5512e-ccda-4bca-82d9-59a23b2d30eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c12f265-765f-42ec-b4b5-703ec67f2bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef5512e-ccda-4bca-82d9-59a23b2d30eb",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "2b83e9d0-e284-451e-a163-fd25daa1884a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "da8b8a27-5389-485b-ad00-fb73467030a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b83e9d0-e284-451e-a163-fd25daa1884a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c4f79da-f86d-4381-960d-5db198d45bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b83e9d0-e284-451e-a163-fd25daa1884a",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "4c1e4d82-f1d7-42e7-940a-f0b4065a49a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "d76f4cc8-8b7d-463e-ba13-c54579cab888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1e4d82-f1d7-42e7-940a-f0b4065a49a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3acafccc-d845-4f7d-be78-5aceed8532b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1e4d82-f1d7-42e7-940a-f0b4065a49a6",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        },
        {
            "id": "90aaa151-2678-41c7-95d2-e11d2a0d85a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "compositeImage": {
                "id": "62e0edd3-6838-44df-95e2-46d4c71785eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90aaa151-2678-41c7-95d2-e11d2a0d85a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bac38c-45b3-4f20-8fd9-68305adc95a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90aaa151-2678-41c7-95d2-e11d2a0d85a2",
                    "LayerId": "94093595-4aae-4b48-a6e1-85d102fff0b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "94093595-4aae-4b48-a6e1-85d102fff0b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0596795b-fbbb-46ac-b14f-2bbe002a8bc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}