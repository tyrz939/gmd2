{
    "id": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_despawn0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "948b6104-ecc4-406b-95c3-2ad69228496a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "016a3fd6-ec10-4985-99f8-c5acaa3c74fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948b6104-ecc4-406b-95c3-2ad69228496a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a76da71f-0f08-4f5a-b5d0-daf81ea93fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948b6104-ecc4-406b-95c3-2ad69228496a",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "8557764e-42f2-4e77-b7b0-efec11b1c89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "55d1bff8-ce72-4ec6-8866-f857c90ac097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8557764e-42f2-4e77-b7b0-efec11b1c89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "376d9067-584b-4c15-bd1e-e3c0539d5f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8557764e-42f2-4e77-b7b0-efec11b1c89e",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "516e2042-1841-4022-867e-b2e8dcbde258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "891c04d7-e9b0-4d4e-bfc1-47f8b4a2aa52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516e2042-1841-4022-867e-b2e8dcbde258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716f5086-ba3c-46f6-9801-3beb7466805f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516e2042-1841-4022-867e-b2e8dcbde258",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "7b573617-8af6-4bf4-8120-8a2956c891e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "bdbc96bc-e5b3-4d4c-8033-ed100f27559d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b573617-8af6-4bf4-8120-8a2956c891e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51707b48-ff57-49e1-be16-2979c865801e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b573617-8af6-4bf4-8120-8a2956c891e3",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "7c2c49ea-4012-478f-a0e0-97ef19058a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "d1a0bfa2-5525-4a1c-ab5f-cec51abffd1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2c49ea-4012-478f-a0e0-97ef19058a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67719d10-f065-4850-a70f-cd3177494b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2c49ea-4012-478f-a0e0-97ef19058a74",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "0ea7b39b-bca1-4250-ba44-c249a6e4457d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "36dd938b-debc-4630-a4c8-581038fce785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea7b39b-bca1-4250-ba44-c249a6e4457d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c523caa-af3a-4b97-aaaf-5ac201e5816e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea7b39b-bca1-4250-ba44-c249a6e4457d",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "6dd4b18a-d55c-4bc8-af09-a1b60c70d545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "5fd074d5-78c9-4af5-8ba0-75e44d2d46bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd4b18a-d55c-4bc8-af09-a1b60c70d545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdca5312-f011-493b-8a5f-5612ed229620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd4b18a-d55c-4bc8-af09-a1b60c70d545",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        },
        {
            "id": "24532156-0e61-44a9-8c46-da3c09630aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "compositeImage": {
                "id": "060ad59f-31ed-42d3-8863-1341711a379b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24532156-0e61-44a9-8c46-da3c09630aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1133ece-9981-4eff-9957-1d5be9d3956e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24532156-0e61-44a9-8c46-da3c09630aa9",
                    "LayerId": "68e96935-d211-4287-aa76-2ba7696f6910"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68e96935-d211-4287-aa76-2ba7696f6910",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b3cf5ec-852f-4a05-b67b-984fdaa8cab7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}