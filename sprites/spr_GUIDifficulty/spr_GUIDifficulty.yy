{
    "id": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUIDifficulty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 42,
    "bbox_right": 214,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4b632b9-e1e9-4366-905e-6e1a4c2d01e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
            "compositeImage": {
                "id": "a5d1f9cd-c677-451a-aa14-ff6ab6a08064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b632b9-e1e9-4366-905e-6e1a4c2d01e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3f19d6-e5ca-4ed3-89fe-218c92ed914f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b632b9-e1e9-4366-905e-6e1a4c2d01e4",
                    "LayerId": "15b67884-a53f-4f55-855e-77a8a21c88e6"
                }
            ]
        },
        {
            "id": "9038ed7e-3939-4af0-a28d-ac3a9c11d954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
            "compositeImage": {
                "id": "712bdb12-df85-49f8-868e-c98af42fb812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9038ed7e-3939-4af0-a28d-ac3a9c11d954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faef0804-add0-4909-9ffe-15010f7ccdeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9038ed7e-3939-4af0-a28d-ac3a9c11d954",
                    "LayerId": "15b67884-a53f-4f55-855e-77a8a21c88e6"
                }
            ]
        },
        {
            "id": "49c50dce-a499-4184-8377-ae108229b251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
            "compositeImage": {
                "id": "b8f82e5a-1ae1-4993-9301-60a7f907c6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c50dce-a499-4184-8377-ae108229b251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571252a2-8eb0-47f6-82a4-e7688b2aa600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c50dce-a499-4184-8377-ae108229b251",
                    "LayerId": "15b67884-a53f-4f55-855e-77a8a21c88e6"
                }
            ]
        },
        {
            "id": "b2964523-52bb-4af6-8b3c-c9bb3c07a707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
            "compositeImage": {
                "id": "28620e18-6af5-437c-a940-0c5e822dc26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2964523-52bb-4af6-8b3c-c9bb3c07a707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e5483f-fd3b-4e90-80b8-327d8a545132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2964523-52bb-4af6-8b3c-c9bb3c07a707",
                    "LayerId": "15b67884-a53f-4f55-855e-77a8a21c88e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "15b67884-a53f-4f55-855e-77a8a21c88e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49f7feec-b3b8-4c06-87a2-11679d2d5bb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 0
}