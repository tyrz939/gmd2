{
    "id": "bbc54e32-3e29-458e-82c9-c29aa3df7e1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUIStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c09b881-ab51-4b50-b674-a04f850ff398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc54e32-3e29-458e-82c9-c29aa3df7e1a",
            "compositeImage": {
                "id": "632e59f4-6f95-433b-89cc-582284199ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c09b881-ab51-4b50-b674-a04f850ff398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4b7a89-3cc0-4067-92d6-4718f95d10a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c09b881-ab51-4b50-b674-a04f850ff398",
                    "LayerId": "f2c54cb6-d455-4652-849a-aafd8a5efeb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "f2c54cb6-d455-4652-849a-aafd8a5efeb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc54e32-3e29-458e-82c9-c29aa3df7e1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}