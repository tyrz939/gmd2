{
    "id": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c0206d8-0174-468b-94be-00bfe866342b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "8d6fa1a8-fc76-49e1-8f39-0649e1bdb8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0206d8-0174-468b-94be-00bfe866342b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8636cb-cb98-43d3-b8bb-8822139b27fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0206d8-0174-468b-94be-00bfe866342b",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "688f47d3-c447-426b-8a13-7fec8c03eeab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "7508b570-f858-410f-874f-7cbd73392bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688f47d3-c447-426b-8a13-7fec8c03eeab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3fba6e1-cfd7-4823-be5f-ca0e7daaff1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688f47d3-c447-426b-8a13-7fec8c03eeab",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "06712f43-b93b-4cb7-9488-f4656aff7b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "e9daa21d-86ff-4b86-a55e-19105c1e4357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06712f43-b93b-4cb7-9488-f4656aff7b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "758e6991-17ce-4145-9d13-d3b91236bf78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06712f43-b93b-4cb7-9488-f4656aff7b3a",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "675fc907-3f3a-494c-88f9-b0725b8804e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "75adfd0e-0d7a-4184-8b7b-71ebc9730da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675fc907-3f3a-494c-88f9-b0725b8804e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2802ebdd-efe5-4eb3-9eca-9218e0f93076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675fc907-3f3a-494c-88f9-b0725b8804e9",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "3a86c9e9-eda2-4dff-8f04-eed2aaed784c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "19262d61-ae5b-4e9d-b348-b6f192d61829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a86c9e9-eda2-4dff-8f04-eed2aaed784c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ef832fd-4e32-4d1f-bb1a-700225cde090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a86c9e9-eda2-4dff-8f04-eed2aaed784c",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "4ffb6c11-a868-48b3-9f75-f0448edb7002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "1fed4cc4-cda7-4cf9-a2a3-c42c66150de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ffb6c11-a868-48b3-9f75-f0448edb7002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03884ebd-8d75-45e1-91d1-8ae72f0dc2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ffb6c11-a868-48b3-9f75-f0448edb7002",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        },
        {
            "id": "98f989d3-ed58-4b46-aa4f-50d73ed4d582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "compositeImage": {
                "id": "300a26f6-4455-4eb5-9264-bd0ff4188b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f989d3-ed58-4b46-aa4f-50d73ed4d582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52bf4106-81ee-46d9-aaa7-bf2e9caa3b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f989d3-ed58-4b46-aa4f-50d73ed4d582",
                    "LayerId": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "db8dd124-4b8a-4a2f-92b1-6da5fd80db4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bfdad0c-0b48-4a8a-b493-0a38e1f304b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}