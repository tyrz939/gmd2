{
    "id": "dd6e41df-3385-495c-99e0-c7f27b18d2b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a533561-1096-41de-8b20-46397ce3b32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6e41df-3385-495c-99e0-c7f27b18d2b0",
            "compositeImage": {
                "id": "7f62566a-32ad-44c0-8d59-937325f17830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a533561-1096-41de-8b20-46397ce3b32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215d83ab-4278-4c98-b011-7f8652f7608b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a533561-1096-41de-8b20-46397ce3b32f",
                    "LayerId": "d18dcfb7-25da-4932-b08a-6992af288a39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d18dcfb7-25da-4932-b08a-6992af288a39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd6e41df-3385-495c-99e0-c7f27b18d2b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}