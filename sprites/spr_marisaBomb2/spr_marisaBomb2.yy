{
    "id": "a6a58603-2f4c-4a7a-a7ba-6fdee14b12ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaBomb2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b943e3e-ec12-4b7b-ab66-47ed7a8229e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6a58603-2f4c-4a7a-a7ba-6fdee14b12ae",
            "compositeImage": {
                "id": "c6762ba4-aa8c-4c0f-a095-a888c6589188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b943e3e-ec12-4b7b-ab66-47ed7a8229e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1877ffe1-8cc4-4611-9fca-3ba2db023db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b943e3e-ec12-4b7b-ab66-47ed7a8229e2",
                    "LayerId": "abe4a89c-49e7-490d-a2c1-5de521ee327e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "abe4a89c-49e7-490d-a2c1-5de521ee327e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6a58603-2f4c-4a7a-a7ba-6fdee14b12ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 48
}