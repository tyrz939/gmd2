{
    "id": "5a3be97e-bd33-44c7-bfc0-e7a829b8125f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaBomb1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8d46fef-8344-4611-8749-962c3ce8e07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a3be97e-bd33-44c7-bfc0-e7a829b8125f",
            "compositeImage": {
                "id": "7161f1dd-5784-40a0-9588-cc3e082a9094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d46fef-8344-4611-8749-962c3ce8e07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f3ebcbb-5309-45fd-beb3-da2b3afcd3f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d46fef-8344-4611-8749-962c3ce8e07b",
                    "LayerId": "a6bd4262-0253-447a-86eb-ea419d38c865"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a6bd4262-0253-447a-86eb-ea419d38c865",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a3be97e-bd33-44c7-bfc0-e7a829b8125f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 240
}