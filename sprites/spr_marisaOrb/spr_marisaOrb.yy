{
    "id": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisaOrb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "576f0add-4f29-4091-8970-615b22b58789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "27c4a3e4-8e23-43f9-a9c5-a12cef0902e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "576f0add-4f29-4091-8970-615b22b58789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b758e3f-49c6-4017-82bf-671f523ed401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "576f0add-4f29-4091-8970-615b22b58789",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "b14bcff3-2981-4bc4-b68c-7d46df08835d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "9a37d4da-4780-4cef-b413-abbfe497e582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14bcff3-2981-4bc4-b68c-7d46df08835d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55beb1bb-d913-4903-8666-21ad0eae8ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14bcff3-2981-4bc4-b68c-7d46df08835d",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "9f7f626b-68e4-40b5-a5a5-081f75187692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "1a075288-ef9a-4f33-bd88-e2a0a9d27584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f7f626b-68e4-40b5-a5a5-081f75187692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a8df04-6c7b-4761-acef-82ae003709d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f7f626b-68e4-40b5-a5a5-081f75187692",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "9c2fe3d1-c85a-4417-b56c-2cca4d1e10ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "443bf30c-db17-441f-9d1a-e9e9942c1f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2fe3d1-c85a-4417-b56c-2cca4d1e10ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da77d880-152c-46f8-a96f-16039823ff0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2fe3d1-c85a-4417-b56c-2cca4d1e10ed",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "7561ec4d-8c6c-4ea6-8985-246954d38ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "80c264ae-2d64-4daa-b2ff-070daee33f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7561ec4d-8c6c-4ea6-8985-246954d38ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74963688-afd7-4b95-bd73-716cf7b8eb6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7561ec4d-8c6c-4ea6-8985-246954d38ab2",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "c353e511-0798-419d-849e-a93017b64f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "12da694c-9751-4b5e-b549-5eb874519976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c353e511-0798-419d-849e-a93017b64f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bff7bf-2707-483c-a2b3-a784e96361f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c353e511-0798-419d-849e-a93017b64f88",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "3c8c5ec4-8cb7-48bf-a3d2-e956c7c61fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "30f2a2ee-65e3-4b68-b1e6-fbb4f444dfba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c8c5ec4-8cb7-48bf-a3d2-e956c7c61fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4630ed7-9c45-4dcb-a1ff-6afa53e0410c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c8c5ec4-8cb7-48bf-a3d2-e956c7c61fc0",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "a69ff2bc-106e-4b24-bd83-c301f25516be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "1d2978f7-6382-45fa-90d4-12bcc51248fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69ff2bc-106e-4b24-bd83-c301f25516be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "374b5e0d-889d-4199-845f-99cc6d8d4a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69ff2bc-106e-4b24-bd83-c301f25516be",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        },
        {
            "id": "7b3f4b70-65a7-4d2f-ae21-3923c2332544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "compositeImage": {
                "id": "ade5be7d-9d20-4e80-8010-2b38fceaac1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3f4b70-65a7-4d2f-ae21-3923c2332544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f08278-3642-4b97-9f54-4fc86b8c10c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3f4b70-65a7-4d2f-ae21-3923c2332544",
                    "LayerId": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3a3b253f-6c37-4f01-a022-3c7fbaf1c4dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29ecea1e-b60d-4e2f-a970-7d04e6fbdb31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}