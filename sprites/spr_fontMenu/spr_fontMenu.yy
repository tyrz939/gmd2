{
    "id": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fontMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8dcf0c7-dc27-431c-9c69-79eeb83fd8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a8994b0f-d865-4bc8-b904-290f5cec3013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8dcf0c7-dc27-431c-9c69-79eeb83fd8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9db6d795-b91e-4c1b-a887-edfa3cbef7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8dcf0c7-dc27-431c-9c69-79eeb83fd8c9",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "9179182f-81fe-4d43-a53c-9748adbad297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "6bd45e56-e333-4920-a06b-dcab6e845d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9179182f-81fe-4d43-a53c-9748adbad297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad35e4f6-6012-4c3b-9a13-b05f48e603df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9179182f-81fe-4d43-a53c-9748adbad297",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "00402bd4-e26b-4b4a-89ff-284b658c7cb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "3b977df9-b5fd-4682-8074-4864d2223a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00402bd4-e26b-4b4a-89ff-284b658c7cb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbcf48a-eee9-4bac-ac3b-519efd22087f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00402bd4-e26b-4b4a-89ff-284b658c7cb1",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "335ac9a6-185c-4322-9974-36199628dcf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "d58efc0d-0af3-4a88-b755-6ccf0a752697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "335ac9a6-185c-4322-9974-36199628dcf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d058cb8-8bb1-4f5d-87bf-549c801b29bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "335ac9a6-185c-4322-9974-36199628dcf2",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "e4558c2e-418d-4866-aa76-4cc9fa70ea20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "8a0f3561-504b-42af-8bdc-b250a679d1f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4558c2e-418d-4866-aa76-4cc9fa70ea20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc92ef73-313a-4c79-a8a5-89ea3de8de6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4558c2e-418d-4866-aa76-4cc9fa70ea20",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "8f965b3e-94e9-482e-8c5e-1dd8baba0453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "fd635793-f257-4433-b7d5-d15df06398f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f965b3e-94e9-482e-8c5e-1dd8baba0453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e40eea-3492-4683-abf9-c91063df62d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f965b3e-94e9-482e-8c5e-1dd8baba0453",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "ff0f61ac-0d8c-4c5a-99a0-a38c4bedbb62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "eb2ac375-3b76-45d4-a864-9cdce124b378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0f61ac-0d8c-4c5a-99a0-a38c4bedbb62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89f9426f-10c0-462e-9da8-d84c7535a4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0f61ac-0d8c-4c5a-99a0-a38c4bedbb62",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "91eca438-8418-42a7-b1d4-0fd4ea996bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "f03bdd01-4e35-4525-b1be-b037ec06a8b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91eca438-8418-42a7-b1d4-0fd4ea996bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26ecf23-231f-4921-9e17-49383448fc10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91eca438-8418-42a7-b1d4-0fd4ea996bb1",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "07fd7b6c-e550-4d43-9b93-a2bb5446c883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "03be4b44-ce45-47f1-bc17-7956bd976e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07fd7b6c-e550-4d43-9b93-a2bb5446c883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9807c6b5-5d31-4d9f-9e03-e4595df7da4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07fd7b6c-e550-4d43-9b93-a2bb5446c883",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "edf07ee8-b6e7-4422-8ae1-782719de934b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "9216736f-2c69-4622-8cf7-dd2ec7ced9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf07ee8-b6e7-4422-8ae1-782719de934b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232a4997-9190-4a51-b1f8-633c8f961424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf07ee8-b6e7-4422-8ae1-782719de934b",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "5d2f43c0-a81d-45ec-9a2f-47cdd28d81a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a94ea819-13c7-4d1a-a338-a4cb3d9155bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d2f43c0-a81d-45ec-9a2f-47cdd28d81a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a918d64e-a574-4701-90e2-76677fee0eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d2f43c0-a81d-45ec-9a2f-47cdd28d81a8",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "a5267073-0fa2-479b-9725-f9ba368b9f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "9b18228c-b91e-4bb7-bae4-328dc9e7bdc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5267073-0fa2-479b-9725-f9ba368b9f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05ac1c0-4184-4a64-8f40-978d2a6f781f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5267073-0fa2-479b-9725-f9ba368b9f7e",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "5d7d95f8-8496-4ae7-88bf-ad875ef68e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "77f6e910-3403-4246-b11d-16cbf32d8c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7d95f8-8496-4ae7-88bf-ad875ef68e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a0f4ab8-4d73-41db-8a59-3374ccb57f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7d95f8-8496-4ae7-88bf-ad875ef68e5d",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "0b29a9f4-2e7a-4e6f-9642-1dcbac96a61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "c2764b13-0f17-4031-aad9-aca446e974ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b29a9f4-2e7a-4e6f-9642-1dcbac96a61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ca4636-9e3a-4332-adb3-284421f325d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b29a9f4-2e7a-4e6f-9642-1dcbac96a61a",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "f9a35cc9-1aad-4669-83f2-f363a27eea56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "f7138d6c-bfa2-4e48-90e3-4eefadb70ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a35cc9-1aad-4669-83f2-f363a27eea56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0817bc5-4d0e-46b4-aa27-faa17acc4654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a35cc9-1aad-4669-83f2-f363a27eea56",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "013d3975-79ea-45ed-a085-6de8e532ac13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "eb46f272-481d-4cdb-bec6-22537bac46a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013d3975-79ea-45ed-a085-6de8e532ac13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15047b42-aa2c-4a6d-8d58-6e5815bf6546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013d3975-79ea-45ed-a085-6de8e532ac13",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "04b3fe51-4919-4757-8653-a056e5e158de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "14735925-69f6-421c-97ae-c2b63b3216be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b3fe51-4919-4757-8653-a056e5e158de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f02f798-82a6-406c-b31f-51376d5aecd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b3fe51-4919-4757-8653-a056e5e158de",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "c2ed2946-42dd-4948-8067-1d677db22c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "8582d441-a088-4344-8d62-2dae98acb7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2ed2946-42dd-4948-8067-1d677db22c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fda6938-4ca6-4724-8063-b50ea0dc83ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ed2946-42dd-4948-8067-1d677db22c94",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "626cc907-6cc5-4eae-9cbc-195c085ec2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "1a4af69b-7e48-4cb1-ae1f-a769ff68bb52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626cc907-6cc5-4eae-9cbc-195c085ec2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b52660-47c6-4363-a30f-2cdb68c70579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626cc907-6cc5-4eae-9cbc-195c085ec2ba",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "cd1f1d2e-fd3e-4e7f-b80d-a5677c4a3b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "afed2446-deac-4cc9-85eb-4925493828e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1f1d2e-fd3e-4e7f-b80d-a5677c4a3b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d906f17-fbc8-4714-a364-7934a8e1fe17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1f1d2e-fd3e-4e7f-b80d-a5677c4a3b66",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "ec6f48b3-2f89-4f2e-b592-db1bc7004ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "88fdabb3-3e81-4ea2-aff4-25d23b03a981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6f48b3-2f89-4f2e-b592-db1bc7004ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "936a16e5-1127-4c2a-9887-b42c25e77e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6f48b3-2f89-4f2e-b592-db1bc7004ee8",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "cd6689db-b535-4fbc-bdbb-005214d2b9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "7c47f1a6-1a28-4402-b916-ec496d4cc217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd6689db-b535-4fbc-bdbb-005214d2b9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b497224-634f-4f34-9be1-a3798077ca0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd6689db-b535-4fbc-bdbb-005214d2b9e1",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "dbfba20a-812b-4c73-a145-f867e38a8036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "b310dc0c-36b4-46ab-b89a-dc3993a46c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbfba20a-812b-4c73-a145-f867e38a8036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d047fd-1b17-4876-8e02-b7333ad6822a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbfba20a-812b-4c73-a145-f867e38a8036",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "773eaf16-dd76-446b-a210-3e56e0d77557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "e3ff6048-c79e-4fc5-a18f-26a06c415494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773eaf16-dd76-446b-a210-3e56e0d77557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961ab481-241d-411c-b7cb-fd234b768977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773eaf16-dd76-446b-a210-3e56e0d77557",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "3bda0a16-ea7e-4bd3-b1c0-f1a744ee4ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "d4af397e-cbc0-4055-b5d9-94bfc6ae676e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bda0a16-ea7e-4bd3-b1c0-f1a744ee4ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3ae34c-3020-4bd4-a7d3-1581a4342c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bda0a16-ea7e-4bd3-b1c0-f1a744ee4ab5",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "a92c908e-82e4-48d2-a7df-25ad01e4f3cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "8497c16a-2ae3-4a5e-81c9-a96010492f46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92c908e-82e4-48d2-a7df-25ad01e4f3cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5826a910-c22c-4919-b79a-913c996d286e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92c908e-82e4-48d2-a7df-25ad01e4f3cc",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "4ed5e118-9cc8-4225-bb76-ba431697938f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "d8d50927-2ed0-4de3-a6bb-a7b8c5f47e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ed5e118-9cc8-4225-bb76-ba431697938f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79030de8-9f92-4b94-8f10-9f9cec4f6275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ed5e118-9cc8-4225-bb76-ba431697938f",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "2605a881-b696-4f8a-b6ee-3077789d9f18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "fbe22eb6-aeb3-4047-b2eb-7d812805f474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2605a881-b696-4f8a-b6ee-3077789d9f18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c3e191-d65d-4556-8689-e2e4a48349c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2605a881-b696-4f8a-b6ee-3077789d9f18",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "bc0ab5b0-9deb-4ce2-81a8-3052c0a75fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "6cf8283a-c7ca-40a9-838d-87abec91850a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc0ab5b0-9deb-4ce2-81a8-3052c0a75fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7dcc08a-1e0a-49b2-8433-748f283da325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc0ab5b0-9deb-4ce2-81a8-3052c0a75fe4",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "f9e1b003-0e24-4246-803f-cc24a048b9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "bb011652-bb9d-482c-a3a5-05fa56babb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e1b003-0e24-4246-803f-cc24a048b9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f88885d-7ab1-452c-9413-23d1c010f87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e1b003-0e24-4246-803f-cc24a048b9e7",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "bc1dd0a1-49af-4485-b8ac-bd0eada1c511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a758296d-ee42-48e5-b2df-c535deb1357b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1dd0a1-49af-4485-b8ac-bd0eada1c511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d169de01-8de7-4d89-8c4f-9d450c85c09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1dd0a1-49af-4485-b8ac-bd0eada1c511",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "429a6345-7f8e-41e6-b3be-a1299a9582c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "4e7245db-1693-40a1-ab6f-04a0acde4581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "429a6345-7f8e-41e6-b3be-a1299a9582c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0dc1d8-6864-4034-8d65-49666cdf1eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "429a6345-7f8e-41e6-b3be-a1299a9582c3",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "96882bd5-86ae-41e0-8aa8-dca55a3bbc31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a15bcb74-8f39-40b7-a510-373a779a86bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96882bd5-86ae-41e0-8aa8-dca55a3bbc31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9525498c-40df-4851-906f-d797e0633a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96882bd5-86ae-41e0-8aa8-dca55a3bbc31",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "aa214c9c-c9e7-4b5c-a0a4-fa57fb13a8d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "941f29f5-ea8e-4b2f-90af-723ab9cf7a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa214c9c-c9e7-4b5c-a0a4-fa57fb13a8d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d74eafb-eb40-44b1-b782-5d9424474bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa214c9c-c9e7-4b5c-a0a4-fa57fb13a8d8",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "7f7cfc6f-515e-45f5-9a41-a7a45f752811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "22b8f495-7ce4-4a0e-bf62-d64ff982e342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7cfc6f-515e-45f5-9a41-a7a45f752811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baafcc14-2b66-4b07-b089-5b8b908edd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7cfc6f-515e-45f5-9a41-a7a45f752811",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "ddbe6125-ad2b-438c-a764-ed0015c36d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "61229b49-9dd4-4c84-8e09-463ab7ef7e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddbe6125-ad2b-438c-a764-ed0015c36d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604952cb-a363-42f3-8e4e-e71afc3a2c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddbe6125-ad2b-438c-a764-ed0015c36d76",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "85d95723-f2d3-462e-b370-be4417b696b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "40a9f0cf-5c87-4fb2-b326-a694aeaab265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d95723-f2d3-462e-b370-be4417b696b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0892f48-6132-49be-b404-3042c8e83623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d95723-f2d3-462e-b370-be4417b696b3",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "affc7270-e84e-424e-a506-38e9cfe6b7f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "37d1de96-c703-4540-8f44-25bf84f708dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "affc7270-e84e-424e-a506-38e9cfe6b7f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e87d74-a9f5-4932-a768-fb2f5298ede9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "affc7270-e84e-424e-a506-38e9cfe6b7f9",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "c5cf7a3d-cbf0-49ab-8a0c-26d02a8bf28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "6f27cc94-2878-4b3f-89b5-3a10f280c05b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5cf7a3d-cbf0-49ab-8a0c-26d02a8bf28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1761d475-760b-4132-b7fa-b2cfc6f1d81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5cf7a3d-cbf0-49ab-8a0c-26d02a8bf28d",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "c7ff0da2-c56c-4c96-8eed-16a1e00dddb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "db7f9fad-96ca-448e-92bf-30ed006170f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ff0da2-c56c-4c96-8eed-16a1e00dddb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b98ba5-d03e-41da-8b94-336913869555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ff0da2-c56c-4c96-8eed-16a1e00dddb8",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "d69b4c03-9999-4b67-8529-ddcbb6c530a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a1e52888-1ce9-4662-b598-e9802fb1ab53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d69b4c03-9999-4b67-8529-ddcbb6c530a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6089c0a-05af-46ee-85cb-3822849f0641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69b4c03-9999-4b67-8529-ddcbb6c530a7",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "ebc58347-e62f-4942-8185-a1454085b66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "4c70f0aa-f418-471d-99a6-37017ab50504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc58347-e62f-4942-8185-a1454085b66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "355ffaba-bcb9-44db-bc5d-78485bec7097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc58347-e62f-4942-8185-a1454085b66f",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "6e9a06c8-eefb-4ae0-b761-e11c166a75d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "1c3f368d-daba-4969-9718-20df4a363ef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9a06c8-eefb-4ae0-b761-e11c166a75d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a36930e7-ad9f-402a-a017-e86308ff85f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9a06c8-eefb-4ae0-b761-e11c166a75d9",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "7925d100-78bd-4d44-b1b9-ee0dde968e03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "c32785bb-ab97-4540-ba13-bc461304c8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7925d100-78bd-4d44-b1b9-ee0dde968e03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6660138-3b26-4b1f-8c98-f5125fa6621a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7925d100-78bd-4d44-b1b9-ee0dde968e03",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "2dc2dce2-475c-480f-9662-f696408e586d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "97062a37-a3a6-4a04-9fc1-17449c0ba3a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc2dce2-475c-480f-9662-f696408e586d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dc7adb3-952b-491d-af6d-1630b65e5515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc2dce2-475c-480f-9662-f696408e586d",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "3d3f96ab-d336-4277-950c-67ff535121ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "9e546b7e-8225-4c02-940e-45d38fefbbdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3f96ab-d336-4277-950c-67ff535121ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5d75c3-0d44-4661-a43d-e5d411b4b3b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3f96ab-d336-4277-950c-67ff535121ee",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "88643aa7-47e0-4108-83f6-d40d8fffa6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "f020b54f-d990-45d8-8ef4-5a7200aca43d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88643aa7-47e0-4108-83f6-d40d8fffa6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca71c0f-241e-4ed5-aecc-6eebe12fea89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88643aa7-47e0-4108-83f6-d40d8fffa6ee",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "58028d74-ac7c-4479-a269-0771f12615c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "2454289a-0305-42e6-8f69-74d0879bda83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58028d74-ac7c-4479-a269-0771f12615c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586d22cd-4684-41cc-b7b2-e001cf3256a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58028d74-ac7c-4479-a269-0771f12615c3",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "d34f4031-d6fb-4862-b1bd-1d80fcb973cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "c5af72bb-cf49-4c6f-acbe-4b633df12084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34f4031-d6fb-4862-b1bd-1d80fcb973cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1186da-a5e1-49b3-9cb5-6cd1f2bd6dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34f4031-d6fb-4862-b1bd-1d80fcb973cb",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "c334e37f-9043-40ec-a07c-b8b47b7fad8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "27fc089f-0334-46a6-949a-be71841e9bd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c334e37f-9043-40ec-a07c-b8b47b7fad8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca1956ea-f6ad-48b2-a285-aa8e2800f4ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c334e37f-9043-40ec-a07c-b8b47b7fad8d",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "db338597-3557-4b1c-886f-7b2361a9eefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a176faa3-8ce0-4918-a1fb-4326ab661715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db338597-3557-4b1c-886f-7b2361a9eefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50a2701-9248-4257-ba5a-421b81c0baa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db338597-3557-4b1c-886f-7b2361a9eefc",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "41d6557c-d17e-4820-80fb-88c795f8d44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "1a05f24c-0051-494e-a7e1-7a464b00b87c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d6557c-d17e-4820-80fb-88c795f8d44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94539bba-3217-440a-a3b4-7efe6bd4b734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d6557c-d17e-4820-80fb-88c795f8d44a",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "16513b74-44d4-4045-8ecf-d37c6ff834b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "4519f8be-f339-45c3-abea-4430c8ef3d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16513b74-44d4-4045-8ecf-d37c6ff834b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96604f57-075a-456c-a3ca-a5febe921d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16513b74-44d4-4045-8ecf-d37c6ff834b6",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "7ab6ff92-defd-41aa-ae59-f54496776f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "0bf74dbd-e2bc-4589-88c2-6cad689872bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab6ff92-defd-41aa-ae59-f54496776f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5d06cc-5815-4de0-b33a-e12d72c71e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab6ff92-defd-41aa-ae59-f54496776f3f",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "86d16f8e-39cc-464c-8f43-ec46b1f826bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "289396bb-ceec-493a-9659-7db57210e1d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d16f8e-39cc-464c-8f43-ec46b1f826bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4615c17e-7abc-48aa-b695-e2a7306eaf8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d16f8e-39cc-464c-8f43-ec46b1f826bb",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "aab10fa5-d6c3-4b5e-9109-889ef2d88892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "de257f94-3267-4962-8dce-156010ac43bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab10fa5-d6c3-4b5e-9109-889ef2d88892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "448b47aa-769a-4478-8c0b-48da55a761e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab10fa5-d6c3-4b5e-9109-889ef2d88892",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "416e1d1c-da1d-485a-81df-ae1a2d7d82a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "004a50b9-80a7-470d-86f9-4b8c98859aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416e1d1c-da1d-485a-81df-ae1a2d7d82a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172db831-39c0-498d-90e4-09fc855117e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416e1d1c-da1d-485a-81df-ae1a2d7d82a0",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "9436bc4d-9a40-46c8-bc00-9aeb53a6e7ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "f4007525-d763-4828-8b07-201c0516f8b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9436bc4d-9a40-46c8-bc00-9aeb53a6e7ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f16ad82-aa00-49b8-aa76-2d487a22273d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9436bc4d-9a40-46c8-bc00-9aeb53a6e7ae",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "3483fa77-15f9-479a-8ccd-34bbd6b21083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "b4b2d001-eb14-4d4f-865b-973402c2b2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3483fa77-15f9-479a-8ccd-34bbd6b21083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91fc751e-33f4-471b-8ca7-adac11853a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3483fa77-15f9-479a-8ccd-34bbd6b21083",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "2fe3c5ca-bc4e-4349-866a-e6526c05dc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "2eae693f-aaa0-402b-b98b-1038b25b8007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe3c5ca-bc4e-4349-866a-e6526c05dc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8b1504-1251-4e40-9cbf-a62574818cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe3c5ca-bc4e-4349-866a-e6526c05dc3a",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "479405ae-6fa6-432c-b676-cb775b376b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "b2c3a64d-d7bb-4519-8193-3f9e3b784448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "479405ae-6fa6-432c-b676-cb775b376b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a23d02-7650-41b2-bdc9-838f8045cba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479405ae-6fa6-432c-b676-cb775b376b89",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "a0886262-1079-4cd1-948e-5b818e76099a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "d654bfec-b163-44b6-98f0-d75de4e33da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0886262-1079-4cd1-948e-5b818e76099a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e188dd76-2318-4982-9add-a26b094e2ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0886262-1079-4cd1-948e-5b818e76099a",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "2a196b77-d532-4af2-9245-4e54f2bd9e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "20401d10-b379-4fb9-abf8-3f5140fbd9cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a196b77-d532-4af2-9245-4e54f2bd9e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f627d522-1957-4f99-a4a8-5b14b10fe274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a196b77-d532-4af2-9245-4e54f2bd9e23",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "2b40fd28-1b78-4805-bc9e-1b628cd2def0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "53da0b73-dda1-4bea-a84b-aa02de32da85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b40fd28-1b78-4805-bc9e-1b628cd2def0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b3f61b-f654-4d96-9d88-97bd098df867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b40fd28-1b78-4805-bc9e-1b628cd2def0",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "183b8ce6-4d5e-4071-be9c-e07b972f1bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "0cd1c87d-257d-4272-9637-98733db4014b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "183b8ce6-4d5e-4071-be9c-e07b972f1bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b81790e-c275-4a52-a4c9-a5ccb8ac2ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "183b8ce6-4d5e-4071-be9c-e07b972f1bed",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "a5d5496d-f022-4870-89d2-db5fb560b064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "96c1c8dd-17bc-48f4-8130-7c4173100639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d5496d-f022-4870-89d2-db5fb560b064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7b525d-4f2b-4c4a-9f9c-27a14fcf7c8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d5496d-f022-4870-89d2-db5fb560b064",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "af143444-b681-4d75-9548-5f9b5c8d06c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "e8512a2f-a191-4d88-860a-c57197c7fb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af143444-b681-4d75-9548-5f9b5c8d06c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62dcef20-ef7a-4e36-9893-e75b7a08e515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af143444-b681-4d75-9548-5f9b5c8d06c4",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "8e2da303-a0a9-4e64-b990-7717c68ae27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "0e7121b4-078c-4d2c-b84b-6396c193dafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2da303-a0a9-4e64-b990-7717c68ae27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e28da49-399a-4307-98e6-f44c088284ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2da303-a0a9-4e64-b990-7717c68ae27e",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "1927c318-6f30-4b5c-b227-8f42457c47b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "d8bad2f8-266a-441e-920b-d82ed44cb5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1927c318-6f30-4b5c-b227-8f42457c47b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450d7227-c5c3-44bc-a7c8-476f510caf53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1927c318-6f30-4b5c-b227-8f42457c47b1",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "e8b8be0a-81d1-4b47-854f-f6bdbd5e9302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "3afe41c8-2821-4021-bab4-4044822632d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b8be0a-81d1-4b47-854f-f6bdbd5e9302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779af777-3f76-417f-bf4f-7b017c05df5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b8be0a-81d1-4b47-854f-f6bdbd5e9302",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "dc212a85-79c9-49d4-83a6-dee5eafa6f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "b4671efb-46c7-44e3-a657-f4aaffa46723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc212a85-79c9-49d4-83a6-dee5eafa6f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9ad7ae-c67c-40c1-ad30-f868c3c73aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc212a85-79c9-49d4-83a6-dee5eafa6f5f",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "cbe952a0-ff60-4ffb-a235-29a58491f083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "35fbfeef-ef89-4ba9-b102-2c43204b88e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe952a0-ff60-4ffb-a235-29a58491f083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d56c61f7-a40f-4fc2-8fe4-70447c91bcb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe952a0-ff60-4ffb-a235-29a58491f083",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "f5413fa3-53a0-4574-8fe9-ea1c91f07b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "084a5a6e-010d-41ea-a0a9-cce2740058b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5413fa3-53a0-4574-8fe9-ea1c91f07b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea2ad381-6b33-4eb4-83bf-611c7b13ef7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5413fa3-53a0-4574-8fe9-ea1c91f07b05",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "68813431-8789-46c6-92c0-e4c3c4fc8e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "92930de5-023c-4045-8423-84ea7687010f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68813431-8789-46c6-92c0-e4c3c4fc8e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e862e193-a8e1-4ab5-968e-178260699a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68813431-8789-46c6-92c0-e4c3c4fc8e82",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "d78d8a7e-0711-489d-aaf2-5d63aa704035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "72ac0fbd-649e-415e-9fd7-a13970c8c971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d78d8a7e-0711-489d-aaf2-5d63aa704035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dce6cb5-b2be-4f3e-8d15-5fdbf436c7d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d78d8a7e-0711-489d-aaf2-5d63aa704035",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "8c4d4c5e-2605-4e3d-9cf3-ab007e041697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "59441e46-7be1-4d35-a47b-1d152b742ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4d4c5e-2605-4e3d-9cf3-ab007e041697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b5be96-76df-4aa8-af6a-a8bcf9492da8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4d4c5e-2605-4e3d-9cf3-ab007e041697",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "d886fcb2-1e55-4ff5-9f4d-63938f518aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "3f35f1be-e014-47ff-9d7d-805fc4b30544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d886fcb2-1e55-4ff5-9f4d-63938f518aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1fd3ec-9358-45d2-9e3a-d48621ca1d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d886fcb2-1e55-4ff5-9f4d-63938f518aa8",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "978fee95-bc54-4fd0-ab52-2154d8d2edea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "e4dd56c7-23ea-4538-b8d8-dd7b50de7698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978fee95-bc54-4fd0-ab52-2154d8d2edea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1881740c-85d8-4d95-bbc6-60224b2df18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978fee95-bc54-4fd0-ab52-2154d8d2edea",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "fa987d8c-5eb9-4591-95a6-5ff8695c4708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "53b174ba-7378-44ca-a2c5-f6604bb2286d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa987d8c-5eb9-4591-95a6-5ff8695c4708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf87fbd-e9b9-49de-9e8e-d48c7e0adfd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa987d8c-5eb9-4591-95a6-5ff8695c4708",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "7b35d911-4bd1-425d-be58-dd2eb0030c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "ac2f5307-c8b1-4011-a00e-5afbc31606ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b35d911-4bd1-425d-be58-dd2eb0030c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1333c5c2-f6f2-42ef-81c3-edee4e41a6cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b35d911-4bd1-425d-be58-dd2eb0030c8c",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "86d8e976-d3d8-4b1e-bcd5-a4de1b39cb1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "acbffad0-0265-4d37-9eb8-e65c4f666cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d8e976-d3d8-4b1e-bcd5-a4de1b39cb1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b5df53-3a06-4c7f-a1a2-f6cd2959cceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d8e976-d3d8-4b1e-bcd5-a4de1b39cb1b",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "63b56a64-2ff1-4a58-ad46-06726edf8e33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "145e3d09-f566-4f7c-b870-4367dba33563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b56a64-2ff1-4a58-ad46-06726edf8e33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674b13b7-9417-4c0d-92a4-ec64df3a5d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b56a64-2ff1-4a58-ad46-06726edf8e33",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "dbf64f66-80e5-46d6-bf17-bd33c620bc6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "2bc64ced-aa30-4631-9381-4d37649af4f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf64f66-80e5-46d6-bf17-bd33c620bc6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ae582c-9ff8-4cd2-95b2-f8d6957aca10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf64f66-80e5-46d6-bf17-bd33c620bc6b",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "bc191b4f-53c0-4e74-95e5-ba691505fb54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a6618898-6c6e-4cd8-9e73-8597044b18d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc191b4f-53c0-4e74-95e5-ba691505fb54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60fc334-827d-432a-8ca8-20bfbb9c6ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc191b4f-53c0-4e74-95e5-ba691505fb54",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "5aea3c67-cc69-404c-9719-b37fd40887b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "2de99c58-3eb6-4a6e-be2e-18b35227eea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aea3c67-cc69-404c-9719-b37fd40887b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92e245d9-2ad1-4e87-acb9-2ad5ae8c8322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aea3c67-cc69-404c-9719-b37fd40887b9",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "dd8320f5-c174-45ba-b48c-799f5c74fc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "a033ac3c-249e-49eb-ac49-307d4f788b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8320f5-c174-45ba-b48c-799f5c74fc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce3deef-e14f-4712-bc27-aa0de704c95c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8320f5-c174-45ba-b48c-799f5c74fc33",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "d4f02771-14c9-45b9-b79f-017553769f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "f63554b8-1ccd-4847-99ec-7b55cc30d96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f02771-14c9-45b9-b79f-017553769f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd24f8c-3481-4b74-a63b-088603302389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f02771-14c9-45b9-b79f-017553769f44",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "e359f834-e959-4ec7-8f7c-3f2efd4cf931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "8f3cb0e3-7d8d-4286-a0a2-ed73a76b038d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e359f834-e959-4ec7-8f7c-3f2efd4cf931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1907d80f-4dd7-4f6a-86f0-28a26a2d6cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e359f834-e959-4ec7-8f7c-3f2efd4cf931",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "5235eb88-d1bc-4091-b6ee-09343c6403fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "0a51ea22-8389-4090-b776-1300e9225606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5235eb88-d1bc-4091-b6ee-09343c6403fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67ce9ae-cba9-4821-9053-a525d0b8ac2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5235eb88-d1bc-4091-b6ee-09343c6403fb",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "58e6de59-bc61-4ceb-a9ac-c59ee681864d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "17cfb420-7de2-4bc5-b5bf-8ef4c3d664a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e6de59-bc61-4ceb-a9ac-c59ee681864d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737d8312-c758-48c5-9ff6-9a9d9d65afe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e6de59-bc61-4ceb-a9ac-c59ee681864d",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        },
        {
            "id": "c1a2ddfd-c182-4597-9ab8-e3483dd8d30f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "compositeImage": {
                "id": "3383b733-a012-4925-acb7-862f9c66ba0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a2ddfd-c182-4597-9ab8-e3483dd8d30f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3ace86-273a-4fa2-864c-898f0deaeca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a2ddfd-c182-4597-9ab8-e3483dd8d30f",
                    "LayerId": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "fbd49b96-83f6-4dd3-88e2-f87fb2ac6df3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8ecb4e8-4096-4d12-af67-d08e4caf7c45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}