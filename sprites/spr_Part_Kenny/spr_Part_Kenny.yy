{
    "id": "ee9a1ca3-c6bc-40cf-9be7-8146c0cd97b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Part_Kenny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 1,
    "bbox_right": 98,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e6e2574-d8eb-4902-80fe-d62ad840c513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9a1ca3-c6bc-40cf-9be7-8146c0cd97b2",
            "compositeImage": {
                "id": "6a1b5071-f88a-484b-af01-b79ab90ac23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6e2574-d8eb-4902-80fe-d62ad840c513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc0bb3d-ac41-4bfb-899e-256522f251b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6e2574-d8eb-4902-80fe-d62ad840c513",
                    "LayerId": "3ac14180-50dc-446c-8354-ee29ddfaed9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "3ac14180-50dc-446c-8354-ee29ddfaed9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9a1ca3-c6bc-40cf-9be7-8146c0cd97b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 45
}