{
    "id": "e1b53565-eaf0-49c3-b13f-79df1da95ac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bboxCircle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d920be95-c49e-420e-81f0-df54f9f3911b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b53565-eaf0-49c3-b13f-79df1da95ac2",
            "compositeImage": {
                "id": "4ef23d5c-ac88-4217-bcae-56487a210329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d920be95-c49e-420e-81f0-df54f9f3911b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b754dfb-e36f-4829-8568-dd10e7ae97e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d920be95-c49e-420e-81f0-df54f9f3911b",
                    "LayerId": "913084a3-101f-45df-928c-977b6187fb2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "913084a3-101f-45df-928c-977b6187fb2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1b53565-eaf0-49c3-b13f-79df1da95ac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}